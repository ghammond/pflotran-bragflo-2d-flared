#! /bin/bash 

# horsetail_diff_area_plot.sh
# Glenn Hammond <gehammo@sandia.gov>

# Usage: horsetail_diff_area_plot.sh <output directory>

# choose bragflo results to compare against
this_dir=$(pwd)
if [[ $this_dir == *"tt04"* ]]; then
  bf_sum_dir=../bragflo_summary_tt_04
else
  bf_sum_dir=../bragflo_summary_orig_data
fi
echo ''
echo 'pwd = '$this_dir
echo 'bf output in '$bf_sum_dir
echo ''

output_dir=horsetail_plot_dir
if [ "$1" != "" ]; then
  output_dir=$1
fi

if [ ! -d "$output_dir" ]; then
  echo ""
  echo "Output directory '$output_dir' not found. Exiting."
  echo ""
  echo "Usage: horsetail_diff_area_plot.sh <output directory>"
  echo ""
  echo "<output directory> must be included if not named 'horsetail_plot_dir'."
  echo ""
  exit 1
fi

screen_file=plot_horsetail_diff_area.stdout
/bin/rm $screen_file
touch $screen_file
for s in {1,2,3,4,5,6,}; do
  for r in {1,2,3,}; do 
    for reg in {WAS_AREA,SPCS,SROR,NROR,OPS,EXP,MPCS,NPCS,SHAFT,}; do
      for var in {PRESBRIN,SATBRINE,}; do
        python horsetail_diff_area.py \
                                      -d $output_dir \
                                      -s $s \
                                      -r $r \
                                      -e $reg \
                                      -a $var \
                                      -c rainbow \
                                      -t 0.02 \
                                      -z 0.01 \
                                      | tee -a $screen_file
      done
    done
  done
done
wait
python generate_plots_of_violators.py -b $bf_sum_dir'/pfd_analysis_bragflo_summary_scenario{}.h5' \
                                      -f $screen_file \
                                      -v replot_violators.sh \
                                      -n replot_nonviolators.sh \
                                      -x 'rel diff threshold 0.02; abs diff threshold 0.01'
wait
chmod uag+x replot_violators.sh
chmod uag+x replot_nonviolators.sh
./replot_violators.sh
./replot_nonviolators.sh

