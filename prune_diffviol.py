'''prune_diffviols.py
   create copy of replot_diffviols.sh that eliminates any lines
   that are going to be plotted by replot_violators.sh
'''

vfile = 'replot_violators.sh'
dfile = 'replot_diffviols.sh'
outfile = 'replot_diffviols_edit.sh'

with open(vfile,'r') as f:
  viols = f.readlines()
with open(dfile,'r') as f:
  diffs = f.readlines()

fout = file(outfile,'w')
fout.writelines(diffs[:26]) #26 lines before python calls begin
for vline in viols[26:]: #20 fields before vector list begins
  #PROBLEM - if vline doesn't exist then dline doesn't get rewritten
  for dline in diffs[26:]: 
    if vline.split()[:20] == dline.split()[:20]:
      save = []
      for v in dline.split()[20:]:
        if v not in vline.split()[20:]:
          save.append(v.strip('\n'))
      if len(save) > 0:
        string1 = ' '.join(vline.split()[:20])
        string2 = ' '.join(save)
        fout.write('{} {}\n'.format(string1,string2))
fout.close()


