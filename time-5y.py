'''
This file defines input that is necessary to a single scenario:
time-5y (the precursor to all scenarios)
including:
1. brine, a flag equal to either 'salado' or 'castile'
2. variables used in the SIMULATION block
3. stratalist, a list of tuples for STRATA blocks
4. variables related to time step control (TIME)
5. variables related to solver behavior (TIMESTEPPER, NEWTON_SOLVER, LINEAR_SOLVER)
6. variables related to output (OUTPUT)

It is read and executed as a python script by prepflotran.py
Variable names in this file must match those in prepflotran.py

Emily 8.24.17
'''

#brine flag is used to determine whether BRUCITEC or BRUCITES
#is written to WIPP_SOURCE_SINK
#if != 'castile' and != 'salado' then WIPP_SOURCE_SINK block is not written
brine = 'no_gas_gen'

#SIMULATION stuff
#Options that are required are hardcoded in block_templates.py
#Remove '!' to activate an option. Add leading '!' to deactivate.
s_flow_options = {
                  'NO_CREEP_CLOSURE':'',
                  'ALLOW_NEGATIVE_GAS_PRESSURE':'',
                  'BRAGFLO_RESIDUAL_UNITS':'',
                  '2D_FLARED_DIRICHLET_BCS':'\n  EXTERNAL_FILE ../dirichlet_bcs.txt\n/',
                  'SCALE_JACOBIAN':'                        ! LSCALE',
                  'CONVERGENCE_TEST':'BOTH                  ! ICONVTEST 1',
                  'LIQUID_RESIDUAL_INFINITY_TOL':'1.d-2     ! FTOL_SAT',
                  'GAS_RESIDUAL_INFINITY_TOL':'1.d-2        ! FTOL_PRES',
                  'MAX_ALLOW_REL_LIQ_PRES_CHANG_NI':'1.d-2  ! EPS_PRES',
                  'MAX_ALLOW_REL_GAS_SAT_CHANGE_NI':'1.d-3  ! EPS_SAT',
                  'GAS_SAT_THRESH_FORCE_EXTRA_NI':'1.d-3    ! SATLIMIT',
                  'GAS_SAT_THRESH_FORCE_TS_CUT':'0.2d0      ! DSATLIM',
                  'MIN_LIQ_PRES_FORCE_TS_CUT':'-1.d8        ! DPRELIM',
                  'MAX_ALLOW_GAS_SAT_CHANGE_TS':'1.d0       ! DSAT_MAX',
                  'MAX_ALLOW_LIQ_PRES_CHANGE_TS':'1.d7      ! DPRES_MAX',
                  'GAS_SAT_CHANGE_TS_GOVERNOR':'0.3d0       ! SATNORM',
                  'LIQ_PRES_CHANGE_TS_GOVERNOR':'5.d5       ! PRESNORM',
                  'GAS_SAT_GOV_SWITCH_ABS_TO_REL':'1.d0     ! TSWITCH',
                  'MINIMUM_TIMESTEP_SIZE':'8.64d-4          ! DELTMIN',
                 }
s_transport = False 
s_chkpt = True     
s_chkpt_format = 'HDF5'
s_chkpt_times = [0.,1.57680000000000e+08]
s_chkpt_unit = 's'
s_restart = False 
s_restart_0 = True #Sorry, this is needed though not used.

#STRATA stuff
#three types of strata:
#1. materials from .h5 file
#   ('filename')
#2. initial or unchanging material by coupling with region
#   ('region','material')
#3. material change
#   ('region','material',start time,end time,'unit')
#units need to be consistent with PFLOTRAN usage
#I will look that up and add instructions

stratalist = [('../grid/materials.h5'), #t = -5y materials
             ]
#TIME stuff
t_final = 5.  # years
t_final_unit = 'y'
t_initial_step = 8.64e+02
t_initial_unit = 's' #'d'
#{at time : limit to this time step}
t_max_step = {0:1.728e+09, #55 years
             } 
t_max_unit = 's'

#TIMESTEPPER stuff
ts_flow_pm = 'FLOW'
ts_flow_maxcuts = 40
ts_flow_maxsteps = 20000

#NEWTON_SOLVER FLOW
ns_flow_pm = 'FLOW'
ns_flow_maxit = 8

#LINEAR_SOLVER FLOW
ls_flow_pm = 'FLOW'
ls_flow_solver = 'DIRECT'

#OBSERVATION points - don't want these, but would need more sophistication
#to get rid of them
obslist = ['rWAS_AREA',
           'rSROR',
           'rNROR',
           'rOPS_AREA',
           'rEXP_AREA',
           'rSPCS',
           'rMPCS',
           'rNPCS',
          ]

#OUTPUT stuff
o_variables = ['!MAXIMUM_PRESSURE',
               'LIQUID_PRESSURE',
               'LIQUID_SATURATION',
               'LIQUID_DENSITY',
               'GAS_PRESSURE',
               'GAS_SATURATION',
               'GAS_DENSITY',
               '!LIQUID_MOLE_FRACTIONS',
               '!GAS_MOLE_FRACTIONS',
               'CAPILLARY_PRESSURE',
               '!VAPOR_PRESSURE',
               '!SATURATION_PRESSURE',
               '!AIR_PRESSURE',
               '!TEMPERATURE',
               'EFFECTIVE_POROSITY',
               '!POROSITY',
               'PERMEABILITY',
               'MATERIAL_ID',
               '!MATERIAL_ID_KLUDGE_FOR_VISIT',
               'VOLUME']
o_format = ['HDF5']
o_times = [
   0.00000000000000e+00,
   1.57680000000000e+08,
          ]
o_times_unit = 's'  # y for years, d for days, etc
o_periodic_times = {1.57680000000000e+08:[0.,1.57680000000000e+08]} #results in same as o_times list
o_periodic_unit = 's'

############ FLOW_CONDITION ##################
#Works only with MODE WIPP_FLOW or MODE BRAGFLO
#(name, liquid pressure, gas saturation)
#values can be numeric or refer to pflotran dataset name listed above
liqp_dataset = 'initial_liq_pressure'
gass_dataset = 'initial_gas_saturation'
flowlist = [('initial',liqp_dataset,gass_dataset),
            ('culebra',CULEBRA.pressure,0.),
            ('magenta',MAGENTA.pressure,0.),
            ('top',BRINESAL.ref_pres,1.-SANTAROS.sat_ibrn),
           ]

############ INITIAL_CONDITION ###############
#Irrelevant if initial conditions are read from restart file.
# (flow condition, (transport condition), region)
iclist = [('initial','all'),
          ('culebra','rCULEBRA'),
          ('magenta','rMAGENTA')]

############ BOUNDARY CONDITION ##############
#(flow condition, (transport condition), region)
bclist = [('top','top_edges'),]


