'''
This file defines input that is relevant to all scenarios and all vectors,
including:
1. DATASET name for soil ref. pressures (possibly belongs elsewhere)
2. WIPP_CREEP_CLOSURE name and file (possibly belongs elsewhere)
3. FLOW_CONDITION setup (possibly belongs elsewhere)
4. INITIAL_CONDITION setup (possibly belongs elsewhere)
5. BOUNDARY_CONDITION setup (possibly belongs elsewhere)
6. MATERIAL_PROPERTY edits (some of these could be elsewhere)
7. calculation of REFCON.STCO_22

It is read and executed as a python script by prepflotran.py
Variable names in this file must match those in prepflotran.py

###
# Always be sure to test the output to make sure the operation requested were
# executed correctly.
# Execute <object>.display_values() to see all the values held in an object.
###
# AUTHORS: Heeho Park (Sandia National Labs) heepark@sandia.gov
#          Brad Day (Sandia National Labs) baday@sandia.gov
#          Emily Stein (Sandia National Labs) ergiamb@sandia.gov
# MOST RECENT UPDATE: 08/04/2016, 4.17.17, 6.22.17, 8.24.17
'''

# ------------ algebra commands -----------------------------------------------
import numpy as np

########### WIPP CREEP_CLOSURE ##################
#Keep somewhere the user can edit
creepclosure.creep_table = 'creep001'
creepclosure.creep_filename = '../pflotran_closure.dat'

############ MATERIAL_PROPERTY ################
# Database "material" includes things like H2 and NITRATE.
# PFLOTRAN "material" includes only things described with params below.
# param_objs{} is a dictionary of both Database and PFLOTRAN materials
# mat_order is a list of PFLOTRAN material names

#copy WAS_AREA params to REPOSIT (everything but self.name and self.id)
param_objs['REPOSIT'].copy(param_objs['WAS_AREA'])
#for all materials do the following:
for obj in s_info.mat_order:
  param_objs[obj].tortuosity = 1.
  #param_objs[obj].rock_density = 2700.
  #param_objs[obj].heat_capacity = 830.
  #param_objs[obj].thcondwet = 2.5
  #param_objs[obj].thconddry = 2.5
  param_objs[obj].prmy_log = param_objs[obj].prmx_log
  param_objs[obj].prmz_log = param_objs[obj].prmx_log
  param_objs[obj].calculate_m()
  #param_objs[obj].calculate_alpha()
  param_objs[obj].make_krp_str()
  #calculate comp_por = comp_rck/porosity for all except SHFTU,SHFTL_T1,SHFTL_T2
  #all DRZ materials overwrite this value later in algebra.py
  if param_objs[obj].comp_rck != None and param_objs[obj].comp_por == None:
    param_objs[obj].comp_por = param_objs[obj].comp_rck/param_objs[obj].porosity
    #print(param_objs[obj].name,param_objs[obj].id,param_objs[obj].comp_por)
  #calculate comp_rck = comp_por*porosity for SHFTU, SHFTL_T1, SHFTL_T2
  #no longer necessary but no harm in filling these values
  if param_objs[obj].comp_rck == None and param_objs[obj].comp_por != None:
    param_objs[obj].comp_rck = param_objs[obj].comp_por * param_objs[obj].porosity

############## additional edits to MATERIAL_PROPERTY ##################

# edited from Brad Day (baday@sandia.gov) pfd_alg.mal
#***********************************************************************
# ALGEBRA commands
#***********************************************************************
# current status is limited to permeability,porosity, and fracture relationships

#***********************************************************************
# S_HALITE = Salado halite
#***********************************************************************
#   sampled parameters:
#       prmx_log, porosity, comp_rck, pressure
#   no further edits required
#***********************************************************************

#**********************************************************************
# DRZ_0 = DRZ around waste from -5 yrs to 0 yrs
#**********************************************************************
#   sampled parameters:
#       none
#**********************************************************************
#   correlate porosity to sampled s_halite porosity plus addpor
DRZ_0.porosity = S_HALITE.porosity + DRZ_0.addpor
#   calculate fully fractured porosity
DRZ_0.frac_phi = DRZ_0.porosity + DRZ_0.dphimax
#   calculate porosity at fracture initiation pressure
porinit = DRZ_0.porosity*np.exp(DRZ_0.comp_rck*DRZ_0.pi_delta/DRZ_0.porosity)
#   calculate n exponential for fractured permeability model
DRZ_0.frac_exp = np.log(10**DRZ_0.kmaxlog/10**DRZ_0.prmx_log)/np.log(DRZ_0.frac_phi/porinit)
#   delete temporary variables
del porinit
#   calculate pore compressibility from newly calc'd porosity
DRZ_0.comp_por = DRZ_0.comp_rck/DRZ_0.porosity

#***********************************************************************
# S_MB139  = Salado marker bed 139
#***********************************************************************
#   sampled parameters:
#       prmx_log, relp_mod, sat_rbrn, pore_dis
#***********************************************************************
#   calculate fully fractured porosity
S_MB139.frac_phi = S_MB139.porosity + S_MB139.dphimax
#   calculate porosity at fracture initiation pressure
porinit = S_MB139.porosity*np.exp(S_MB139.comp_rck*S_MB139.pi_delta/S_MB139.porosity)
#   calculate n exponential for fractured permeability model
S_MB139.frac_exp = np.log(10**S_MB139.kmaxlog/10**S_MB139.prmx_log)/np.log(S_MB139.frac_phi/porinit)
#   delete temporary variables
del porinit

#***********************************************************************
# S_ANH_AB = Salado anhydrite layers a + b
#***********************************************************************
#   sampled parameters:
#       none
#***********************************************************************
#   correlate permeability to sampled S_MB139 permeability
S_ANH_AB.prmx_log = S_MB139.prmx_log
S_ANH_AB.prmy_log = S_ANH_AB.prmx_log
S_ANH_AB.prmz_log = S_ANH_AB.prmx_log
#   correlate permeability model to sampled S_MB139 permeability model
S_ANH_AB.relp_mod = S_MB139.relp_mod
S_ANH_AB.krp_str = S_MB139.krp_str
#   correlate pore distribution to sampled S_MB139 pore distribution
S_ANH_AB.pore_dis = S_MB139.pore_dis
S_ANH_AB.alpha = S_MB139.alpha
S_ANH_AB.m = S_MB139.m
#   correlate residual brine and gas saturation to sampled S_MB139
#   residual brine and gas saturation
S_ANH_AB.sat_rbrn = S_MB139.sat_rbrn
S_ANH_AB.sat_rgas = S_MB139.sat_rgas
#   calculate fully fractured porosity
S_ANH_AB.frac_phi = S_ANH_AB.porosity + S_ANH_AB.dphimax
#   calculate porosity at fracture initiation pressure
porinit = S_ANH_AB.porosity*np.exp(S_ANH_AB.comp_rck*S_ANH_AB.pi_delta/S_ANH_AB.porosity)
#   calculate n exponential for fractured permeability model
S_ANH_AB.frac_exp = np.log(10**S_ANH_AB.kmaxlog/10**S_ANH_AB.prmx_log)/np.log(S_ANH_AB.frac_phi/porinit)
#   delete temporary variables
del porinit

#***********************************************************************
# S_MB138 = Salado marker bed 138
#***********************************************************************
#   sampled parameters:
#       none
#***********************************************************************
#   correlate permeability to sampled S_MB139 permeability
S_MB138.prmx_log = S_MB139.prmx_log
S_MB138.prmy_log = S_MB138.prmx_log
S_MB138.prmz_log = S_MB138.prmx_log
#   correlate permeability model to sampled S_MB139 permeability model
S_MB138.relp_mod = S_MB139.relp_mod
S_MB138.krp_str = S_MB139.krp_str
#   correlate pore distribution to sampled S_MB139 pore distribution
S_MB138.pore_dis = S_MB139.pore_dis
S_MB138.alpha = S_MB139.alpha
S_MB138.m = S_MB139.m
#   correlate residual brine and gas saturation to sampled S_MB139
#   residual brine and gas saturation
S_MB138.sat_rbrn = S_MB139.sat_rbrn
S_MB138.sat_rgas = S_MB139.sat_rgas
#   calculate fully fractured porosity
S_MB138.frac_phi = S_MB138.porosity + S_MB138.dphimax
#   calculate porosity at fracture initiation pressure
porinit = S_MB138.porosity*np.exp(S_MB138.comp_rck*S_MB138.pi_delta/S_MB138.porosity)
#   calculate n exponential for fractured permeability model
S_MB138.frac_exp = np.log(10**S_MB138.kmaxlog/10**S_MB138.prmx_log)/np.log(S_MB138.frac_phi/porinit)
#   delete temporary variables
del porinit

#***********************************************************************
# CAVITY_1 = open waste area from -5 yrs to 0 yrs
#***********************************************************************
#   sampled parameters:
#       none
#   no further edits required
#***********************************************************************

#***********************************************************************
# CAVITY_2 = open rest of repository from -5 yrs to 0 yrs
#***********************************************************************
#   sampled parameters:
#       none
#   no further edits required
#***********************************************************************

#***********************************************************************
# CAVITY_3 = ops/exp from -5 yrs to 0 yrs
#***********************************************************************
#   sampled parameters:
#       none
#   no further edits required
#***********************************************************************

#***********************************************************************
# CAVITY_4 = shaft from -5 yrs to 0 yrs
#***********************************************************************
#   sampled parameters:
#       none
#   no further edits required
#***********************************************************************

#***********************************************************************
# IMPERM_Z = impermeable zone
#***********************************************************************
#   sampled parameters:
#       none
#   comments:
#       includes the rustler, santa rosa, dewey lake, and castile.
#       used to control drainage of formations above the salado into the
#       shaft prior to t=0 years.  it is also used to prevent brine pocket
#       from becoming de-pressurized prior to drilling intrusion.
#   no further edits required
#***********************************************************************

#***********************************************************************
# CASTILER = brine pocket
#***********************************************************************
#   sampled parameters:
#       comp_rck, pressure, prmx_log
#   comments:
#       this section is modified to account for a brine pocket porosity
#       treatment used in the 1997 c97 calculations.
#***********************************************************************
#   calculate brine pocket porosity from
#   min(comp_rck)/min(por_bkpt) = max(comp_rck)*max(por_bkpt)
#   = 1.0860e-10
CASTILER.porosity = CASTILER.comp_rck/1.0860E-10

#***********************************************************************
# DRZ_OE_0 = DRZ around ops/exp from -5 yrs to 0 yrs
#***********************************************************************
#   sampled parameters:
#       none
#***********************************************************************
#   correlate porosity to sampled DRZ_0 porosity
DRZ_OE_0.porosity = DRZ_0.porosity
#   turn on fracture
DRZ_OE_0.fracture=True
#   correlate fracture parameters to DRZ_0
DRZ_OE_0.pi_delta = DRZ_0.pi_delta
DRZ_OE_0.pf_delta = DRZ_0.pf_delta
DRZ_OE_0.ifrx = DRZ_0.ifrx
DRZ_OE_0.ifry = DRZ_0.ifry
DRZ_OE_0.ifrz = DRZ_0.ifrz
DRZ_OE_0.kmaxlog = DRZ_0.kmaxlog
DRZ_OE_0.frac_phi = DRZ_0.frac_phi
#   calculate porosity at fracture initiation pressure
porinit = DRZ_OE_0.porosity*np.exp(DRZ_OE_0.comp_rck*DRZ_OE_0.pi_delta/DRZ_OE_0.porosity)
#   calculate n exponential for fractured permeability model
DRZ_OE_0.frac_exp = np.log(10**DRZ_OE_0.kmaxlog/10**DRZ_OE_0.prmx_log)/np.log(DRZ_OE_0.frac_phi/porinit)
# - delete temporary variables
del porinit
#   calculate pore compressibility from newly calc'd porosity
DRZ_OE_0.comp_por = DRZ_OE_0.comp_rck/DRZ_OE_0.porosity

#***********************************************************************
# DRZ_PC_0 = DRZ around PCS from -5 yrs to 0 yrs
#***********************************************************************
#   sampled parameters:
#       none
#***********************************************************************
#   correlate porosity to sampled DRZ_0 porosity
DRZ_PC_0.porosity = DRZ_0.porosity
#   turn on fracture
DRZ_PC_0.fracture=True
#   correlate fracture parameters to DRZ_0
DRZ_PC_0.pi_delta = DRZ_0.pi_delta
DRZ_PC_0.pf_delta = DRZ_0.pf_delta
DRZ_PC_0.ifrx = DRZ_0.ifrx
DRZ_PC_0.ifry = DRZ_0.ifry
DRZ_PC_0.ifrz = DRZ_0.ifrz
DRZ_PC_0.kmaxlog = DRZ_0.kmaxlog
DRZ_PC_0.frac_phi = DRZ_0.frac_phi
#   calculate porosity at fracture initiation pressure
porinit = DRZ_PC_0.porosity*np.exp(DRZ_PC_0.comp_rck*DRZ_PC_0.pi_delta/DRZ_PC_0.porosity)
#   calculate n exponential for fractured permeability model
DRZ_PC_0.frac_exp = np.log(10**DRZ_PC_0.kmaxlog/10**DRZ_PC_0.prmx_log)/np.log(DRZ_PC_0.frac_phi/porinit)
# - delete temporary variables
del porinit
#   calculate pore compressibility from newly calc'd porosity
DRZ_PC_0.comp_por = DRZ_PC_0.comp_rck/DRZ_PC_0.porosity

#***********************************************************************
# CAVITY_5 = PCS from -5 yrs to 0 yrs
#***********************************************************************
#   sampled parameters:
#       sat_rbrn
#   no further edits required
#***********************************************************************

#***********************************************************************
# OPS_AREA = operations area from -5 yrs to 0 yrs
#***********************************************************************
#   sampled parameters:
#       none
#   no further edits required
#***********************************************************************

#***********************************************************************
# EXP_AREA = experimental area from -5 yrs to 0 yrs
#***********************************************************************
#   sampled parameters:
#       none
#   no further edits required
#***********************************************************************

#***********************************************************************
# CULEBRA = Culebra member of Rustler
#***********************************************************************
#   sampled parameters:
#       none (for BRAGFLO)
#   no further edits required
#***********************************************************************

#***********************************************************************
# MAGENTA = Magenta member of Rustler
#***********************************************************************
#   sampled parameters:
#       none
#   no further edits required
#***********************************************************************

#***********************************************************************
# DEWYLAKE = Dewey Lake red beds
#***********************************************************************
#   sampled parameters:
#       none
#   no further edits required
#***********************************************************************

#***********************************************************************
# SANTAROS = Santa Rosa
#***********************************************************************
#   sampled parameters:
#       none
#   no further edits required
#***********************************************************************

#***********************************************************************
# WAS_AREA = waste area from 0 yrs to 10000 yrs
#***********************************************************************
#   sampled parameters (hydrologic):
#       sat_rbrn sat_rgas
#   no further edits required
#***********************************************************************
WAS_AREA.creep = True

#***********************************************************************
# DRZ_1 = DRZ around waste from 0 yrs to 10000 yrs
#***********************************************************************
#   sampled parameters:
#       prmx_log
#***********************************************************************
#   correlate porosity to DRZ_0
DRZ_1.porosity = DRZ_0.porosity
#   turn on fracture
DRZ_1.fracture=True
#   correlate fracture parameters to DRZ_0
DRZ_1.pi_delta = DRZ_0.pi_delta
DRZ_1.pf_delta = DRZ_0.pf_delta
DRZ_1.ifrx = DRZ_0.ifrx
DRZ_1.ifry = DRZ_0.ifry
DRZ_1.ifrz = DRZ_0.ifrz
DRZ_1.kmaxlog = DRZ_0.kmaxlog
DRZ_1.frac_phi = DRZ_0.frac_phi
#   calculate porosity at fracture initiation pressure
porinit = DRZ_1.porosity*np.exp(DRZ_1.comp_rck*DRZ_1.pi_delta/DRZ_1.porosity)
#   calculate n exponential for fractured permeability model
DRZ_1.frac_exp = np.log(10**DRZ_1.kmaxlog/10**DRZ_1.prmx_log)/np.log(DRZ_1.frac_phi/porinit)
#   delete temporary variables
del porinit
#   calculate pore compressibility from newly calc'd porosity
DRZ_1.comp_por = DRZ_1.comp_rck/DRZ_1.porosity

#***********************************************************************
# DRZ_PC_1 = DRZ around PCS from 0 yrs to 200 yrs
#***********************************************************************
#   sampled parameters:
#       none
#***********************************************************************
#   correlate permeability to DRZ_1
DRZ_PC_1.prmx_log = DRZ_1.prmx_log
DRZ_PC_1.prmy_log = DRZ_PC_1.prmx_log
DRZ_PC_1.prmz_log = DRZ_PC_1.prmx_log
#   correlate porosity to DRZ_0
DRZ_PC_1.porosity = DRZ_0.porosity
#   turn on fracture
DRZ_PC_0.fracture=True
#   correlate fracture parameters to DRZ_0
DRZ_PC_1.pi_delta = DRZ_0.pi_delta
DRZ_PC_1.pf_delta = DRZ_0.pf_delta
DRZ_PC_1.ifrx = DRZ_0.ifrx
DRZ_PC_1.ifry = DRZ_0.ifry
DRZ_PC_1.ifrz = DRZ_0.ifrz
DRZ_PC_1.kmaxlog = DRZ_0.kmaxlog
DRZ_PC_1.frac_phi = DRZ_0.frac_phi
#   calculate porosity at fracture initiation pressure
porinit = DRZ_PC_1.porosity*np.exp(DRZ_PC_1.comp_rck*DRZ_PC_1.pi_delta/DRZ_PC_1.porosity)
#   calculate n exponential for fractured permeability model
DRZ_PC_1.frac_exp = np.log(10**DRZ_PC_1.kmaxlog/10**DRZ_PC_1.prmx_log)/np.log(DRZ_PC_1.frac_phi/porinit)
# - delete temporary variables
del porinit
#   calculate pore compressibility from newly calc'd porosity
DRZ_PC_1.comp_por = DRZ_PC_1.comp_rck/DRZ_PC_1.porosity

#***********************************************************************
# PCS_T1 = PCS from 0 yrs to 100 yrs
#***********************************************************************
#   sampled parameters:
#       pore_dis, porosity, prmx_log, sat_rbrn, sat_rgas
#   no further edits required
#***********************************************************************

#***********************************************************************
# CONC_PLG = intrusion borehole concrete plugs
#***********************************************************************
#   sampled parameters:
#       prmx_log
#   no further edits required
#***********************************************************************

#***********************************************************************
# BH_OPEN = intrusion borehole open section
#***********************************************************************
#   sampled parameters:
#       none
#   use KRP11 instead of KRP5
#***********************************************************************
BH_OPEN.relp_mod = 11
BH_OPEN.krp_str = 'BH_OPEN_KRP11'

#***********************************************************************
# BH_SAND = intrusion borehole silty sand section
#***********************************************************************
#   sampled parameters:
#       none
#   no further edits required
#***********************************************************************

#***********************************************************************
# BH_CREEP = intrusion borehole silty sand creep closure section
#***********************************************************************
#   sampled parameters:
#       none
#***********************************************************************
#   correlate permeability to BH_SAND minus 1 om
BH_CREEP.prmx_log = BH_SAND.prmx_log-1.0
BH_CREEP.prmy_log = BH_CREEP.prmx_log
BH_CREEP.prmz_log = BH_CREEP.prmx_log

#***********************************************************************
# UNNAMED = Unnamed member of Rustler
#***********************************************************************
#   sampled parameters:
#       none
#   no further edits required
#*********************************************************************

#***********************************************************************
# TAMARISK = Tamarisk member of Rustler
#***********************************************************************
#   sampled parameters:
#       none
#   no further edits required
#*********************************************************************

#***********************************************************************
# FORTYNIN = Forty-Niner member of Rustler
#***********************************************************************
#   sampled parameters:
#       none
#   no further edits required
#*********************************************************************

#***********************************************************************
# REPOSIT = reposit:was_area
#***********************************************************************
#   sampled parameters:
#       none 
#   comments:
#       "reposit:was_area" assigns all waste area parameters to ror
#   copy is done above - no further edits required
#***********************************************************************
REPOSIT.creep = True

#***********************************************************************
# CONC_MON = concrete filled shaft
#***********************************************************************
#   sampled parameters:
#       none 
#***********************************************************************
#   correlate residual brine and gas saturation to SHFTU
#   residual brine and gas saturation
CONC_MON.sat_rbrn = SHFTU.sat_rbrn
CONC_MON.sat_rgas = SHFTU.sat_rgas

#***********************************************************************
# SHFTU = shaft upper
#***********************************************************************
#   sampled parameters:
#       prmx_log sat_rbrn sat_rgas
#***********************************************************************
#   correlate pore distribution to CONC_MON pore distribution
SHFTU.pore_dis = CONC_MON.pore_dis
#   turn on compressibility - Why do you need to turn this on? Emily 5.2.17
SHFTU.compress=True
#   set bulk compressibility - I set this above. Emily 5.2.17
#SHFTU.comp_rck = SHFTU.porosity*SHFTU.comp_por

#***********************************************************************
# SHFTL_T1 = shaft lower from 0 yrs to 200 yrs
#***********************************************************************
#   sampled parameters:
#       prmx_log
#***********************************************************************
#   correlate residual brine and gas saturation to SHFTU
#   residual brine and gas saturation
SHFTL_T1.sat_rbrn = SHFTU.sat_rbrn
SHFTL_T1.sat_rgas = SHFTU.sat_rgas
#   correlate pore distribution to CONC_MON pore distribution
SHFTL_T1.pore_dis = CONC_MON.pore_dis
#   turn on compressibility - Why do you need to turn this on? Emily 5.2.17
SHFTL_T1.compress=True
#   set bulk compressibility - I set this above. Emily 5.2.17
#SHFTL_T1.comp_rck = SHFTL_T1.porosity*SHFTL_T1.comp_por

#***********************************************************************
# SHFTL_T2 = shaft lower from 200 yrs to 10000 yrs
#***********************************************************************
#   sampled parameters:
#       prmx_log
#***********************************************************************
#   correlate residual brine and gas saturation to SHFTU
#   residual brine and gas saturation
SHFTL_T2.sat_rbrn = SHFTU.sat_rbrn
SHFTL_T2.sat_rgas = SHFTU.sat_rgas
#   correlate pore distribution to CONC_MON pore distribution
SHFTL_T2.pore_dis = CONC_MON.pore_dis
#   turn on compressibility - Why do you need to turn this on? Emily 5.2.17
SHFTL_T2.compress=True
#   set bulk compressibility - I set this above. Emily 5.2.17
#SHFTL_T2.comp_rck = SHFTL_T2.porosity*SHFTL_T2.comp_por

#***********************************************************************
# DRZ_OE_1 = DRZ around ops/exp from 0 yrs to 10000 yrs
#***********************************************************************
#   sampled parameters:
#       none
#***********************************************************************
#   correlate permeability to DRZ_1
DRZ_OE_1.prmx_log = DRZ_1.prmx_log
DRZ_OE_1.prmy_log = DRZ_OE_1.prmx_log
DRZ_OE_1.prmz_log = DRZ_OE_1.prmx_log
#   correlate porosity to DRZ_0
DRZ_OE_1.porosity = DRZ_0.porosity
#   turn on fracture
DRZ_PC_0.fracture=True
#   correlate fracture parameters to DRZ_0
DRZ_OE_1.pi_delta = DRZ_0.pi_delta
DRZ_OE_1.pf_delta = DRZ_0.pf_delta
DRZ_OE_1.ifrx = DRZ_0.ifrx
DRZ_OE_1.ifry = DRZ_0.ifry
DRZ_OE_1.ifrz = DRZ_0.ifrz
DRZ_OE_1.kmaxlog = DRZ_0.kmaxlog
DRZ_OE_1.frac_phi = DRZ_0.frac_phi
#   calculate porosity at fracture initiation pressure
porinit = DRZ_OE_1.porosity*np.exp(DRZ_OE_1.comp_rck*DRZ_OE_1.pi_delta/DRZ_OE_1.porosity)
#   calculate n exponential for fractured permeability model
DRZ_OE_1.frac_exp = np.log(10**DRZ_OE_1.kmaxlog/10**DRZ_OE_1.prmx_log)/np.log(DRZ_OE_1.frac_phi/porinit)
#   delete temporary variables
del porinit
#   calculate pore compressibility from newly calc'd porosity
DRZ_OE_1.comp_por = DRZ_OE_1.comp_rck/DRZ_OE_1.porosity

#***********************************************************************
# PCS_T2 = PCS from 100 yrs to 200 yrs
#***********************************************************************
#   sampled parameters:
#       porosity, por2perm
#   comments:
#       calculate prmx_log from porosity and por2perm (sampled) and apply to
#       prmy_log and prmz_log, making sure that permeability does not increase
#       when going from PCS_T1 to PCS_T2
#***********************************************************************
#   set permeability in x as a function of porosity and por2perm
PCS_T2.prmx_log = -21.187*(1.0-PCS_T2.porosity)+1.5353+PCS_T2.por2perm
#   set permeability in x no higher than in PCS_T1
PCS_T2.prmx_log = np.fmin(PCS_T2.prmx_log, PCS_T1.prmx_log)
#   set permeability in y and z same as x
PCS_T2.prmy_log = PCS_T2.prmx_log
PCS_T2.prmz_log = PCS_T2.prmx_log
#   correlate pore distribution to sampled PCS_T1 pore distribution
PCS_T2.pore_dis = PCS_T1.pore_dis
#   correlate residual brine and gas saturation to sampled PCS_T1
#   residual brine and gas saturation
PCS_T2.sat_rbrn = PCS_T1.sat_rbrn
PCS_T2.sat_rgas = PCS_T1.sat_rgas

#***********************************************************************
# PCS_T3 = pcs from 200 yrs to 10000 yrs
#***********************************************************************
#   sampled parameters:
#       porosity
#   comments:
#       calculate prmx_log from porosity (sampled) and por2perm and apply to
#       prmy_log and prmz_log, making sure that permeability does not increase
#       when going from PCS_T2 to PCS_T3
#***********************************************************************
#   set permeability in x as a function of PCS_T3 porosity and PCS_T2 distribution
PCS_T3.prmx_log = -21.187*(1.0-PCS_T3.porosity)+1.5353+PCS_T2.por2perm
#   set permeability in x no higher than in PCS_T2
PCS_T3.prmx_log = np.fmin(PCS_T3.prmx_log, PCS_T2.prmx_log)
#   set permeability in y and z same as x
PCS_T3.prmy_log = PCS_T3.prmx_log
PCS_T3.prmz_log = PCS_T3.prmx_log
#   correlate pore distribution to sampled PCS_T1 pore distribution
PCS_T3.pore_dis = PCS_T1.pore_dis
#   correlate residual brine and gas saturation to sampled PCS_T1
#   brine and gas saturation
PCS_T3.sat_rbrn = PCS_T1.sat_rbrn
PCS_T3.sat_rgas = PCS_T1.sat_rgas

#***********************************************************************
# DRZ_PCS = drz around pcs from 200 yrs to 10000 yrs
#***********************************************************************
#   sampled parameters:
#       prmx_log
#   comments:
#       correlate permeability to sampled DRZ_1 permeability,
#       making sure that permeability does not increase
#       when going from DRZ_1 to DRZ_PCS
#***********************************************************************
#   set permeability in x no higher than in DRZ_1
DRZ_PCS.prmx_log = np.fmin(DRZ_PCS.prmx_log,DRZ_1.prmx_log)
#   set permeability in y and z same as x
DRZ_PCS.prmy_log = DRZ_PCS.prmx_log
DRZ_PCS.prmz_log = DRZ_PCS.prmx_log
#   correlate porosity to sampled DRZ_0 porosity
DRZ_PCS.porosity = DRZ_0.porosity
#   calculate pore compressibility from newly calc'd porosity
DRZ_PCS.comp_por = DRZ_PCS.comp_rck/DRZ_PCS.porosity

#***********************************************************************
# BRINESAL = Salado brine
#***********************************************************************
#   sampled parameters:
#       none
#***********************************************************************
#   nothing yet (tbd)

#***********************************************************************
# REFCON = reference constants
#***********************************************************************
#   sampled parameters:
#       none directly sampled
#       stco_22 calculated from sampled params
#***********************************************************************
# set degradation flag based on probdeg
print ('probdeg length = {}'.format(len(WAS_AREA.probdeg)))
plasidx = np.zeros(len(WAS_AREA.probdeg),'=i4')
for i in xrange(len(plasidx)):
  if (WAS_AREA.probdeg[i] - 2) == 0: plasidx[i] = 1
# cellulose mass
ch_cell = WAS_AREA.cellchw + WAS_AREA.celcchw + WAS_AREA.celechw
rh_cell = WAS_AREA.cellrhw + WAS_AREA.celcrhw + WAS_AREA.celerhw
# rubber mass
ch_rupl = WAS_AREA.rubbchw + WAS_AREA.rubcchw + WAS_AREA.rubechw + \
          REFCON.plasfac * (WAS_AREA.plaschw + WAS_AREA.plscchw + \
                            WAS_AREA.plsechw)
rh_rupl = WAS_AREA.rubbrhw + WAS_AREA.rubcrhw + WAS_AREA.ruberhw + \
          REFCON.plasfac * (WAS_AREA.plasrhw + WAS_AREA.plscrhw + \
                            WAS_AREA.plserhw)
# total mass of cellulosics
wtceltot = ch_cell + rh_cell
# total mass of rubber and plastics
wtrpltot = ch_rupl + rh_rupl
# total mass of biodegradable material
wtbiotot = wtceltot + wtrpltot * plasidx #array
# total cpr assumed for the vector
a1 = wtbiotot/REFCON.mw_cell             #array
# total cpr that could be potentially be consumed during 10000 y
a2 = WAS_AREA.gratmici * REFCON.yrsec * 10000. * wtbiotot #array
# maximum cpr as minimum of assumed and potentially consumed
max_c = np.zeros(len(a1),'=f8')
for i in xrange(len(max_c)):
  max_c[i] = min(a1[i],a2[i])                    #array
# fraction of cpr that could be consumed using nitrate
f_no3 = NITRATE.qinit*6./4.8/max_c       #array
# f_no3 cannot be greater than 1
for i in xrange(len(f_no3)):
  f_no3[i] = min(f_no3[i],1.0)                #array
# f_so4 is assumed to be the remaining mechanism with methanogenesis removed
f_so4 = 1.0 - f_no3                      #array
# moles of water produced per mole of organic carbon
#no need for WAS_AREA.smic_h20, so don't bother with it
REFCON.stco_22 = f_no3*7.4/6. + f_so4*5./6.
#***********************************************************************
# reality check
#param_objs['S_MB139'].display_values()
#param_objs['BRINESAL'].display_values()
#param_objs['SHFTU'].display_values()
#param_objs['SHFTL_T1'].display_values()
#param_objs['CAVITY_1'].display_values()
#param_objs['CAVITY_2'].display_values()
#param_objs['OPS_AREA'].display_values()
#param_objs['DRZ_1'].display_values()
#param_objs['CONC_MON'].display_values()
#param_objs['REPOSIT'].display_values()
#param_objs['PCS_T1'].display_values()
#param_objs['WAS_AREA'].display_values()
#param_objs['DRZ_PC_0'].display_values()
#param_objs['DRZ_OE_0'].display_values()
#param_objs['REFCON'].display_values()
#param_objs['CULEBRA'].display_values()
#param_objs['MAGENTA'].display_values()
#param_objs['FORTYNIN'].display_values()
#param_objs['DEWYLAKE'].display_values()
#param_objs['SANTAROS'].display_values()
#param_objs['TAMARISK'].display_values()
#print('stco_22 type = {}'.format(type(REFCON.stco_22)))
