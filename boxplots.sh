#! /bin/bash 

# Before running this script, you must:
# sftp desired bragflo output (*.bf.h5) from:
# jt:/home1/rsarath/pfd_analysis/bragflo_output
# to:
# ./example_bragflo
# python collect_bragflo_h5.py 1 10
# or something similar with desired bragflo output and paths

# choose bragflo results to compare against
this_dir=$(pwd)
if [[ $this_dir == *"tt04"* ]]; then
  bf_sum_dir=../bragflo_summary_tt_04 #not used for fluxes
else
  bf_sum_dir=../bragflo_summary_orig_data #not used for fluxes
fi
echo ''
echo 'pwd = '$this_dir
echo 'bf output in '$bf_sum_dir
echo ''

output_dir=horsetail_plot_dir
if test -d $output_dir; then
  read -p "$output_dir exists. Overwrite duplicate files? [yes/no]: " stdin
  case $stdin in
    no | n) echo ""
      echo "If you want to keep contents of $output_dir, move it and rerun script."
      echo "Exiting." 
      echo ""
      exit 1
      ;;
    yes | y) echo ""
      echo "Will overwrite duplicate files. Starting..."
      echo ""
      ;;
    *) echo ""
      echo "Enter yes or no. Exiting."
      echo ""
      exit 1
      ;;
  esac
else
  mkdir $output_dir
fi

for s in {1,2,3,4,5,6}; do
  for r in {1,2,3}; do
    for reg in {WAS_AREA,}; do #SPCS,SROR,NROR,OPS,EXP,MPCS,NPCS,SHAFT}; do
      for var in {PRESBRIN,SATBRINE,}; do
        python horsetail.py -f $bf_sum_dir/pfd_analysis_bragflo_summary_scenario$s.h5 \
                            -p s$s \
                            -v all \
                            -s $s \
                            -r $r \
                            -e $reg \
                            -a $var \
                            -d $output_dir \
                            -l boxplots \
#                            -c rainbow
      done
    done
  done
done
