#! /bin/bash 

# plot_horsetail_diff_area_flux.sh
# Glenn Hammond <gehammo@sandia.gov>

# Usage: plot_horsetail_diff_area_flux.sh <output directory>
# before running this script you must calculate differences
# using horsetail_flux.sh (calls horsetail_fluxes.py)

# Use this to tabulate and plot differences calc'd with
# absolute area integration.
# Use plot_horsetail_diff_cum_flux.sh to tabulate and plot cum flux diffs

# choose bragflo results to compare against
this_dir=$(pwd)
if [[ $this_dir == *"tt04"* ]]; then
  bf_sum_dir=../bragflo_summary_tt_04
  bf_dir=/home1/ergiamb/pfd_analysis/bragflo_output_tt_04
else
  bf_sum_dir=../bragflo_summary_orig_data
  bf_dir=/home1/ergiamb/pfd_analysis/bragflo_output_fluxes
fi
echo ''
echo 'pwd = '$this_dir
echo 'bf flux data in '$bf_dir
echo ''
# confirm output directory
output_dir=horsetail_flux_dir
if [ "$1" != "" ]; then
  output_dir=$1
fi
if [ ! -d "$output_dir" ]; then
  echo ""
  echo "Output directory '$output_dir' not found. Exiting."
  echo ""
  echo "Usage: horsetail_diff_cum_flux_plot.sh <output directory>"
  echo ""
  echo "<output directory> must be included if not named 'horsetail_flux_dir'."
  echo ""
  exit 1
fi
# call horsetail_diff_cum_flux.py to make scatter plots of difference metrics
screen_file=plot_horsetail_diff_area_flux.stdout
/bin/rm $screen_file
touch $screen_file
for s in {1,2,3,4,5,6,}; do
  for r in {1,2,3}; do
    for reg in {INSTANT_BRINE_MASS_FLUX,}; do
      for var in {lwb_north_mb138,lwb_north_anhydrite,lwb_north_mb139,lwb_south_mb138,lwb_south_anhydrite,lwb_south_mb139,borehole_culebra,shaft_culebra,}; do
        python horsetail_diff_cum_flux.py -d ${output_dir} \
                                          -s $s \
                                          -r $r \
                                          -e $reg \
                                          -a $var \
                                          -c rainbow \
                                          -t 0.1 \
                                          -z 100. \
                                           | tee -a $screen_file
      done
    done
  done
done
# call generate_plots_of_fluxviols.py to write bash scripts for replotting
wait
python generate_plots_of_fluxviols.py -b "$bf_sum_dir"'/pfd_analysis_bragflo_summary_scenario{}.h5' \
                                      -d $bf_dir \
                                      -f $screen_file \
                                      -v replot_flux_violators.sh \
                                      -n replot_flux_nonviolators.sh \
                                      -x 'rel diff 0.1; abs diff 100 kg'
# call newly created bash scripts to plot horsetails (violators, nonviolators)
wait
chmod uag+x replot_flux_violators.sh
chmod uag+x replot_flux_nonviolators.sh
./replot_flux_violators.sh
./replot_flux_nonviolators.sh


