Instructions for running the BRAGFLO - PFLOTRAN comparison.

FEBRUARY 23, 2018
This repo contains grid and associated region files, initial conditions, etc
for use with BRAGFLO mode.  As long as you are running in BRAGFLO mode, it is
NOT necessary to run prepflotran first for the first 10 vectors.

OPTIONS are for use with pflotran branch glenn/wipp-solution-controls.

Run control can be parallel or serial.
prePFLOTRAN produces a bash shell script that will run time-5y scenario for
each vector, run pf_PFD_restart0y_r{}_v{}.py for each vector, and run each 
vector for each scenario (s1, s2, etc.).

NOTE: The only post-processing included in runcontrol_parallel.sh and
run_control_serial.sh is calculation of volume-averaged quantities using
volume_averages.py.

Other post-processing options:
-Plots of max difference can be made by changing the plotting flag to "yes"
in ./example_bragflo/setup_s1_diff.sh and setup_s2_diff.sh (Step 5) 
-ParaView post-processing of differnces can be done with paraview_diff.sh.
-Comparison of volume-weighted averages can be done with horsetail.sh.

############################ INSTRUCTIONS #############################
0. Install parallel:
  a.) sudo apt-get install parallel (or whatever the command is on Solaris)
  b.) please let me know if parallel isn't an option

1. Using SSH:
  a.) git clone git@gitlab-ex.sandia.gov:wipp/pflotran-bragflo-2d-flared.git
  b.) git clone git@gitlab-ex.sandia.gov:wipp/prepflotran.git

2. Set up PREPFLOTRAN_DIR and PFLOTRAN_DIR
  a.) export PREPFLOTRAN_DIR=/path/to/your/prepflotran/repository
  b.) export PFLOTRAN_SRC=/path/to/your/pflotran/src/directory
(You can add those lines to your .bashrc or .bash_profile and never have to
 export them again.)

3. In your prepflotran/ directory,
  a.) cd test/
  b.) python ../src/prepflotran.py

4. In your pflotran-bragflo-2d-flared/ directory:
  a.) ./cleanup.sh #if necessary/desired
  b.) ./copy_input.sh
  c.) ./runcontrol_parallel.sh OR ./runcontrol_serial.sh

5. At this point in time, comparison to bragflo is set up for v001 (s1/s2)
   and v002 (s1/s2):
  a.) cd ../example_bragflo
  b.) Example bragflo output is provided: bf2_PFD_r1_s1_v001.bf.h5,
      and bf2_PFD_r1_s2_v001.bf.h5, and bf2_init_PFD_r1_s2_v001.inp. 
      Edit the file names in setup_s1_diff.sh, setup_s2_diff.sh,
      and setup_init_diff.sh if yours are different. 
  c.) ./setup_init_diff.sh
  d.) ./setup_s1_diff.sh (set flag to "yes" for plotting)
  e.) ./setup_s2_diff.sh (set flag to "yes" for plotting)

6. Viewing PFLOTRAN results:
  a.) edit setup_xmf.sh to create xmf files for any vectors you'd like to plot
      in ParaView. This is necessary before Step 7.

7. Viewing differences (created in Step 5) in ParaView:
  a.) if you want more than v001, v002, s1, s2:
      sftp desired bragflo output from jt:/home1/rsarath/pfd_analysis/bragflo_output
  b.) complete Steps 5 and 6 (edit scripts to add vectors/scenarios)
  c.) ./paraview_diff.sh (edit script to add vectors/scenarios)

8. Viewing time step history:
  a.) edit setup_timestep.sh to plot time steps as fn of time for any vectors

9. Calculating and plotting volume-averaged quantities:
  a.) runcontrol.sh will have run volume_averages.py for each simulation
  b.) edit plot_vol_ave.py to make line plots of pflotran results alone

10. Making horsetail plots and boxplots of bragflo and pflotran output:
  a.) sftp desired bragflo output from jt:/home1/rsarath/pfd_analysis/bragflo_output
      to ./example_bragflo
  b.) python collect_bragflo_h5.py <scenario #> <num vectors>
  c.) edit horsetail.sh for desired outputs and regions (response functions)
  d.) ./horsetail.sh
  e.) this will also make horsetail plots of difference between bragflo and pflotran
  f.) python horsetail.py -h for help

11. Making barchart of maximum difference for each vector in a scenario/replicate:
  a.) sftp desired bragflo output from jt:/home1/rsarath/pfd_analysis/bragflo_output
      to ./example_bragflo
  b.) python collect_bragflo_h5.py <scenario #> <num vectors>
  c.) python plot_bf_pf_comparison.py -h for help

