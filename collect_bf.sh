#! /bin/bash 

# Before running this script, you must:
# sftp desired bragflo output (*.bf.h5) from:
# jt:/home1/rsarath/pfd_analysis/bragflo_output
# to:
# ./example_bragflo

for s in {1,2,3,4,5,6}; do
  python collect_bragflo_h5.py -i /home1/rsarath/pfd_analysis/bragflo_output \
                               -d ../bragflo_summary_orig_data \
                               -s $s \
                               -v 300
done
