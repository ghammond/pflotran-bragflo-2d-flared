'''bf_outward_flux.py
   read bragflo .bf.h5 history variables to a pandas df.
   calculate cumulative flux away from repository and save to txt file
   in format easily read into df again for later use.
   Emily Stein (ergiamb@sandia.gov)
   6.11.18
'''
import sys
import os
import argparse
import numpy as np
from h5py import File
#import h5py
import pandas as pd
print(pd.__version__)
import matplotlib.pyplot as plt

#copy the following from collect_bragflo_h5.py
flux_dict = {#column index:name
               231+1:'lwb_south_mb139',
               232+1:'lwb_south_anhydrite',
               233+1:'lwb_south_mb138',
               234+1:'lwb_south_culebra',
               235+1:'lwb_north_mb139',
               236+1:'lwb_north_anhydrite',
               237+1:'lwb_north_mb138',
               238+1:'lwb_north_culebra',
               260+1:'borehole_mb138',
               261+1:'borehole_salado',
               262+1:'borehole_culebra',
               263+1:'shaft_mb138',
               264+1:'shaft_salado',
               265+1:'shaft_culebra',
              }
sorted_keys = sorted(flux_dict.keys())
def read_bf_history(input_dir,replicate,scenario,vector):
  '''load desired history variables into a pandas df
     in this case fluxes with unit conversions -> m3 to kg and sec to yr
  '''
  print('processing r{} s{} v{}'.format(replicate,scenario,vector))
  # Unit conversion
  reference_brine_density = 1220. # kg/m^3 1.2200E+03
  sec_per_yr = 3.1536e7
  # make list of column headers
  obs_list = ['TIMEYR']
  #obs_list = ['TIMESEC']
  for key in sorted_keys:
    obs_list.append('{} Water [kg/y]'.format(flux_dict[key]))
  # read h5 file
  hf = File(os.path.join(input_dir,'bf2_PFD_r{}_s{}_v{:0>3}.bf.h5'.format(replicate,scenario,vector)),'r')
  dataset = '/output_tables/history_variables_table'
  df = pd.DataFrame(data=hf[dataset][:,sorted_keys]*reference_brine_density*sec_per_yr,
                    index=hf[dataset][:,0]/sec_per_yr,
                    columns=obs_list[1:])
  hf.close()

  #print df
  return df 

def calc_cumulative_fluxes(output_dir,df,replicate,scenario,vector):
  '''from existing pandas df of flux rates (instantaneous fluxes), 
     calculate cumulative fluxes and write to txt file
     write both total cumulative flux and outward only cumulative flux
     at all time steps
  '''
  # cumulative flux (both positive and negative)
  dt = df.index[1:] - df.index[:-1]
  dt_df = pd.DataFrame(data=dt.values,index=df.index[1:],
                       columns = ['dt_flow [y]'])
  temp_df = df.iloc[1:].copy()
  for col in temp_df: 
    temp_df[col] = temp_df[col]*dt
  cumflux_df = pd.DataFrame(data=temp_df.cumsum(axis=0).values,
                            index=temp_df.index,
                            columns=['{} Water [kg]'.format(x.split()[0]) for x in temp_df.columns])
  # cumulative flux - outward (away from repository)
  for col in temp_df.columns: #it's already multiplied by dt
    if 'south' in col:
      temp_df[col] = temp_df[col].where(temp_df[col]<0.,other=0.) * -1.
    else:
      temp_df[col] = temp_df[col].where(temp_df[col]>0.,other=0.)
  cumout_df = pd.DataFrame(data = temp_df.cumsum(axis=0).values,
                           index=temp_df.index,
                           columns = ['{} Water Out [kg]'.format(x.split()[0]) for x in temp_df.columns])
  # join these into one df
  df = df.join(cumflux_df)
  df = df.join(cumout_df)
  df = df.join(dt_df)
  # write df to text file
  filename = os.path.join(output_dir,'bf2_PFD_r{}_s{}_v{:0>3}_fluxes.txt'.format(replicate,scenario,vector))
  df.to_csv(filename,sep=' ',index_label='Time [y]')

  return df
###############################################################################
if __name__ == '__main__':
  bf_dir = '/home1/rsarath/pfd_analysis/bragflo_output'
  #bf_dir = '/home1/ergiamb/pfd_analysis/bragflo_output_tt_04'
  #bf_out = bf_dir
  #bf_out = '/home1/ergiamb/pfd_analysis/bragflo_output_fluxes_test'
  bf_out = '/home1/ergiamb/pfd_analysis/bragflo_output_fluxes'
#  df = read_bf_history(bf_dir,1,2,1)
#  calc_cumulative_fluxes(bf_out,df,1,2,1)
  for s in range(6):
    for r in range(3):
      for v in range(100):
        df = read_bf_history(bf_dir,r+1,s+1,v+1)
        calc_cumulative_fluxes(bf_out,df,r+1,s+1,v+1)
        
