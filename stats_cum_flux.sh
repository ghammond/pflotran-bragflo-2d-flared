#!/bin/bash

# stats_cum_flux.sh
# call stats_cum_flux.py for however many locations you give it
# before running this script you must ./horsetail_flux.sh

# confirm output directory
output_dir=horsetail_flux_dir
if test -d $output_dir; then
  read -p "$output_dir exists. Overwrite duplicate contents? [yes/no]: " stdin
  case $stdin in
    no | n) echo ""
      echo "If you want to keep $output_dir, move it and rerun script."
      echo "Exiting." 
      echo ""
      exit 1
      ;;
    yes | y) echo ""
      echo "Will overwrite duplicate contents. Starting..."
      echo ""
      ;;
    *) echo ""
      echo "Enter yes or no. Exiting."
      echo ""
      exit 1
      ;;
  esac
else
  mkdir $output_dir
fi
#call python script
#for var in {borehole_culebra,shaft_culebra,}; do
for var in {lwb_north_mb138,lwb_north_anhydrite,lwb_north_mb139,lwb_south_mb138,lwb_south_anhydrite,lwb_south_mb139,borehole_culebra,shaft_culebra,}; do
  python stats_cum_flux.py -d $output_dir \
                           -a $var
done
