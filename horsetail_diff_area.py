'''horsetail_diff_area.py

   Plots the time-averaged areas between horsetails. 
   The difference is normalized by the time-averaged area under 
   the bragflo curve.

   Glenn Hammond <gehammo@sandia.gov>
   Emily Stein <ergiamb@sandia.gov>
'''

import sys
import os
import shutil
import argparse

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import pandas as pd
import horsetail as ht

#mpl.rcParams['font.size'] = 12

#limits for log scale plotting
diff_lim_dict = {#variable,#limit tuple
                 'PRESBRIN':(1.e1,1.e6),
                 'SATBRINE':(1.e-7,1.e0),
                 'POROS':(1.e-7,1.e0),
                 'BRINEVOL':(1.e-2,1.e4),
                }
bfvalue_lim_dict = {#variable,#limit tuple
                    'PRESBRIN':(1.e4,5.e7),
                    'SATBRINE':(1.e-4,1.1e0),
                    'POROS':(1.e-2,1.1e0),
                    'BRINEVOL':(1.e-1,1.e5),
                   }
flux_lim = (1.e-4,3.e8)
fluxdiff_lim = (1.e-4,1.e7)
reldiff_lim = (1.e-6,1.e2)

class HorsetailAreaPlot(object):
  '''Plots scatter plot of areas for individual variables
     Requires knowledge of columns in input file, which are:
     Vector DiffArea_per_time BragfloArea_per_time Diff_over_Bragflo
     Will make up and use short header names within this script.
     see https://stackoverflow.com/questions/5147112/how-to-put-individual-tags-for-a-scatter-plot
  '''
  def  __init__(self,directory,scenario,replicate,
                region,variable,colorflag='rainbow',
                reldiff_threshold=0.03,effectively_zero_diff=0.01,
                plot_string=None):
    self.directory = directory
    self.s = scenario
    self.r = replicate
    self.reg = region
    self.var = variable
    self.string = 'r{}_s{}_{}_{}'.format(self.r,self.s,self.reg,self.var)
    self.titlestring = 'PFD_r{}_s{} {} {}'.format(self.r,self.s,self.reg,self.var)
    sys.stdout.write('\n{:<30}'.format(self.string))

    self.load_df()
    self.threshold = reldiff_threshold
    self.effectively_zero_diff = effectively_zero_diff
    self.plot_string = plot_string

    if colorflag == 'blue_and_red':
      self.colors = ['blue' for x in xrange(100)]
    elif colorflag == 'rainbow':
      self.colors = []
      tencolors = ['red','darkorange','gold','limegreen','green',
                'turquoise','teal','blue','purple','magenta']
      for x in range(10): #100 colors
        self.colors.extend(tencolors)
    else:
      print('ERROR: Unrecognized choice for color {}.'.format(colorflag))
      print('Choose "rainbow" or "blue_and_red". Exiting.')
      sys.exit()
    self.edgecolors = ['black' for x in xrange(100)]
    self.labels = ['{}'.format(i) for i in self.df.index]

  def load_df(self,):
    '''load _diff_area.txt into pandas DataFrame
       (has fewer columns than _diff_cum.txt in horsetail_diff_cum_flux.py)
    '''
    string = 'r{}_s{}_{}_{}_diff_area.txt'.format(self.r,self.s,self.reg,self.var)
    self.area_filename = os.path.join(self.directory,string)
    self.df = pd.read_table(self.area_filename,sep=' ',engine='python',
              header=None,index_col=0,skiprows=5,skip_footer=4,
              names=['vector','difference','bragflo','relative_difference'],)

  def find_violators(self,):
    '''Find vectors that violate both rel_diff threshold and
       abs_diff effectively_zero threshold. List them as violators
       for later replotting (generate_plots_of_violators.py)
       of line plots v. time
    '''
    self.violators = []
    self.scaled_values = self.df.abs()['relative_difference'].copy()
    for i in xrange(len(self.scaled_values)):
      if (self.df.abs()['difference'].iloc[i] < self.effectively_zero_diff):
        self.scaled_values.iloc[i] = 0.
      if abs(self.scaled_values.iloc[i]) >= self.threshold:
        self.violators.append([self.labels[i],self.scaled_values.iloc[i]])
    if len(self.violators) > 0:
      print('Thresholds of {:7.1e}/{:7.1e} exceeded for {} vector(s):'.format(
                                                           self.threshold,
                                                           self.effectively_zero_diff,
                                                           len(self.violators)))
      for i in xrange(len(self.violators)):
        print('\t\t\t\t\t{:>4} : {:11.4e}'.format(self.violators[i][0],
                                                  self.violators[i][1]))
      print('\n')
    else:
      sys.stdout.write('All pass!\n')

  def reldiff_v_vector(self,):
    '''scatter plot of relative difference versus vector number
       uses self.scaled_values which has zeros where
       DiffArea/time < effectively_zero, so we are plotting real values
       only for the violators found in find_violators()
    '''
    fancy_slice = [int(i)-1 for i in self.scaled_values.index]
    localcolors = np.array(self.colors)[fancy_slice] #fancy slicing creates a copy
    edgecolors = ['black' for x in xrange(len(localcolors))]

    fig = plt.figure(figsize=(12,8))
    ax = fig.add_subplot(111)
    plt.title(self.titlestring)
    plt.xlabel('Vector #')
#    if 'FLUX' in self.reg: #planning ahead for inheritance to flux class
#      plt.ylabel('Relative Difference in Liquid Flux ')
#    else:
    plt.ylabel('Relative Difference: Area Between the Curves / Area Under BRAGFLO Curve')
    for i in xrange(self.scaled_values.size):
      if self.scaled_values.iloc[i] == 0.:
        localcolors[i] = 'white'
      elif self.scaled_values.iloc[i] > self.threshold:
        edgecolors[i] = 'red'
    plt.scatter(self.df.index,self.scaled_values,s=100,c=localcolors,edgecolors=edgecolors)
    for label, x, y in zip(self.labels,self.df.index,self.scaled_values):
      if abs(y) >= self.threshold:
        plt.annotate(
          label,
          xy=(x, y), xytext=(-10,10),
          textcoords='offset points', ha='right', va='bottom')
    if max(self.scaled_values) < 0.05:
      plt.ylim(-0.001,0.05)
      plt.yticks([x*0.01 for x in range(6)])
      plt.grid(axis='y')
    if len(self.scaled_values) == 100:
      plt.xlim(0,101)
    plt.grid()
    x0,y0 = np.array([[0,101],[3.e-2,3.e-2]])
    line0 = mlines.Line2D(x0,y0,lw=2.,color='black',alpha=0.5)
    ax.add_line(line0)
    if self.plot_string:
      plt.text(0.02,0.02,self.plot_string,transform=ax.transAxes) 
    plt_filename = os.path.join(self.directory,self.string +'_vecscatter.png')
    plt.savefig(plt_filename,bbox_inches='tight')
    #plt.show()
    plt.close('all')

  def reldiff_v_absdiff(self,):
    '''plot normalized area (relative difference) versus absolute area
       (absolute difference)
    '''
    fancy_slice = [int(i)-1 for i in self.scaled_values.index]
    localcolors = np.array(self.colors)[fancy_slice] #fancy slicing creates a copy
    edgecolors = ['black' for x in xrange(len(localcolors))]
    for i in xrange(len(self.scaled_values)):
      if self.scaled_values.iloc[i] > self.threshold:
        edgecolors[i] = 'red'

    fig = plt.figure(figsize=(12,8))
    ax = fig.add_subplot(111)
    plt.title(self.titlestring)
    plt.scatter(x=self.df.abs()['difference'],y=self.df.abs()['relative_difference'],
                               c=localcolors,s=100,edgecolors=edgecolors)
    for label, x, y in zip(self.labels,self.df.abs()['difference'],self.df.abs()['relative_difference']):
      if abs(y) >= self.threshold and abs(x) > self.effectively_zero_diff:
        plt.annotate(
          label,
          xy=(x, y), xytext=(10,10),
          textcoords='offset points', ha='right', va='bottom')
    plt.xscale('log')
    plt.yscale('log')
    plt.grid()
    #x,y limits and axis labels specific to variable.
    if 'FLUX' in self.reg:
      #if self.df['difference'].abs().max() < 1.e3:
      #  x0,y0 = np.array([[1.e-7,1.e3],[self.threshold,self.threshold]])
      #else:
      x0,y0 = np.array([fluxdiff_lim,[self.threshold,self.threshold]])
      x1,y1 = np.array([[self.effectively_zero_diff,self.effectively_zero_diff],
                        reldiff_lim])
      plt.xlabel('Difference: Area Between the Curves [kg]')
      plt.ylabel('Relative Difference: Area Between the Curves / Area Under BRAGFLO Curve')
    else:
      x0,y0 = np.array([diff_lim_dict[self.var],[self.threshold,self.threshold]])
      x1,y1 = np.array([[self.effectively_zero_diff,self.effectively_zero_diff],
                        reldiff_lim])
      plt.xlabel('Difference: Area Between the Curves / Time')
      plt.ylabel('Relative Difference: Area Between the Curves / Area Under BRAGFLO Curve')
    plt.xlim(x0)
    plt.ylim(y1)
    line0 = mlines.Line2D(x0,y0,lw=2.,color='black',alpha=0.5)
    line1 = mlines.Line2D(x1,y1,lw=2.,color='black',alpha=0.5)
    ax.add_line(line0)
    ax.add_line(line1)
    if self.plot_string:
      plt.text(0.02,0.02,self.plot_string,transform=ax.transAxes) 
    plt_filename = os.path.join(self.directory,self.string +'_difscatter.png')
    plt.savefig(plt_filename,bbox_inches='tight')
    #plt.show()
    plt.close('all')

  def reldiff_v_absvalue(self,):
    '''plot normalized area (relative difference) versus absolute
       bragflo value
    '''
    fancy_slice = [int(i)-1 for i in self.scaled_values.index]
    localcolors = np.array(self.colors)[fancy_slice] #fancy slicing creates a copy
    edgecolors = ['black' for x in xrange(len(localcolors))]
    for i in xrange(len(self.scaled_values)):
      if self.scaled_values.iloc[i] > self.threshold:
        edgecolors[i] = 'red'

    fig = plt.figure(figsize=(12,8))
    ax = fig.add_subplot(111)
    plt.title(self.titlestring)
    plt.scatter(x=self.df.abs()['bragflo'],y=self.df.abs()['relative_difference'],
                               c=localcolors,s=100,edgecolors=edgecolors)
    for label, d, x, y in zip(
                              self.labels,
                              self.df.abs()['difference'],
                              self.df.abs()['bragflo'],
                              self.df.abs()['relative_difference']):
      if abs(y) >= self.threshold and abs(d) > self.effectively_zero_diff:
        plt.annotate(
          label,
          xy=(x, y), xytext=(10,10),
          textcoords='offset points', ha='right', va='bottom')
    plt.xscale('log')
    plt.yscale('log')
    plt.grid()
    #x,y limits and axis labels specific to variable.
    if 'FLUX' in self.reg:
      x0,y0 = np.array([flux_lim,[self.threshold,self.threshold]])
      x1,y1 = np.array([[self.effectively_zero_diff,self.effectively_zero_diff],
                        reldiff_lim])
      plt.xlabel('Area Under BRAGFLO Curve [kg]')
      plt.ylabel('Relative Difference: Area Between the Curves / Area Under BRAGFLO Curve')
    else:
      x0,y0 = np.array([bfvalue_lim_dict[self.var],[self.threshold,self.threshold]])
      x1,y1 = np.array([[self.effectively_zero_diff,self.effectively_zero_diff],
                        reldiff_lim])
      plt.xlabel('Area Under BRAGFLO Curve / Time')
      plt.ylabel('Relative Difference: Area Between the Curves / Area Under BRAGFLO Curve')
    plt.xlim(x0) #(1.e-4,2.5e8)
    plt.ylim(y1) #(1.e-6,1.e2)
    line0 = mlines.Line2D(x0,y0,lw=2.,color='black',alpha=0.5)
    line1 = mlines.Line2D(x1,y1,lw=2.,color='black',alpha=0.5)
    ax.add_line(line0)
    ax.add_line(line1)
    if self.plot_string:
      plt.text(0.02,0.02,self.plot_string,transform=ax.transAxes) 
    plt_filename = os.path.join(self.directory,self.string +'_valscatter.png')
    plt.savefig(plt_filename,bbox_inches='tight')
    #plt.show()
    plt.close('all')

###############################################################################
if __name__ == '__main__':
  '''get command line arguments and run stuff
  '''
  string1 = ','.join([k for k in ht.var_dict.keys()])
  string2 = ','.join([k for k in diff_lim_dict.keys()])

  parser = argparse.ArgumentParser()
  parser.add_argument('-d','--horsetail_plot_dir',
                      help='directory containing _diff_area.txt files' +
                           ' [default = ./horsetail_plot_dir]',
                      default = 'horsetail_plot_dir',
                      required=False,)
  parser.add_argument('-s','--scenario',
                      help='int (1 thru 6) specifying scenario' +
                           ' [default = 1]',
                      default = 1,
                      required = False,)
  parser.add_argument('-r','--replicate',
                      help='int (1 thru 3) specifying replicate' +
                           ' [default = 1]',
                      default = 1,
                      required = False,)
  parser.add_argument('-e','--region',
                      help='To plot vol-ave output, string must exactly equal one of: ' +
                            string1 +
                           '\n[default = WAS_AREA]',
                      default='WAS_AREA',
                      required = False,)
  parser.add_argument('-a','--variable',
                      help='To plot vol-ave output, string must exactly equal one of: ' +
                            string2 +
                           '\n[default = PRESBRIN]',
                      default='PRESBRIN',
                      required = False,)
  parser.add_argument('-c','--colors',
                      help='choose "rainbow" or "blue_and_red"' +
                           ' [default = rainbow]',
                      default='rainbow',
                      required=False,)
  parser.add_argument('-t','--threshold',
                      help='rel diff threshold above which flag violators' +
                           ' [default = 0.03]',
                      default = 0.03,
                      required = False,)
  parser.add_argument('-z','--effectively_zero',
                      help='abs diff threshold below which do not flag violators' +
                           ' [default = 0.01]' +
                           ' which is good for saturation but not for fluxes',
                      default = 0.01,
                      required = False,)
  parser.add_argument('-x','--text_for_plot',
                      help='optional string to add to plots' +
                           ' [default = None]',
                      default = None,
                      required = False,)

  args = parser.parse_args()

  diff = HorsetailAreaPlot(directory=args.horsetail_plot_dir,
                    scenario=int(args.scenario),
                    replicate=int(args.replicate),
                    region=args.region,
                    variable=args.variable,
                    colorflag=args.colors,
                    reldiff_threshold=float(args.threshold),
                    effectively_zero_diff=float(args.effectively_zero),
                    plot_string = args.text_for_plot)
  diff.find_violators()
  #diff.reldiff_v_vector()
  diff.reldiff_v_absdiff()
  #diff.reldiff_v_absvalue()
    
