
#!/bin/bash
#Edit the path to bash if necessary for your machine.

screen_file=volave.stdout
/bin/rm $screen_file
touch $screen_file
for s in {s1,s2,s3,s4,s5,s6,}; do
  cd $s
    for r in {1,2,3,}; do
      for v in {1..100}; do
        printf -v v "%03d" "$v"
        echo "Running pf_PFD_r${r}_${s}_v${v}.in"
        python ../volume_averages.py pf_PFD_r${r}_${s}_v${v}-obs-0.tec \
               | tee -a ../$screen_file
      done
    done
  cd ..
done
