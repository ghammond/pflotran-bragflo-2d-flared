'''
Emily 5.30.18
if you forget to pipe volave.sh output to a file
you can use this to get the last times in each simulation
Usage is limited to a single pflotran-obs-*.tec file.
use a shell script loop (lastline.sh > lastline.txt)
then grep -v E+04 lastline.txt
'''

import sys
import os
import numpy

def lastline():
  infilename = sys.argv[1] #works for one obs file
  if len(sys.argv) != 2:
    print('ERROR: command line arguments not provided.\n')
    print('Usage: python lastline.py pflotran-obs-0.tec')
    sys.exit(0)

  #loadtxt results in rows of data, not columns, very confusing
  data = numpy.loadtxt(infilename,skiprows=1,dtype=str) #returns ndarray
  ntimes = data.shape[0] #length of row in data
  noutputs = data.shape[1] #length of column in data
  last_time = data[-1,0]
  print('In lastline(), {}, ntimes = {}, last time = {}'.format(infilename,ntimes,last_time))

if __name__ == '__main__':
  lastline()
  #print('done') 
