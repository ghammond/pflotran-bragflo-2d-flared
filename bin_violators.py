'''horsetail_diff_area.py

   Bins the simulations that violate performance metrics by scenario, 
   replicate, vector, region and variable.

   Glenn Hammond <gehammo@sandia.gov>
'''

import sys
import os
import shutil
import argparse

def binit(tuples,sort,merge_redundant,scenario,replicate,region,
          variable,vector,min_tolerance,max_tolerance):
  min_error = 0
  max_error = 'inf' 
  if min_tolerance != 'all':
    min_error = min_tolerance
  if max_tolerance != 'all':
    max_error = max_tolerance
  if False:
    print('')
    print('scenario={},replicate={},region={},variable={},vector={}'.format(\
          scenario,replicate,region,variable,vector))
    print('  difference: {} - {}'.format(min_error,max_error))
  violating_tuples = []
  for tple in tuples:
    region_found = False
    if type(region) is list:
      region_found = False
      for r in region:
         if r == tple[2]:
           region_found = True
           break
    else:
      region_found = (tple[2] == region or region == 'all')
    if (tple[0] == scenario or scenario == 'all' and \
       (tple[1] == replicate or replicate == 'all') and \
       region_found and \
       (tple[3] == variable or variable == 'all') and \
       (tple[4] == vector or vector == 'all') and \
       (tple[5] > min_tolerance or min_tolerance == 'all') and \
       (tple[5] <= max_tolerance or max_tolerance == 'all')):
      violating_tuples.append(tple)
  if sort:
    converged = False
    while not converged:
      converged = True
      for i in range(len(violating_tuples)-1):
        if violating_tuples[i][5] < violating_tuples[i+1][5]:
          converged = False
          temp_tple = violating_tuples[i]
          violating_tuples[i] = violating_tuples[i+1]
          violating_tuples[i+1] = temp_tple
  if remove_redundant and len(violating_tuples) > 1:
    # remove violators that have redundant scenario, replicate, and vector
    non_redundant_violators = []
    non_redundant_violators.append(violating_tuples[0])
    for tple in violating_tuples:
      redundant = False
      for tple2 in non_redundant_violators:
        if tple[0] == tple2[0] and \
           tple[1] == tple2[1] and \
           tple[4] == tple2[4]:
          redundant = True
          break
      if not redundant:
        non_redundant_violators.append(tple)
    if False:
      print('  {} redundant violators'.format(len(violating_tuples)-
                                    len(non_redundant_violators)))
    violating_tuples = non_redundant_violators
  #print('  {} violator(s)'.format(len(violating_tuples)))
  if len(violating_tuples) > 0:
    if False:
      print('    [scenario,replicate,region,variable,vector,difference]')
      for tple in violating_tuples:
        sys.stdout.write('        ')
        print(tple)

    if True:
      print('\nRegion(s): {}'.format(region))
      print('Variable: {}'.format(variable))
      print('Number of violators: {}'.format(len(violating_tuples)))
      print('        \ts\tr\tv\tdifference')
      for tple in violating_tuples:
        sys.stdout.write('        ')
        print('        {}\t{}\t{}\t{}'.format(tple[0],tple[1],tple[4],tple[5]))

tuples = []

region_list = {'WAS_AREA':[],
               'SPCS':[],
               'SROR':[],
               'MPCS':[],
               'NROR':[],
               'NPCS':[],
               'OPS':[],
               'SHAFT':[],
               'EXP':[]}

variable_list = {'PRESBRIN':[],
                 'SATBRINE':[],
                 'PRESGAS':[],
                 'SATGAS':[],
                 'POROS':[],
                 'BRINEVOL':[]}

scenario_list = []
for i in range(6+1):
  scenario_list.append([])

replicate_list = []
for i in range(3+1):
  replicate_list.append([])

vector_list = []
for i in range(100+1):
  vector_list.append([])

filename = 'plot_horsetail_diff_area.stdout'


f = open(filename,'r')
for line in f:
  if line.startswith('r'):
    w = line.split('Threshold')
    if len(w) > 1:
      w = w[0].strip().split('_')
      r = int(w[0].split('r')[1])
      s = int(w[1].split('s')[1])
      if len(w) > 4:
        reg = "_".join(w[2:len(w)-1])
        var = w[len(w)-1]
      else:
        reg = w[2]
        var = w[3]
  elif line.split(':')[0].lstrip().rstrip().isdigit():
    w = line.split(':')
    vector = int(w[0])
    area = float(w[1])
    
    tple = [s,r,reg,var,vector,area]
    region_list[reg].append(tple)
    variable_list[var].append(tple)
    scenario_list[s].append(tple)
    replicate_list[r].append(tple)
    vector_list[vector].append(tple)
    tuples.append(tple)
f.close()    

#print(region_list['WAS_AREA'])
#print('')

sort = False
remove_redundant = True
all = 'all'
scenario = all
replicate = all
region = all
variable = all
vector = all
min_tolerance = all
max_tolerance = all

region_list = ['WAS_AREA','NROR','SROR']
variable_list = ['PRESBRIN','SATBRINE']
min_tol_list = [0.1,0.03,0.01]
max_tol_list = [all,0.1,0.03]

if False:
  for region in region_list:
    for variable in variable_list:
      for i in range(len(min_tol_list)):
        binit(tuples,sort,remove_redundant,scenario,replicate,region,
              variable,vector,min_tol_list[i],max_tol_list[i])

print('! ----------------------------------------------------------------- !')
print('! ----------------------------------------------------------------- !')

region_list = ['SPCS','MPCS','NPCS','OPS','SHAFT','EXP']
#min_tol_list = [0.5,0.2,0.1,0.05,0.01]
#max_tol_list = [all,0.5,0.2,0.1,0.05]
min_tol_list = [0.5,0.2,0.1]
max_tol_list = [all,0.5,0.2]
if False:
  for region in region_list:
    for variable in variable_list:
      for i in range(len(min_tol_list)):
        binit(tuples,sort,remove_redundant,scenario,replicate,region,
              variable,vector,min_tol_list[i],max_tol_list[i])

print('! ----------------------------------------------------------------- !')
print('! ################################################################# !')
print('Sorted results by binned region and variable')
scenario = all
replicate = all
region = all
variable = all
vector = all
min_tolerance = all
max_tolerance = all
sort = True
remove_redundant = True

if False:
  region_list = ['WAS_AREA','NROR','SROR']
  variable = 'SATBRINE'
  binit(tuples,sort,remove_redundant,scenario,replicate,region_list,
        variable,vector,min_tolerance,max_tolerance)

  variable = 'PRESBRIN'
  binit(tuples,sort,remove_redundant,scenario,replicate,region_list,
        variable,vector,min_tolerance,max_tolerance)
  
  region_list = ['SPCS','MPCS','NPCS','OPS','SHAFT','EXP']
  
  variable = 'SATBRINE'
  binit(tuples,sort,remove_redundant,scenario,replicate,region_list,
        variable,vector,min_tolerance,max_tolerance)

  variable = 'PRESBRIN'
  binit(tuples,sort,remove_redundant,scenario,replicate,region_list,
        variable,vector,min_tolerance,max_tolerance)

region_list = ['WAS_AREA','NROR','SROR','SPCS','MPCS',
               'NPCS','OPS','SHAFT','EXP']
variable_list = ['PRESBRIN','SATBRINE']
for region in region_list:
  for variable in variable_list:
    binit(tuples,sort,remove_redundant,scenario,replicate,region,
          variable,vector,min_tolerance,max_tolerance)

print('done')
