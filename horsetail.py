'''Uncertainty quantification of bragflo/pflotran outputs:
   horsetail plots of bragflo results,
   horsetail plots of pflotran results
   boxplots of bragflo,
   boxplots of pflotran,
   horsetail plots of differences
   horsetail plots of relative differences

   Emily Stein (ergiamb@sandia.gov)
   Glenn Hammond (gehammo@sandia.gov)
'''

import sys
import os
import shutil
import argparse

import numpy as np
import h5py
import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import abs_integral_area as aia

mpl.rcParams['lines.markersize'] = 6
mpl.rcParams['font.size'] = 12

#global variables
reg_dict = { #bf:pf
             'WAS_AREA':'rWAS_AREA',
             'SPCS':'rSPCS',
             'SROR':'rSROR',
             'MPCS':'rMPCS',
             'NROR':'rNROR',
             'NPCS':'rNPCS',
             'OPS':'rOPS_AREA',
             'SHAFT':'rSHAFT_unofficial',
             'EXP':'rEXP_AREA',
            }
var_dict = { #bf:pf
             'PRESBRIN':'VA Liquid Pressure [Pa]',
             'SATBRINE':'VA Liquid Saturation',
             'PRESGAS':'VA Gas Pressure [Pa]',
             'SATGAS':'VA Gas Saturation',
             'POROS':'VA Effective Porosity',
             'BRINEVOL':'Brine Volume',
            }
ylim_dict = { #bf:limits
             'PRESBRIN':(0.e6,1.8e7),
             'SATBRINE':(-0.05,1.05),
             'PRESGAS':(0.e6,1.8e7),
             'SATGAS':(-0.05,1.05),
             'POROS':(-0.05,1.05),
             'BRINEVOL':(0.,1.e6), #useless limit
            }
threshold_dict = {
                  'PRESBRIN':5.e5,
                  'SATBRINE':0.125,
                  'POROS':0.125,
                  'BRINEVOL':1.e2, #untested threshold
                  'percent':5.,
                 }
                 
class HorsetailPlot(object):
  '''Uncertainty analysis of bragflo and pflotran outputs
     must choose one scenario, one replicate, one region,
     one output variable at a time
  '''
  def  __init__(self,bf_file='pfd_analysis_bragflo_summary.h5',
                pf_dir='s1',
                scenario=1,replicate=1,vectors='all',
                region='WAS_AREA',variable='PRESBRIN',
                times=[0.,99.,349.,549.,750.,999.,1199.,1400.,1999.,3000.,10000.],
                colors='blue_and_red',
                plot_string=None):
    self.bf_h5 = h5py.File(bf_file,'r')
    self.pf_dir=pf_dir
    self.s= scenario
    self.r= replicate
    self.reg = region
    self.var = variable
    self.plot_string = plot_string
    #build lists of vectors and run indices
    if vectors == ['all']:
      self.vectors = [x+1 for x in xrange(100)]
    else: #no real type checking here, could backfire
      self.vectors = [int(x) for x in vectors]
    self.run_idx = []
    for v in self.vectors:
      self.run_idx.append((v-1) + 100*(self.r-1))
    #associate pf terms with bf terms
    self.reg_dict = reg_dict
    self.var_dict = var_dict
    self.ylim_dict = ylim_dict
    self.times = times
    self.ntimes = len(self.times)
    if colors == 'blue_and_red': #this could be more elegant cycle but whatever
      self.bf_color = ['blue' for x in range(100)] #len(self.vectors))]
      self.pf_color = ['red' for x in range(100)] #len(self.vectors))]
      self.bf_linestyle = '-'
      self.pf_linestyle = '-'
      self.bf_marker = 'x'
      self.pf_marker = '+'
    elif colors == 'monochromatic':
      self.bf_color = ['grey' for x in range(100)] #len(self.vectors))]
      self.pf_color = ['black' for x in range(100)] #len(self.vectors))]
      self.bf_linestyle = '-'
      self.pf_linestyle = '-'
      self.bf_marker = 'None'
      self.pf_marker = 'None'
    elif colors == 'rainbow':
      self.bf_color = []
      tencolors = ['red','darkorange','gold','limegreen','green',
                       'turquoise','teal','blue','purple','magenta']
      for x in xrange(10): #build list of 100
        self.bf_color.extend(tencolors)
      self.pf_color = self.bf_color
      self.bf_linestyle = '-'
      self.pf_linestyle = '-'
      self.bf_marker = 'x'
      self.pf_marker = '+'
    else:
      print('ERROR: unrecognized color choice {}'.format(colors))
      print('choose -c blue_and_red OR -c rainbow. Exiting.')
      sys.exit()
    try:
      self.string = '{} {}'.format(self.var_dict[self.var],self.reg_dict[self.reg])
    except KeyError:
      pass
    
  def bf_load_var(self,run_idx):
    '''load entire time series of single bragflo output variable
       into a pandas DataFrame
       Source is an hdf5 file containing outputs for all simulations
    '''
    grp_scenario = self.bf_h5['/Scenario {:d}'.format(self.s)]
    bf_time_yr = grp_scenario['TIMES_YEARS'][...]
    bf_output = grp_scenario[self.reg][self.var][run_idx,:][...]
    #label with pflotran terms so headers match in the two dfs
    df = pd.DataFrame(data=bf_output,index=bf_time_yr,columns=[self.string])
    #print('bf run_idx{} max:{:e}'.format(run_idx,df[self.string].max()))
    #print('bf run_idx{} min:{:e}'.format(run_idx,df[self.string].min()))
    return df

  def bf_tabular(self,):
    '''load selected vectors, selected times, single output variable
       into a pandas DataFrame, where index (rows) is run/vector, and
       columns are times (analogous to Dakota response_fns).
       Source is an hdf5 file containing outputs for all simulations
    '''
    grp_scenario = self.bf_h5['/Scenario {:d}'.format(self.s)]
    bf_time_yr = grp_scenario['TIMES_YEARS'][...]
    idx_bf_times = []
    for i in xrange(len(bf_time_yr)):
      if any(round(t) == round(bf_time_yr[i]) for t in self.times):
        idx_bf_times.append(i)
    if len(idx_bf_times) != self.ntimes:
      print('ERROR: len(idx_bf_times) {} != ntimes {}. Exiting.'.format(len(self.idx_bf_times),self.ntimes))
      sys.exit()
    bf_output = grp_scenario[self.reg][self.var][...]
    data = bf_output[[x for x in self.run_idx]][:,[i for i in idx_bf_times]]
    self.bf_df = pd.DataFrame(data=data,
                      index=self.vectors, #list of vectors in play
                      columns=self.times,)
    inst_flux_found = self.reg.lower().find('instant') > -1
    if inst_flux_found: #convert kg/s to kg/yr for bragflo
      self.bf_df = self.bf_df * 3.153600E+07 #s/yr
    #print(df)
    #return df

  def pf_load_var(self,v):
    '''load entire time series of multiple pflotran output variables
       into a pandas DataFrame
       Source is a text file that looks like pflotran obs output
    '''
    filename = os.path.join(self.pf_dir,'pf_PFD_r{0}_s{1}_v{2:0>3}-obs-0.ave'.format(
                 self.r,self.s,v))
    with open (filename,'r') as f:
      header = f.readline().split(',')
      for i in xrange(len(header)):
        header[i] = header[i].strip('"\n')
    df = pd.read_table(filename,delim_whitespace=True,
              index_col=0,skiprows=1,names=header[1:])
    #print('pf v{} max:{:e}'.format(v,df[self.string].max()))
    #print('pf v{} min:{:e}'.format(v,df[self.string].min()))
    return df

  def pf_tabular(self,):
    '''load selected vectors, selected times, single output variable
       into a pandas DataFrame, where index (rows) is vector, and
       columns are times (analogous to Dakota response_fns).
       Sources are text files each containing outputs of single vector
    '''
    self.pf_df = pd.DataFrame(data=[],
                      index=self.vectors,
                      columns=self.times,
                      dtype=np.float)
    for v in self.vectors:
      filename = os.path.join(self.pf_dir,'pf_PFD_r{0}_s{1}_v{2:0>3}-obs-0.ave'.format(
                 self.r,self.s,v))
      with open (filename,'r') as f:
        header = f.readline().split(',')
        for i in xrange(len(header)):
          header[i] = header[i].strip('"\n')
      temp_df = pd.read_table(filename,delim_whitespace=True,
              index_col=0,skiprows=1,names=header[1:]).T #transpose
      try:
        self.pf_df.loc[v,:] = temp_df.loc[self.string,self.times]
      except ValueError:
        columns = []
        for t in self.times:
          clocation = temp_df.columns.get_loc(t)
          try:
            columns.append(int(clocation))
          except TypeError:
            columns.append(clocation.start)
        ilocation = temp_df.index.get_loc(self.string)
        self.pf_df.loc[v,:] = temp_df.iloc[ilocation,columns]
        #print('ValueError in r{}_s{}_v{:0>3}'.format(self.r,self.s,v))
#    print(df)
#    return df

  def bf_plot_horsetail(self,axes):
    '''plot bragflo horsetail 
       calculate y axis limits for anything other than porosity or saturation
       DEPRECATED - about to be
       use of color not updated
    '''
    max_list = []
    min_list = []
    for i in xrange(len(self.vectors)):
      run_idx = (self.vectors[i]-1) + 100*(self.r-1)
      df = self.bf_load_var(run_idx)  
      max_list.append(df.max()[self.string])
      min_list.append(df.min()[self.string])
      df.plot(ax=axes,color=self.bf_color[i],marker=self.bf_marker,legend=False,)
    if (self.var == 'POROS' or self.var == 'SATBRINE' or self.var == 'SATGAS'):
      axes.set_ylim(self.ylim_dict[self.var])
    else:
      axes.set_ylim(self.find_ylim(min_list,max_list))
    axes.set_xlabel('Time [y]')
    axes.set_ylabel(self.var_dict[self.var])
    axes.set_title('bf_PFD_r{}_s{} {} {}'.format(self.r,
                      self.s,self.reg,self.var))

  def pf_plot_horsetail(self,axes):
    '''plot pflotran horsetail
       calculate y axis limits for anything other than porosity or saturation
       USE WITH CAUTION NOT COMPATIBLE WITH pf_tabular()
    '''
    max_list = []
    min_list = []
    for v in self.vectors:
      df = self.pf_load_var(v)
      max_list.append(df.max()[self.string])
      min_list.append(df.min()[self.string])
      df[self.string].plot(ax=axes,color=self.pf_color[v],legend=False,)
    if (self.var == 'POROS' or self.var == 'SATBRINE' or self.var == 'SATGAS'):
      axes.set_ylim(self.ylim_dict[self.var])
    else:
      axes.set_ylim(self.find_ylim(min_list,max_list))
    axes.set_xlabel('Time [y]')
    axes.set_ylabel(self.string)
    axes.set_title('pf_PFD_r{}_s{} {}'.format(self.r,
                       self.s,self.string))

  def bf_coincident_times(self,axes):
    '''plot bragflo horsetail from df containing all requested vectors
       CALL bf_tabular FIRST
    '''
    #bf_df = self.bf_tabular()
    min_list = self.bf_df.T.min()
    max_list = self.bf_df.T.max()
    if len(self.vectors) == 1:
      color = self.bf_color[self.vectors[0]-1]
      self.bf_df.T.plot(ax=axes,color=color,marker=self.bf_marker,
                   linestyle=self.bf_linestyle,legend=False,)
    else:
      colors = [self.bf_color[v-1] for v in self.vectors]
      self.bf_df.T.plot(ax=axes,color=colors,marker=self.bf_marker,
                   linestyle=self.bf_linestyle,legend=False,)
    if (self.var == 'POROS' or self.var == 'SATBRINE' or self.var == 'SATGAS'):
      axes.set_ylim(self.ylim_dict[self.var])
    else:
      axes.set_ylim(self.find_ylim(min_list,max_list))
    axes.set_xlabel('Time [y]')
    axes.set_ylabel(var_dict[self.var])
    axes.set_title('bf_PFD_r{}_s{} {} {}'.format(self.r,
                       self.s,self.reg,self.var))
    if self.plot_string:
      axes.text(0.02,0.96,self.plot_string,transform=axes.transAxes)

  def pf_coincident_times(self,axes):
    '''plot pflotran horsetail using coincident output times only
       CALL pf_tabular() FIRST
    '''
    #pf_df = self.pf_tabular()
    min_list = self.pf_df.T.min()
    max_list = self.pf_df.T.max()
    if len(self.vectors) == 1:
      color = self.pf_color[self.vectors[0]-1]
      self.pf_df.T.plot(ax=axes,color=color,marker=self.pf_marker,
                   linestyle=self.pf_linestyle,legend=False,)
    else:
      colors = [self.pf_color[v-1] for v in self.vectors]
      self.pf_df.T.plot(ax=axes,color=colors,marker=self.pf_marker,
                   linestyle=self.pf_linestyle,legend=False,)
    if (self.var == 'POROS' or self.var == 'SATBRINE' or self.var == 'SATGAS'):
      axes.set_ylim(self.ylim_dict[self.var])
    else:
      axes.set_ylim(self.find_ylim(min_list,max_list))
    axes.set_xlabel('Time [y]')
    axes.set_ylabel(var_dict[self.var])
    axes.set_title('pf_PFD_r{}_s{} {}'.format(self.r,
                       self.s,self.string))
    if self.plot_string:
      axes.text(0.02,0.96,self.plot_string,transform=axes.transAxes)

  def find_ylim(self,min_list,max_list):
    '''return y limits as function of min and max variable value
    '''
    min_value = min(min_list)
    max_value = max(max_list)
    if max_value > 0.:
      max_value *= 1.1
    elif max_value < 0.:
      max_value *= 0.9
    else: #=0
      max_value = -0.1*min_value
    if min_value < 0.:
      min_value *= 1.1
    elif min_value > 0.:
      min_value *= 0.9
    else: #=0
      min_value = -0.1*max_value
    return(min_value,max_value)

  def bf_boxplots(self,axes):
    '''plot bragflo box plots
       CALL bf_tabular() FIRST
    '''
    #df = self.bf_tabular()
    boxlines = self.bf_df.boxplot(ax=axes,return_type='dict',widths=0.7)#positions=self.times)
    plt.setp(boxlines['boxes'],color='royalblue')
    plt.setp(boxlines['medians'],color='royalblue')
    plt.setp(boxlines['whiskers'],linestyle='-',color='royalblue')
    plt.setp(boxlines['caps'],color='royalblue')
    plt.setp(boxlines['fliers'],color='royalblue',marker='x',markersize=6)
    boxlines['whiskers'] = [5,95]
    min_list = self.bf_df.min()
    max_list = self.bf_df.max()
    if (self.var == 'POROS' or self.var == 'SATBRINE' or self.var == 'SATGAS'):
      axes.set_ylim(self.ylim_dict[self.var])
    else:
      axes.set_ylim(self.find_ylim(min_list,max_list))
    axes.set_xlabel('Time [y]')
    axes.set_ylabel(var_dict[self.var])
    axes.set_title('bf_PFD_r{}_s{} {} {}'.format(self.r,
                      self.s,self.reg,self.var))
    xticks = axes.get_xticklabels()
    plt.setp(xticks,rotation=45,fontsize=12)


  def pf_boxplots(self,axes):
    '''plot pflotran box plots
       CALL pf_tabular() FIRST
    '''
    #df = self.pf_tabular()
    boxlines = self.pf_df.boxplot(ax=axes,return_type='dict',widths=0.3)#positions=self.times)
    plt.setp(boxlines['boxes'],color='red')
    plt.setp(boxlines['medians'],color='red')
    plt.setp(boxlines['whiskers'],linestyle='-',color='red')
    plt.setp(boxlines['caps'],color='red')
    plt.setp(boxlines['fliers'],color='red',marker='+',markersize=6)
    boxlines['whiskers'] = [5,95]
    min_list = self.pf_df.min()
    max_list = self.pf_df.max()
    if (self.var == 'POROS' or self.var == 'SATBRINE' or self.var == 'SATGAS'):
      axes.set_ylim(self.ylim_dict[self.var])
    else:
      axes.set_ylim(self.find_ylim(min_list,max_list))
    axes.set_xlabel('Time [y]')
    axes.set_ylabel(var_dict[self.var])
    axes.set_title('pf_PFD_r{}_s{} {}'.format(self.r,
                      self.s,self.string))
    xticks = axes.get_xticklabels()
    plt.setp(xticks,rotation=45,fontsize=12)

  def overlay_horsetail(self,axes):
    '''overlay horsetail plots, using coincident output times only
       UNNECESSARY METHOD - SHOULD BE DEPRECATED
       CALL bf_tabular() and pf_tabular() first if you use it.
    '''
    #bf_df = self.bf_tabular()
    #pf_df = self.pf_tabular()
    min_list = self.pf_df.T.min()
    max_list = self.pf_df.T.max()
    if len(self.vectors) == 1:
      self.bf_df.T.plot(ax=axes,color=self.bf_color[0],marker=self.bf_marker,
                        linestyle=self.bf_linestyle,legend=False,)
    else:
      self.bf_df.T.plot(ax=axes,color=self.bf_color,marker=self.bf_marker,
                        linestyle=self.bf_linestyle,legend=False,)
    if len(self.vectors) == 1:
      self.pf_df.T.plot(ax=axes,color=self.pf_color[0],marker=self.pf_marker,
                   linestyle=self.pf_linestyle,legend=False,)
    else:
      self.pf_df.T.plot(ax=axes,color=self.pf_color,marker=self.pf_marker,
                   linestyle=self.pf_linestyle,legend=False,)
    if (self.var == 'POROS' or self.var == 'SATBRINE' or self.var == 'SATGAS'):
      axes.set_ylim(self.ylim_dict[self.var])
    else:
      axes.set_ylim(self.find_ylim(min_list,max_list))
    axes.set_xlabel('Time [y]')
    axes.set_ylabel(var_dict[self.var])
    axes.set_title('overlay PFD_r{}_s{} {}'.format(self.r,
                       self.s,self.string))

  def diff_horsetail_cum_flux(self):
    '''calculates the absolute value of the difference between the BRAGFLO and
       PFLOTRAN cumulative fluxes at the end of a simulation for each vector.
       CALL bf_tabular() and pf_tabular() FIRST
       DEPRECATED - use methods in horsetail_fluxes.py instead
    '''
    #bf_df = self.bf_tabular()
    #pf_df = self.pf_tabular()
    max_time = int(self.bf_df.columns.max())
    #max_time_index = self.bf_df.columns.values.size
    num_vectors = self.bf_df.index.size
    string = 'r{}_s{}_{}_{}'.format( \
                  args.replicate,args.scenario,args.region,args.variable)
    fout = open(os.path.join(output_dir,string+'_diff_cum_flux.txt'),'w')
    fout.write(string+'\n')
    fout.write('Number of Vectors: {}\n'.format(num_vectors))
    fout.write('Vector   Difference in Cumulative Flux   ' + \
               'Maximum Cumulative Flux\n\n')
    differences = np.zeros(num_vectors,'=f8')
    ivec = 0
    for v in self.bf_df.index:
      differences[ivec] = abs(self.bf_df.loc[v,max_time] - \
                              self.pf_df.loc[v,max_time])
      fout.write('{} {} {}\n'.format(v,differences[ivec], \
                             max(abs(self.bf_df.loc[v,max_time]), \
                                 abs(self.pf_df.loc[v,max_time]))))
      ivec += 1
    fout.write('\nMean: {}\n'.format(np.mean(differences)))
    fout.write('Median: {}\n'.format(np.median(differences)))
    fout.write('Standard Deviation: {}\n'.format(np.std(differences)))
    fout.close()
  
  def diff_horsetail_area(self):
    '''calculates the absolute value of area between BRAGFLO and PFLOTRAN 
       variables for each vector.
       calculate the area under the BRAGFLO curve to use for scaling (rel_scale)
       (scaling appropriate when all values > 0. not sure we want to use it for
       saturation.)
       CALL bf_tabular() and pf_tabular() FIRST
    '''
    #bf_df = self.bf_tabular()
    #pf_df = self.pf_tabular()
    max_time = self.bf_df.columns.values.max()
    num_vectors = self.bf_df.index.size
    string = 'r{}_s{}_{}_{}'.format( \
                  args.replicate,args.scenario,args.region,args.variable)
    fout = open(os.path.join(output_dir,string+'_diff_area.txt'),'w')
    fout.write(string+'\n')
    fout.write('Number of Vectors: {}\n'.format(num_vectors))
    fout.write('Maximum Time: {}\n'.format(max_time))
    #fout.write('Vector   Time Normalized Area (Area/Maximum Time)\n\n')
    fout.write('Vector DiffArea_per_time BragfloArea_per_time relative_difference\n\n')
    areas = np.zeros(num_vectors,'=f8')
    # if a pressure, flux, or brine volume, scale by max abs press in reg during run
    pressure_found = args.variable.lower().find('pres') > -1
    cum_flux_found = args.region.lower().find('cumulative') > -1
    inst_flux_found = args.region.lower().find('instant') > -1
    vol_found = args.variable.lower().find('vol') > -1
    rel_scale = 0.
    ivec = 0
    for v in self.bf_df.index:
      [area,times,values] = \
        aia.IntegralAreaBetweenCurves(self.bf_df.columns.values,
                                      self.bf_df.loc[v].values,
                                      self.pf_df.columns.values,
                                      self.pf_df.loc[v].values, \
                                      'first')
      rel_scale = np.trapz(y=self.bf_df.loc[v],x=self.bf_df.columns)
#returns exactly the same number as trapz()
#      [rel_scale_aia,times,values] = \
#        aia.IntegralAreaBetweenCurves(self.bf_df.columns.values,
#                                      self.bf_df.loc[v].values,
#                                      self.bf_df.columns.values,
#                                      np.zeros_like(self.bf_df.loc[v].values), \
#                                      'first')
#      print(rel_scale)
#      print(rel_scale_aia)
#      if (pressure_found or cum_flux_found or inst_flux_found or vol_found):
#        rel_scale = max(self.bf_df.loc[v].abs().max(), \
#                        self.pf_df.loc[v].abs().max(),1.)
#      fout.write('{} {} {}\n'.format(v,area/max_time,rel_scale))
      areas[ivec] = area/max_time
      fout.write('{} {} {} {}\n'.format(v,area/max_time,rel_scale/max_time,
                                        area/rel_scale))
      ivec += 1
    fout.write('\nMean: {}\n'.format(np.mean(areas)))
    fout.write('Median: {}\n'.format(np.median(areas)))
    fout.write('Standard Deviation: {}\n'.format(np.std(areas)))
    fout.close()
  
  def rel_diff_horsetail_area(self):
    '''calculates the absolute value of area between BRAGFLO and PFLOTRAN 
       in relative difference space - ie bragflo value = 1 and pflotran
       value is fraction of bragflo value
       CALL bf_tabular() and pf_tabular() FIRST
    '''
    #bf_df = self.bf_tabular()
    #pf_df = self.pf_tabular()
    denom = self.bf_df.where(self.bf_df != 0.,other=1.e-2)
    pf_df = self.pf_df/denom #self.bf_df #modify df locally only
    bf_df = self.bf_df/denom #self.bf_df
    max_time = bf_df.columns.values.max()
    num_vectors = bf_df.index.size
    string = 'r{}_s{}_{}_{}'.format( \
                  args.replicate,args.scenario,args.region,args.variable)
    fout = open(os.path.join(output_dir,string+'_rel_diff_area.txt'),'w')
    fout.write(string+'\n')
    fout.write('Number of Vectors: {}\n'.format(num_vectors))
    fout.write('Maximum Time: {}\n'.format(max_time))
    fout.write('Vector   Time Normalized Area (Area/Maximum Time)\n\n')
    areas = np.zeros(num_vectors,'=f8')
    rel_scale = 0. #will always be zero, keep for compatibility w/ diff_horsetail_area()
    ivec = 0
    for v in bf_df.index:
      [area,times,values] = \
        aia.IntegralAreaBetweenCurves(bf_df.columns.values,
                                      bf_df.loc[v].values,
                                      pf_df.columns.values,
                                      pf_df.loc[v].values, \
                                      'first')
      areas[ivec] = area/max_time
      fout.write('{} {} {}\n'.format(v,area/max_time,rel_scale))
      ivec += 1
    fout.write('\nMean: {}\n'.format(np.mean(areas)))
    fout.write('Median: {}\n'.format(np.median(areas)))
    fout.write('Standard Deviation: {}\n'.format(np.std(areas)))
    fout.close()
  
  def diff_horsetail(self,axes):
    '''horsetail plot of differences
       CALL bf_tabular() and pf_tabular() FIRST
    '''
    #bf_df = self.bf_tabular()
    #pf_df = self.pf_tabular()
    df = self.bf_df - self.pf_df
    min_list = df.T.min()
    max_list = df.T.max()
    if len(self.vectors) == 1:
      df.T.plot(ax=axes,color=self.bf_color[0],marker='*',
                legend=False,)
    else:
      df.T.plot(ax=axes,color=self.bf_color,marker='*',
                legend=False,)
    #choose ylim based on threshold
    threshold = threshold_dict[self.var]
    if (max_list.max() < 2.*threshold and 
        min_list.min() > -2.*threshold):
      axes.set_ylim(-2.*threshold,2.*threshold)
    else:
      axes.set_ylim(self.find_ylim(min_list,max_list))
    axes.set_xlabel('Time [y]')
    axes.set_ylabel('BF - PF difference {}'.format(self.var))
    axes.set_title('bf-pf_PFD_r{}_s{} {}'.format(self.r,
                       self.s,self.string))
    return(max_list.abs().where(max_list.abs()>min_list.abs(),other=min_list.abs()))
  
  def rel_diff_horsetail(self,axes):
    '''horsetail plot of relative differences expressed as percent
       CALL bf_tabular() and pf_tabular() FIRST
    '''
    #bf_df = self.bf_tabular()
    #pf_df = self.pf_tabular()
    df = (self.bf_df - self.pf_df)/self.bf_df * 100.
    min_list = df.T.min()
    max_list = df.T.max()
    if len(self.vectors) == 1:
      df.T.plot(ax=axes,color=self.pf_color[0],marker='*',
                legend=False,)
    else:
      df.T.plot(ax=axes,color=self.pf_color,marker='*',
                legend=False,)
    #choose ylim based on threshold
    threshold = threshold_dict['percent']
    if (max_list.max() < 2.*threshold and 
        min_list.min() > -2.*threshold):
      axes.set_ylim(-2.*threshold,2.*threshold)
    else:
      axes.set_ylim(self.find_ylim(min_list,max_list))
    axes.set_xlabel('Time [y]')
    axes.set_ylabel('(BF - PF)/BF*100 % difference {}'.format(self.var))
    axes.set_title('%_diff_PFD_r{}_s{} {}'.format(self.r,
                       self.s,self.string))
    return(max_list.abs().where(max_list.abs()>min_list.abs(),other=min_list.abs()))

  def scatter_plot(self,axes,series,threshold):
    '''scatter plot of values in series, a pandas series
       use index labels for x axis
       annotate values that exceed threshold
       best suited for all values > 0
       WHAT WAS I USING THIS FOR? use methods in plot_horsetail_diff_area.py and
       plot_horsetail_diff_cum_flux.py instead
    '''
    edgecolors = ['black' for x in xrange(len(series))]
    labels = []
    violators = []
    for i in xrange(len(series)):
      if series.iloc[i] >= threshold:
        edgecolors[i] = 'red'
        violators.append(series.index[i])
      labels.append('{}'.format(series.index[i]))
    axes.scatter(series.index,series,s=100,c=self.pf_color,edgecolors=edgecolors)
    for label, x, y in zip(labels,series.index,series):
      if abs(y) >= threshold:
        axes.annotate(
          label,
          xy = (x,y), xytext=(-10,-10),
          textcoords='offset points', ha='right', va='bottom')
    if series.max() < 2.*threshold:
      axes.set_ylim(-0.1*threshold,2.*threshold)
    axes.set_xlim(min(series.index)-1,max(series.index)+1)
    axes.set_title('bf-pf_PFD_r{}_s{} {}'.format(self.r,
                       self.s,self.string))
    return(violators)
  
 ##############################################################################
if __name__ == '__main__':
  '''get command line arguments and run stuff
  '''
  string1 = ','.join([k for k in reg_dict.keys()])
  string2 = ','.join([k for k in var_dict.keys()])

  parser = argparse.ArgumentParser(
    description='Make horsetail plots of bragflo and pflotran results.')
  parser.add_argument('-f','--bragflo_h5_file',
                      help='hdf5 file containing summary of bragflo output' +
                           ' [default = ./pfd_analysis_bragflo_summary.h5]',
                      default='pfd_analysis_bragflo_summary.h5',
                      required=False,)
  parser.add_argument('-p','--pflotran_dir',
                      help='directory containing pflotran *obs*.ave files' +
                           ' [default = ./s1]',
                      default = 's1',
                      required=False,)
  parser.add_argument('-s','--scenario',
                      help='int (1 thru 6) specifying scenario' +
                           ' [default = 1]',
                      default = 1,
                      required = False,) 
  parser.add_argument('-r','--replicate',
                      help='int (1 thru 3) specifying replicate' +
                           ' [default = 1]',
                      default = 1,
                      required = False,)
  parser.add_argument('-v','--vectors', nargs='+',
                      help='int, list of ints [1-100], or keyword "all"' +
                           ' specifying vectors' +
                           ' [default = "all"]',
                      default='all',
                      required=False,)
  parser.add_argument('-e','--region',
                      help='To plot vol-ave output, string must exactly equal one of: ' +
                            string1 +
#                           '\nTo plot flux, string must exactly equal one of: ' + string3 +
                           '\n[default = WAS_AREA]',
                      default='WAS_AREA',
                      required = False,)
  parser.add_argument('-a','--variable',
                      help='To plot vol-ave output, string must exactly equal one of: ' +
                            string2 +
#                           '\nTo plot flux, string must exactly equal one of: ' + string4 +
                           '\n[default = PRESBRIN]',
                      default='PRESBRIN',
                      required = False,)
  parser.add_argument('-d','--output_dir',
                      help='relative path to directory for output' +
                           ' [default = .]',
                      default='.',
                      required = False,)
  parser.add_argument('-l','--layout', nargs='+',
                      help='list diff_area,horsetails,violators,nonviolators,differences,and/or boxplots' +
                           ' [default = diff_area]',
                      default='diff_area',
                      required = False,)
  parser.add_argument('-c','--colors',
                      help='choose "rainbow" or "blue_and_red"' +
                           ' [default = blue_and_red]',
                      default='blue_and_red',
                      required = False,)
  parser.add_argument('-x','--text_for_plot',
                      help='optional string to add to plots' +
                           ' [default = None]',
                      default = None,
                      required = False,)

  args = parser.parse_args()

  #check that output dir exists. will overwrite contents with abandon
  #corresponding shell script asks for permission to overwrite and 
  #creates dir if it doesn't exist.
  output_dir = os.path.join(os.getcwd(),args.output_dir)
  if not os.path.isdir(output_dir):
    print('ERROR: {} does not exist. Create it and try again.'.format(output_dir))
    print('Exiting.')
    sys.exit()

#a few times for box plots
  few_times = [0.,
           101.,
           351.,
           551.,750.,
           1001.,1201.,
           1400.,
           2001.,3000.,
           4000.,5000.,6000.,7000.,8000.,9000.,10000.,]
#all times for differences
  times = [0.,1.,2.,3.,4.,5.,6.,7.,8.,9., #10
           10.,20.,30.,40.,50.,60.,70.,80.,90., #9
           99.,101.,150.,199.,201.,250., #7 -1=6
           300.,349.,351.,400.,450.,500., #7 -1=6
           549.,551.,600.,650.,700.,750.,800., #7
           850.,900.,950.,999.,1001.,1100., #7 -1=6
           1199.,1201.,1300.,1400.,1500.,1549.,1551., #7
           1600.,1700.,1800.,1900.,1999.,2001.,3000., #7
           4000.,5000.,6000.,7000.,8000.,9000.,10000.,] #7 for total of 68 times

  print('looking for {}'.format(args.variable))
  if args.variable in var_dict.keys():
    horsetail = HorsetailPlot(
                              bf_file=args.bragflo_h5_file,
                              pf_dir=args.pflotran_dir,
                              scenario=int(args.scenario),
                              replicate=int(args.replicate),
                              vectors=args.vectors,
                              region=args.region,
                              variable=args.variable,
                              times=times,
                              colors=args.colors,
                              plot_string=args.text_for_plot)
    horsetail.bf_tabular()
    horsetail.pf_tabular()
    #horsetail.diff_horsetail_area()
    #horsetail.rel_diff_horsetail_area()

  print(args.layout)
  if ('diff_area' in args.layout or 'diff_area_only' in args.layout):
    horsetail.diff_horsetail_area()
  if ('horsetails' in args.layout):
    fig, axes = plt.subplots(nrows=1,ncols=3,figsize=(24,8)) #width,height
#    horsetail.bf_plot_horsetail(axes[0])
    horsetail.bf_coincident_times(axes[0])
    horsetail.pf_coincident_times(axes[1])
    horsetail.bf_coincident_times(axes[2])
    horsetail.pf_coincident_times(axes[2])
    axes[2].set_title('PFD_r{}_s{} {} {}'.format(args.replicate,args.scenario,
                       args.region,args.variable))
    plt.savefig(os.path.join(output_dir,'r{}_s{}_{}_{}_horsetail.png'.format(
              args.replicate,args.scenario,args.region,args.variable)),
                bbox_inches='tight')
  if ('violators' in args.layout):
    fig, axes = plt.subplots(nrows=1,ncols=1,figsize=(8,8)) #width,height
    horsetail.bf_coincident_times(axes)
    horsetail.pf_coincident_times(axes)
    axes.set_title('PFD_r{}_s{} {} {} violators'.format(args.replicate,args.scenario,
                       args.region,args.variable))
#    print (horsetail.bf_df.loc[int(args.vectors[0])])
    for v in args.vectors:
      label = 'v{:0>3}'.format(v)
      x = horsetail.bf_df.loc[int(v)].abs().idxmax(axis=0)
      y = horsetail.bf_df.loc[int(v),x]
      axes.annotate(
        label,
        xy=(x,y),
        xycoords='data',
        xytext=(-10,10),
        textcoords='offset points',ha='right',va='bottom',
        color=horsetail.bf_color[int(v)-1],
        fontsize=15)
    plt.savefig(os.path.join(output_dir,'r{}_s{}_{}_{}_vhorsetail.png'.format(
              args.replicate,args.scenario,args.region,args.variable)),
                bbox_inches='tight')
  if ('nonviolators' in args.layout):
    fig, axes = plt.subplots(nrows=1,ncols=1,figsize=(8,8)) #width,height
    horsetail.bf_coincident_times(axes)
    horsetail.pf_coincident_times(axes)
    axes.set_title('PFD_r{}_s{} {} {} nonviolators'.format(args.replicate,args.scenario,
                       args.region,args.variable))
    plt.savefig(os.path.join(output_dir,'r{}_s{}_{}_{}_nvhorsetail.png'.format(
              args.replicate,args.scenario,args.region,args.variable)),
              bbox_inches='tight')
  if ('differences' in args.layout):
    fig, axes = plt.subplots(nrows=1,ncols=2,figsize=(16,8)) #width,height
    max_abs_list = horsetail.diff_horsetail(axes[0])
    max_abs_list.to_csv(os.path.join(output_dir,'r{}_s{}_{}_{}_absdiff.txt'.format(
              args.replicate,args.scenario,args.region,args.variable)),sep=' ',)

    max_per_list = horsetail.rel_diff_horsetail(axes[1])
    max_per_list.to_csv(os.path.join(output_dir,'r{}_s{}_{}_{}_perdiff.txt'.format(
              args.replicate,args.scenario,args.region,args.variable)),sep=' ',)

    plt.savefig(os.path.join(output_dir,'r{}_s{}_{}_{}_diff.png'.format(
                args.replicate,args.scenario,args.region,args.variable)),
              bbox_inches='tight')

    fig, axes = plt.subplots(nrows=1,ncols=2,figsize=(16,8))
    horsetail.scatter_plot(axes[0],max_abs_list,threshold_dict[args.variable])
    axes[0].set_xlabel('Vector #')
    axes[0].set_ylabel('{} max |difference|'.format(args.variable))
    exceeds = horsetail.scatter_plot(axes[1],max_per_list,threshold_dict['percent'])
    axes[1].set_xlabel('Vector #')
    axes[1].set_ylabel('{} max |% difference|'.format(args.variable))
    plt.savefig(os.path.join(output_dir,'r{}_s{}_{}_{}_scatterdiff.png'.format(
                args.replicate,args.scenario,args.region,args.variable)),
              bbox_inches='tight')
    with open(os.path.join(output_dir,'r{}_s{}_{}_{}_exceeds_perdiff.txt'.format(
              args.replicate,args.scenario,args.region,args.variable)),'w') as f:
      for v in exceeds:
        f.write('{}\n'.format(v))
  if ('boxplots' in args.layout):
    if args.variable in var_dict.keys():
      #create horsetail object with default short list of times for boxplots
      horsetail_few = HorsetailPlot(
                              bf_file=args.bragflo_h5_file,
                              pf_dir=args.pflotran_dir,
                              scenario=int(args.scenario),
                              replicate=int(args.replicate),
                              vectors=args.vectors,
                              region=args.region,
                              variable=args.variable,
                              times=few_times,
                              colors=args.colors,
                              plot_string=args.text_for_plot)
      horsetail_few.bf_tabular()
      horsetail_few.pf_tabular()
#    elif args.variable in (loc_dict.keys()):
#      #create flux horsetail object with default short list of times for boxplots
#      horsetail_few = FluxHorsetailPlot(
#                              bf_file=args.bragflo_h5_file,
#                              pf_dir=args.pflotran_dir,
#                              scenario=int(args.scenario),
#                              replicate=int(args.replicate),
#                              vectors=args.vectors,
#                              region=args.region,
#                              variable=args.variable,
#                              colors=args.colors)
#      horsetail_few.bf_tabular()
#      horsetail_Few.pf_tabular()
    #arrange plots in rows and columns
    fig, axes = plt.subplots(nrows=1,ncols=1,figsize=(12,8)) #width,height
    horsetail_few.bf_boxplots(axes)
    horsetail_few.pf_boxplots(axes)
    plt.savefig(os.path.join(output_dir,'r{}_s{}_{}_{}_boxplots.png'.format(
                args.replicate,args.scenario,args.region,args.variable)),
                bbox_inches='tight')
    #plt.show()
  if ('horsetails' not in args.layout and
      'violators' not in args.layout and
      'nonviolators' not in args.layout and
      'differences' not in args.layout and
      'diff_area' not in args.layout and
      'diff_area_only' not in args.layout and
      'boxplots' not in args.layout):
    print('Unrecognized --layout (-l) option.')
    print('Choose horsetails, violators, nonviolators, differences, diff_area_only, and/or boxplots')
    print('Exiting')
    sys.exit()

  #done
  print('r{}_s{}_{}_{} done'.format(args.replicate,args.scenario,args.region,args.variable))
