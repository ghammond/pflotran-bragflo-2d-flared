'''
   stats_cum_flux.py

   Read *diff_cum.txt files, 100 each replicate/scenario and
   calculate stats - mean, median, .05 and .95 quantile.
   Use pandas DataFrames so whole pile of stats could easily be
   added, if anyone wants more.

   Emily Stein <ergiamb@sandia.gov>
   07.20.18
'''

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import argparse
import os,sys
import horsetail_fluxes as hf

#this one doesn't get fancy input reads because it needs everything.
#so that I can tabulate all the numbers in one place.

rs_pairs = []
for s in range(6):
  for r in range(3):
    rs_pairs.append('r{}_s{}'.format(r+1,s+1))

locations = [
             'lwb_north_mb139',
             'lwb_north_anhydrite',
             'lwb_north_mb138',
             'lwb_south_mb139',
             'lwb_south_anhydrite',
             'lwb_south_mb138',
             'borehole_culebra',
             'shaft_culebra',
            ]

stats = ['mean','median',
         'quantile05','quantile95',
         'quantile25','quantile75',
        ]

headings = []
for code in ['bragflo','pflotran']:
  for stat in stats:
    headings.append('{}_{}'.format(code,stat))

def stats_cum_flux(input_dir,location='borehole_culebra'):
  '''
     Calculate and tabulate stats for each cum flux in each R/S pair.
     
  '''
  #create empty df that will hold all stats for single location
  df = pd.DataFrame(data=None,index=rs_pairs,columns=headings,dtype=float)
  #plot_df = pd.DataFrame(data=None,index=[x for x in range(1,101)],dtype=float)
  #create axes for plot
  fig,axes = plt.subplots(nrows=1,ncols=1,figsize=(12,8)) #width,height
  bf_data = []
  pf_data = []
  #loop through all scenario/replicate pairs to calc stats and fill df
  for s in range(1,7):
    for r in range(1,4):
      filename = os.path.join(input_dir,'r{}_s{}_{}_diff_cum.txt'.format(
                                         r,s,location))
      rs_df = pd.read_table(filename,sep=' ',engine='python',
              header=None,index_col=0,skiprows=5,skip_footer=4,
              names=['vector','bragflo','pflotran','difference','relative_difference'])
      #save to df
      df.loc['r{}_s{}'.format(r,s),'bragflo_mean'] = rs_df['bragflo'].mean()
      df.loc['r{}_s{}'.format(r,s),'bragflo_median'] = rs_df['bragflo'].median()
      df.loc['r{}_s{}'.format(r,s),'bragflo_quantile05'] = \
                                                       rs_df['bragflo'].quantile(0.05)
      df.loc['r{}_s{}'.format(r,s),'bragflo_quantile25'] = \
                                                       rs_df['bragflo'].quantile(0.25)
      df.loc['r{}_s{}'.format(r,s),'bragflo_quantile75'] = \
                                                       rs_df['bragflo'].quantile(0.75)
      df.loc['r{}_s{}'.format(r,s),'bragflo_quantile95'] = \
                                                       rs_df['bragflo'].quantile(0.95)

      df.loc['r{}_s{}'.format(r,s),'pflotran_mean'] = rs_df['pflotran'].mean()
      df.loc['r{}_s{}'.format(r,s),'pflotran_median'] = rs_df['pflotran'].median()
      df.loc['r{}_s{}'.format(r,s),'pflotran_quantile05'] = \
                                                       rs_df['pflotran'].quantile(0.05)
      df.loc['r{}_s{}'.format(r,s),'pflotran_quantile25'] = \
                                                       rs_df['pflotran'].quantile(0.25)
      df.loc['r{}_s{}'.format(r,s),'pflotran_quantile75'] = \
                                                       rs_df['pflotran'].quantile(0.75)
      df.loc['r{}_s{}'.format(r,s),'pflotran_quantile95'] = \
                                                       rs_df['pflotran'].quantile(0.95)
      #add to boxplot - how am I going to space these out
      #plot_df.join(rs_df['bragflo'],rsuffix='r{}_s{}'.format(location))
      bf_data.append(rs_df['bragflo'].values)
      pf_data.append(rs_df['pflotran'].values)

  #save output
  boxlines = axes.boxplot(bf_data,widths=0.7,)
  plt.setp(boxlines['boxes'],color='royalblue')
  plt.setp(boxlines['medians'],color='royalblue')
  plt.setp(boxlines['whiskers'],color='royalblue')
  plt.setp(boxlines['caps'],linestyle='-',color='royalblue')
  boxlines['whiskers'] = [5,95]
  plt.setp(boxlines['fliers'],color='royalblue',marker='x',markersize=6)
  boxlines = axes.boxplot(pf_data,widths=0.3,)
  plt.setp(boxlines['boxes'],color='red')
  plt.setp(boxlines['medians'],color='red')
  plt.setp(boxlines['whiskers'],linestyle='-',color='red')
  plt.setp(boxlines['caps'],color='red')
  boxlines['whiskers'] = [5,95]
  plt.setp(boxlines['fliers'],color='red',marker='+',markersize=6)
  plt.title('Cumulative Flux Stats for {}'.format(location))
  plt.ylabel('Cumulative Flux at 10000 y [kg]')
  plt.xlabel('Replicate/Scenario')
  axes.set_xticklabels([rs.upper() for rs in rs_pairs],rotation=45,fontsize=12)
  #plt.show()
  plt.savefig('{}_stats.png'.format(location),bbox='tight')




  df.to_csv(os.path.join(input_dir,'{}_stats.txt'.format(location)),sep=' ')

###############################################################################
if __name__ == '__main__':
  '''get command line arguments and run stuff
  '''
  string1 = ','.join([loc for loc in locations])
  parser = argparse.ArgumentParser(
    description='Calculate, write, and plot stats for cumulative fluxes.')
  parser.add_argument('-d','--input_dir',
                      help='directory containing *_diff_cum.txt files' +
                           ' [default = ./horsetail_flux_dir]',
                      default='horsetail_flux_dir',
                      required=False,)
  parser.add_argument('-a','--variable',
                      help='To plot vol-ave output, string must exactly equal one of: ' +
                            string1 +
                           '\n[default = borehole_culebra]',
                      default='borehole_culebra',
                      required = False,)
  args = parser.parse_args()
  stats_cum_flux(args.input_dir,args.variable)
  

