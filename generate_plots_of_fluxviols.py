'''generate_plots_of_fluxviols.py

   From output of plot_horsetail_diff_cum_flux.sh find violators and
   write shell script that calls horsetail_fluxes.py to replot horsetails
   of violators and nonviolators separately

   Glenn Hammond <gehammo@sandia.gov>
   Emily Stein <ergiamb@sandia.gov>
'''

import sys
import os
import shutil
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-b','--bf_path',
                    help='path to bragflo summary file - at this time not needed for fluxes' +
                    ' [default = ../bragflo_summary_orig_data/pfd_analysis_bragflo_summary_scenario{}.h5]',
                    default = '../bragflo_summary_orig_data/pfd_analysis_bragflo_summary_scenario{}.h5',
                    required=False,)
parser.add_argument('-d','--bf_dir',
                    help='path to directory containing bragflo flux txt files' +
                         ' [default = /home1/ergiamb/pfd_analysis/bragflo_output_fluxes]',
                    default = '/home1/ergiamb/pfd_analysis/bragflo_output_fluxes',
                    required=False,)
parser.add_argument('-f','--screen_file',
                    help='file of screen output from plot_horsetail_diff_cum_flux.sh' +
                         ' [default = ./plot_horsetail_diff_cum_flux.stdout]',
                    default = 'plot_horsetail_diff_cum_flux.stdout',
                    required=False,)
parser.add_argument('-v','--violators_sh',
                    help='name of shell script to replot violators' +
                         ' [default = ./replot_flux_violators.sh]',
                    default = 'replot_flux_violators.sh',
                    required=False,)
parser.add_argument('-n','--nonviolators_sh',
                    help='name of shell script to replot nonviolators' +
                         '[default = ./replot_flux_nonviolators.sh]',
                    default = 'replot_flux_nonviolators.sh',
                    required=False,)
parser.add_argument('-x','--text_for_plot',
                    help='optional string to put on plots' +
                         ' [default = None]',
                    default=None,
                    required=False,)
args = parser.parse_args()

command = 'python horsetail_fluxes.py '
bf_path = args.bf_path
bf_dir = args.bf_dir

filename = args.screen_file
outfilename = args.violators_sh
outfilename2 = args.nonviolators_sh
#not perfect but following will do:
output1 = 'horsetail_' + outfilename.split('/')[-1].split('.')[0] + '_dir'
output2 = 'horsetail_' + outfilename2.split('/')[-1].split('.')[0] + '_dir'

output_dir_block = '''output_dir={}
bf_dir={}
if test -d $output_dir; then
  read -p "$output_dir exists. Overwrite duplicate files? [yes/no]: " stdin
  case $stdin in
    no | n) echo ""
      echo "If you want to keep $output_dir, move it and rerun script."
      echo "Exiting." 
      echo ""
      exit 1
      ;;
    yes | y) echo ""
      echo "Will overwrite duplicate files. Starting..."
      echo ""
      ;;
    *) echo ""
      echo "Enter yes or no. Exiting."
      echo ""
      exit 1
      ;;
  esac
else
  mkdir $output_dir
fi\n
'''

f = open(filename,'r')
fout = open(outfilename,'w') #for violators
fout.write('#! /bin/bash\n\n')
fout.write(output_dir_block.format(output1,bf_dir))
fout2 = open(outfilename2,'w') #for nonviolators
fout2.write('#! /bin/bash\n\n')
fout2.write(output_dir_block.format(output2,bf_dir))
vectors = []
s = 0
r = 0
reg = 0
var = 0
for line in f:
  if line.startswith('r'):
    w = line.split('Threshold')
    if len(w) > 1:
      w = w[0].strip().split('_')
      s = int(w[1].split('s')[1])
      r = int(w[0].split('r')[1])
      #print('s {} r {}'.format(s,r))
      if 'OUT' in w: #if len(w) > 4:
        reg = "_".join(w[2:7])
        var = "_".join(w[7:])
      else:
        #print('not OUT')
        reg = "_".join(w[2:6])
        var = "_".join(w[6:])
        #print('reg {} var {}'.format(reg,var))
  elif line.split(':')[0].lstrip().rstrip().isdigit():
    w = line.split(':')
    vectors.append(int(w[0]))
    #print(vectors[-1])
  else:
    #add to shell scripts for violators
    #for purposes of replotting horsetails - if reg = INSTANT_BRINE_MASS_FLUX
    #change it to CUMULATIVE_BRINE_MASS_FLUX
    if reg == 'INSTANT_BRINE_MASS_FLUX':
      reg = 'CUMULATIVE_BRINE_MASS_FLUX'
    if len(vectors) > 0:
      fout.write('{} -f {} -b $bf_dir -p s{} '.format(command,
                                          bf_path.format(s),
                                          s))
      fout.write('-d $output_dir -l violators -c rainbow ')
      if args.text_for_plot:
        fout.write('-x "{}" '.format(args.text_for_plot))
      fout.write('-s {} -r {} -e {} -a {} -v '.format(s,r,reg,var))
      for v in vectors:
        fout.write('{} '.format(v))
      fout.write('\n')
    #add to shell scripts for nonviolators
      fout2.write('{} -f {} -b $bf_dir -p s{} '.format(command,
                                           bf_path.format(s),
                                           s))
      fout2.write('-d $output_dir -l nonviolators -c rainbow ')
      if args.text_for_plot:
        fout2.write('-x "{}" '.format(args.text_for_plot))
      fout2.write('-s {} -r {} -e {} -a {} -v '.format(s,r,reg,var))
      for v in range(1,101):
        if v not in vectors:
          fout2.write('{} '.format(v))
      fout2.write('\n')
    #reset vectors list for next reg/var
    vectors = []
    
f.close()    
fout.close()
fout2.close()
print('done')
