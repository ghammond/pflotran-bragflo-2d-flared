#! /bin/bash 
#an example of how you can use write_xmf.py if you want.
cd time-5y
for i in {001,002,003,004,005,006,007,008,009,010}; do
  python ../write_xmf.py pf_PFD_r1_time-5y_v$i ../grid/domain-elev-equal
done
cd ..
for s in {1,2,3,4,5,6}; do
  cd s$s
  for i in {001,002,003,004,005,006,007,008,009,010}; do
    python ../write_xmf.py pf_PFD_r1_s$s'_v'$i ../grid/domain-elev-equal
  done
  cd ..
done
