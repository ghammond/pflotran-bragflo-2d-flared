'''Uncertainty quantification of bragflo/pflotran outputs:
   horsetail plots of bragflo fluxes
   horsetail plots of pflotran fluxes

   Emily Stein (ergiamb@sandia.gov)
   Glenn Hammond (gehammo@sandia.gov)
'''

import sys
import os
import shutil
import argparse

import numpy as np
import h5py
import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import abs_integral_area as aia

import horsetail as ht

#print(pd.__version__)

#global variables

loc_dict = { #bf:pf
             'lwb_north_mb139':'mb139_nlwb',
             'lwb_north_anhydrite':'anh_nlwb',
             'lwb_north_mb138':'mb138_nlwb',
             'lwb_north_culebra':'cul_nlwb',
             'lwb_south_mb139':'mb139_slwb',
             'lwb_south_anhydrite':'anh_slwb',
             'lwb_south_mb138':'mb138_slwb',
             'lwb_south_culebra':'cul_slwb',
             'borehole_mb138':'bh_mb138',
             'borehole_salado':'bh_salado',
             'borehole_culebra':'bh_culebra',
             'shaft_mb138':'shaft_mb138',
             'shaft_salado':'shaft_salado',
             'shaft_culebra':'shaft_culebra',
            }
flux_dict = {
             'CUMULATIVE_BRINE_MASS_FLUX':'Water [kg]',
             'INSTANT_BRINE_MASS_FLUX':'Water [kg/y]',
             'CUMULATIVE_BRINE_MASS_FLUX_OUT':'Water Out [kg]',
            }
threshold_dict = { 
                  'percent':5.,
                  'lwb_north_mb139':1., #this tiny threshold means
                  'lwb_north_anhydrite':1., #autoscale instead
                  'lwb_north_mb138':1., #can choose threshold later
                  'lwb_north_culebra':1.,
                  'lwb_south_mb139':1.,
                  'lwb_south_anhydrite':1.,
                  'lwb_south_mb138':1.,
                  'lwb_south_culebra':1.,
                  'borehole_mb138':1.,
                  'borehole_salado':1.,
                  'borehole_culebra':1.,
                  'shaft_mb138':1.,
                  'shaft_salado':1.,
                  'shaft_culebra':1.,
                 }   

 ##############################################################################
class FluxHorsetailPlot(ht.HorsetailPlot):
  '''Uncertainty analysis of bragflo and pflotran flux outputs
     must choose one scenario, one replicate, one location,
     one kind of flux from flux_dict
     
     This is confusing, but if I use "region" for CUMULATIVE_BRINE_MASS_FLUX
     and "variable" for flux location, then bf methods from HorsetailPlot()
     work without modification. so that is what I am doing
 
     And with redefinition of self.string, all the pflotran plotting methods
     should work, too.
  '''
  def  __init__(self,bf_file='pfd_analysis_bragflo_summary.h5',
                pf_dir='s1',
                scenario=1,replicate=1,vectors='all',
                region='CUMULATIVE_BRINE_MASS_FLUX',variable='borehole_culebra',
                times=[0.,99.,349.,549.,750.,999.,1199.,1400.,1999.,3000.,10000.],
                colors='blue_and_red',
                plot_string=None,
                xlim=(None,None),
                ylim=(None,None)):
    super(FluxHorsetailPlot, self).__init__(bf_file,pf_dir,scenario,replicate,vectors,
          region,variable,times,colors,plot_string)
    self.loc_dict = loc_dict
    self.flux_dict = flux_dict
    self.string = '{} {}'.format(self.loc_dict[self.var],self.flux_dict[self.reg])
    self.bf_string = '{} {}'.format(self.var,self.flux_dict[self.reg])
    self.cum_string = '{} {}'.format(self.loc_dict[self.var],
                                     self.flux_dict['CUMULATIVE_BRINE_MASS_FLUX'])
    self.bf_cum_string = '{} {}'.format(self.var,
                                     self.flux_dict['CUMULATIVE_BRINE_MASS_FLUX'])
    self.xlim = xlim
    self.ylim = ylim
    
  def bf_load_var(self,v,bf_dir):
    '''load entire time series of multiple bragflo fluxes into a pandas df.
       source is a text file premade that looks like pflotran obs output
       cumulative fluxes are already in there

       nonzero flux rate at t=0 is artifact of -5 to 0 y simulation. 
       corresponding NaN at t=0 in cumulative flux.
       Replace all t=0 flux values with zero.

       return the df
    '''
    filename = os.path.join(bf_dir,'bf2_PFD_r{}_s{}_v{:0>3}_fluxes.txt'.format(
                 self.r,self.s,v))
    with open (filename,'r') as f:
      header = f.readline().split('" "')
      for i in xrange(len(header)):
        header[i] = header[i].strip('"\n')
    df = pd.read_table(filename,delim_whitespace=True,
              index_col=0,skiprows=1,names=header)
    df.iloc[0] = 0.
    return df
  
  def pf_load_var(self,v):
    '''load entire time series of multiple pflotran fluxes
       into a pandas DataFrame
       Source is a text file that looks like pflotran obs output
       Calculate cumulative outward fluxes and join to DataFrame
       return df
    '''
    filename = os.path.join(self.pf_dir,'pf_PFD_r{0}_s{1}_v{2:0>3}-int.dat'.format(
                 self.r,self.s,v))
    with open (filename,'r') as f:
      header = f.readline().split(',')
      for i in xrange(len(header)):
        header[i] = header[i].strip('"\n')
    df = pd.read_table(filename,delim_whitespace=True,
              index_col=0,skiprows=1,names=header)
    flux_list = []
    for col in df.columns:
      if 'Water [kg/y]' in col:
        flux_list.append(col)
    dt = df.index[1:] - df.index[:-1]
    outflux_df = df.iloc[1:][flux_list].copy()
    #outflux_df = pd.DataFrame(data=df.iloc[1:][flux_list])
    for col in outflux_df.columns:
      outflux_df[col] *= dt
      if 'slwb' in col:
        outflux_df[col] = outflux_df[col].where(outflux_df[col]<0.,other=0.) * -1.
      else:
        outflux_df[col] = outflux_df[col].where(outflux_df[col]>0.,other=0.)
    cumout_df = pd.DataFrame(data = outflux_df.cumsum(axis=0).values,
                index = outflux_df.index,
                columns = ['{} Water Out [kg]'.format(x.split()[0]) for x in outflux_df.columns])
    new_df = pd.DataFrame(data=df.iloc[1:].join(cumout_df))
    return new_df

  def plot_and_compare(self,bf_dir,plot_flag=True,compare_flag=True):
    '''call bf_load_var() and pf_load_var()
       make horsetail plots of both
       tabulate differences in cumulative fluxes
       this may not be the best way and it is not consistent with previous methods
       or as flexible.
       but whatever. need something to do the job.
       plot_flag == True then plot otherwise not
    '''
    print('plot_and_compare r{} s{} {} using {}'.format(self.r,self.s,self.var,
                                                 bf_dir))
    max_list = []
    min_list = []
    bf_list = []
    pf_list = []
    diff_list = []
    rel_diff_list = []
    rel_scale = []
    areas = []
    max_time = 1.e4 #inflexibility alert! will be wrong if simulation didn't finish
    fig, axes = plt.subplots(nrows=1,ncols=1,figsize=(8,8)) #width,height
    for v in self.vectors: #loop thru vectors
      bf_df = self.bf_load_var(v,bf_dir)
      pf_df = self.pf_load_var(v)
      max_list.append(bf_df.max()[self.bf_string])
      min_list.append(bf_df.min()[self.bf_string])
      if plot_flag:  # plot horsetails
        bf_df[self.bf_string].plot(ax=axes,color=self.bf_color[v-1],
                                 linestyle='--',legend=False,)
        pf_df[self.string].plot(ax=axes,color=self.pf_color[v-1],
                              linestyle='-',legend=False,)
      if compare_flag:
        # store cumulative flux at end of each simulation
        # if 1e-4 run didn't complete then final is not 10ky and may not match
        bf_final = bf_df.iloc[-1][self.bf_cum_string]
        pf_final = pf_df.iloc[-1][self.cum_string]
        diff = bf_final - pf_final
        bf_list.append(bf_final)
        pf_list.append(pf_final)
        diff_list.append(diff)
        rel_diff_list.append(diff/bf_final)
        # integrate area between the curves
        # you could use this on cumulative flux but it makes more sense on instant
        [area,times,values] = \
          aia.IntegralAreaBetweenCurves(bf_df.index.values,
                                      bf_df[self.bf_string].values,
                                      pf_df.index.values,
                                      pf_df[self.string].values, \
                                      'all')
        areas.append(area)
        [rel_scale_aia,times,values] = \
          aia.IntegralAreaBetweenCurves(bf_df.index.values,
                                      bf_df[self.bf_string].values,
                                      bf_df.index.values,
                                      np.zeros_like(bf_df[self.bf_string].values), \
                                      'first')
        rel_scale.append(rel_scale_aia)
    # end of loop thru vectors
    if compare_flag:
      # print integral diffs to file - same text layout as in horsetail.py
      filename = os.path.join(output_dir,'r{}_s{}_{}_{}_diff_area.txt'.format(
                self.r,self.s,self.reg,self.var))
      with open(filename,'w') as f:
        f.write('r{}_s{}_{}_{}\n'.format(self.r,self.s,self.reg,self.var))
        f.write('Number of Vectors: {}\n'.format(len(areas)))
        f.write('Maximum Time: {}\n'.format(max_time))
        f.write('vector DiffArea BragfloArea relative_difference\n\n')
        for i in range(len(areas)):
          f.write('{} {} {} {}\n'.format(self.vectors[i],
                                       areas[i],
                                       rel_scale[i],
                                       areas[i]/rel_scale[i]))
        f.write('\nMean: {}\n'.format(np.mean(np.array(areas))))
        f.write('Median: {}\n'.format(np.median(np.array(areas))))
        f.write('Standard Deviation: {}\n'.format(np.std(np.array(areas))))
      # save cumulative flux diffs to txt file
      filename = os.path.join(output_dir,'r{}_s{}_{}_diff_cum.txt'.format(
                self.r,self.s,self.var))
      with open(filename,'w') as f:
        f.write('r{}_s{}_{}_{}\n'.format(self.r,self.s,self.reg,self.var))
        f.write('Number of Vectors: {}\n'.format(len(areas)))
        f.write('Maximum Time: {}\n'.format(max_time))
        f.write('vector bragflo pflotran difference relative_difference\n\n')
        for i in range(len(diff_list)):
          f.write('{} {} {} {} {}\n'.format(self.vectors[i],
                                       bf_list[i],
                                       pf_list[i],
                                       diff_list[i],
                                       rel_diff_list[i]))
        f.write('\nMean Rel Diff: {}\n'.format(np.mean(np.array(rel_diff_list))))
        f.write('Median Rel Diff: {}\n'.format(np.median(np.array(rel_diff_list))))
        f.write('Standard Deviation: {}\n'.format(np.std(np.array(rel_diff_list))))
    if plot_flag:  # put finishing touches on the horsetail plot
      if self.plot_string:
        if abs(min(min_list)) < abs(max(max_list)):
          axes.text(0.02,0.96,self.plot_string,transform=axes.transAxes)
        else:
          axes.text(0.02,0.02,self.plot_string,transform=axes.transAxes)
      #axes.set_ylim(self.find_ylim(min_list,max_list))
      axes.set_xlabel('Time [y]')
      if self.reg == 'CUMULATIVE_BRINE_MASS_FLUX':
        axes.set_ylabel('Cumulative Liquid Flux [kg]')
      elif self.reg == 'CUMULATIVE_BRINE_MASS_FLUX_OUT':
        axes.set_ylabel('Cumulative Liquid Flux Out [kg]')
      elif self.reg == 'INSTANT_BRINE_MASS_FLUX':
        axes.set_ylabel('Liquid Flux [kg/y]')
      axes.set_title('PFD_r{}_s{} {}'.format(self.r,
                         self.s,self.var))
      plt.savefig(os.path.join(output_dir,'r{}_s{}_{}_{}_horsetail.png'.format(
                  self.r,self.s,self.reg,self.var)),
                  bbox_inches='tight')
      #plt.show()
      plt.close('all')
     
  def diff_flux_area(self):
    '''Use aia.IntegralAreaBetweenCurves() to calculate difference between solutions.
       Can be used on CUMULATIVE_LIQUID_MASS_FLUX, CUMULATIVE_LIQUID_MASS_FLUX_OUT,
       or INSTANT_LIQUID_MASS_FLUX; perhaps best suited for use with INSTANT...
       Differs from diff_horsetail_area because uses output for all time steps from
       both simulations instead of specific corresponding output times as in
       ht.diff_horsetail_area().
    '''
    pass #change of plans. to avoid reloading data add the integration to plot_and_compare()
    
  def replot_violators(self,bf_dir):
    '''pared down version of plot_and_compare() for replotting violators 
       doesn't include the compare part. uses different file name.
       labels vectors
    '''
    print('replot flux violators r{} s{} {} using {}'.format(self.r,self.s,self.var,
                                                             bf_dir))
    max_list = []
    min_list = []
    fig, axes = plt.subplots(nrows=1,ncols=1,figsize=(8,8)) #width,height
    for v in self.vectors:
      bf_df = self.bf_load_var(v,bf_dir)
      pf_df = self.pf_load_var(v)
      max_list.append(bf_df.max()[self.bf_string])
      min_list.append(bf_df.min()[self.bf_string])
      bf_df[self.bf_string].plot(ax=axes,color=self.bf_color[v-1],
                                 linestyle='--',legend=False,)
      pf_df[self.string].plot(ax=axes,color=self.pf_color[v-1],
                              linestyle='-',legend=False,)
      label = 'v{:0>3}'.format(v)
      x = bf_df[self.bf_string].abs().idxmax(axis=0)
      y = bf_df.loc[x,self.bf_string]
      plt.annotate(
        label,
        xy=(x,y),
        xycoords='data',
        xytext=(-10,10),
        textcoords='offset points',ha='right',va='bottom',
        color=self.bf_color[v-1],
        fontsize=15)
    if self.plot_string:
      if abs(min(min_list)) < abs(max(max_list)):
        axes.text(0.02,0.96,self.plot_string,transform=axes.transAxes)
      else:
        axes.text(0.02,0.02,self.plot_string,transform=axes.transAxes)
    axes.set_xlabel('Time [y]')
    if self.reg == 'CUMULATIVE_BRINE_MASS_FLUX':
      axes.set_ylabel('Cumulative Liquid Flux [kg]')
    elif self.reg == 'CUMULATIVE_BRINE_MASS_FLUX_OUT':
      axes.set_ylabel('Cumulative Liquid Flux Out [kg]')
    elif self.reg == 'INSTANT_BRINE_MASS_FLUX':
      axes.set_ylabel('Liquid Flux [kg/y]')
    #axes.set_yscale('log')
    axes.set_xlim(self.xlim)
    axes.set_ylim(self.ylim)
    axes.set_title('PFD_r{}_s{} {} violators'.format(self.r,
                       self.s,self.var))
    plt.savefig(os.path.join(output_dir,'r{}_s{}_{}_{}_vhorsetail.png'.format(
                self.r,self.s,self.reg,self.var)),
                bbox_inches='tight')
    plt.show()
    plt.close('all')

  def replot_nonviolators(self,bf_dir):
    '''pared down version of plot_and_compare() for replotting nonviolators 
       doesn't include the compare part. uses different file name.
       does not labels vectors
    '''
    print('replot flux nonviolators r{} s{} {} using {}'.format(
                                        self.r,self.s,self.var,bf_dir))
    max_list = []
    min_list = []
    bf_list = []
    pf_list = []
    diff_list = []
    rel_diff_list = []
    fig, axes = plt.subplots(nrows=1,ncols=1,figsize=(8,8)) #width,height
    for v in self.vectors:
      bf_df = self.bf_load_var(v,bf_dir)
      pf_df = self.pf_load_var(v)
      max_list.append(bf_df.max()[self.bf_string])
      min_list.append(bf_df.min()[self.bf_string])
      bf_df[self.bf_string].plot(ax=axes,color=self.bf_color[v-1],
                                 linestyle='--',legend=False,)
      pf_df[self.string].plot(ax=axes,color=self.pf_color[v-1],
                              linestyle='-',legend=False,)
    if self.plot_string:
      if abs(min(min_list)) < abs(max(max_list)):
        axes.text(0.02,0.96,self.plot_string,transform=axes.transAxes)
      else:
        axes.text(0.02,0.02,self.plot_string,transform=axes.transAxes)
    #axes.set_ylim(self.find_ylim(min_list,max_list))
    axes.set_xlabel('Time [y]')
    if self.reg == 'CUMULATIVE_BRINE_MASS_FLUX':
      axes.set_ylabel('Cumulative Liquid Flux [kg]')
    elif self.reg == 'CUMULATIVE_BRINE_MASS_FLUX_OUT':
      axes.set_ylabel('Cumulative Liquid Flux Out [kg]')
    elif self.reg == 'INSTANT_BRINE_MASS_FLUX':
      axes.set_ylabel('Liquid Flux [kg/y]')
    #axes.set_yscale('log')
    axes.set_title('PFD_r{}_s{} {} nonviolators'.format(self.r,
                       self.s,self.var))
    plt.savefig(os.path.join(output_dir,'r{}_s{}_{}_{}_nvhorsetail.png'.format(
                self.r,self.s,self.reg,self.var)),
                bbox_inches='tight')
    #plt.show()
    plt.close('all')

  def plot_timestep_violators(self,bf_dir):
    '''horsetail plots of timestep size vs. time. with line labels.
       file name and plot title to indicate how these are violators
    '''
    print('plot timestep of violators r{} s{} {} {} using {}'.format(self.r,self.s,
                                                        self.reg,self.var,
                                                        bf_dir))
    fig, axes = plt.subplots(nrows=1,ncols=1,figsize=(8,8)) #16,8)) #width,height
    for v in self.vectors:
      bf_df = self.bf_load_var(v,bf_dir)
      pf_df = self.pf_load_var(v)
      bf_df['dt_flow [y]'].plot(ax=axes,color=self.bf_color[v-1],
                                 linestyle='--',legend=False,)
      pf_df['dt_flow [y]'].plot(ax=axes,color=self.pf_color[v-1],
                              linestyle='-',legend=False,)
      label = 'v{:0>3}'.format(v)
      x = bf_df['dt_flow [y]'].abs().idxmax(axis=0)
      y = bf_df.loc[x,'dt_flow [y]']
      plt.annotate(
        label,
        xy=(x,y),
        xycoords='data',
        xytext=(-10,10),
        textcoords='offset points',ha='right',va='bottom',
        color=self.bf_color[v-1],
        fontsize=15)
    if self.plot_string:
      axes.text(0.02,0.96,self.plot_string,transform=axes.transAxes)
    axes.set_xlabel('Time [y]')
    axes.set_ylabel('Time Step [y]')
    axes.set_xlim(1000.,2000.)
    #axes.set_yscale('log')
    axes.set_title('PFD_r{}_s{} {} {} violators'.format(self.r,
                       self.s,self.reg,self.var))
    plt.savefig(os.path.join(output_dir,'r{}_s{}_{}_{}_vtimestep.png'.format(
                self.r,self.s,self.reg,self.var)),
                bbox_inches='tight')
    plt.show()
    plt.close('all')

  def plot_timestep_nonviolators(self,bf_dir):
    '''horsetail plots of timestep size vs. time. without line labels.
       file name and plot title to indicate how these are nonviolators
    '''
    print('plot timestep of nonviolators r{} s{} {} {} using {}'.format(self.r,self.s,
                                                        self.reg,self.var,
                                                        bf_dir))
    fig, axes = plt.subplots(nrows=1,ncols=1,figsize=(8,8)) #width,height
    for v in self.vectors:
      bf_df = self.bf_load_var(v,bf_dir)
      pf_df = self.pf_load_var(v)
      bf_df['dt_flow [y]'].plot(ax=axes,color=self.bf_color[v-1],
                                 linestyle='--',legend=False,)
      pf_df['dt_flow [y]'].plot(ax=axes,color=self.pf_color[v-1],
                              linestyle='-',legend=False,)
      label = 'v{:0>3}'.format(v)
      x = bf_df['dt_flow [y]'].abs().idxmax(axis=0)
      y = bf_df.loc[x,'dt_flow [y]']
    if self.plot_string:
      axes.text(0.02,0.96,self.plot_string,transform=axes.transAxes)
    axes.set_xlabel('Time [y]')
    axes.set_ylabel('Time Step [y]')
    #axes.set_yscale('log')
    axes.set_title('PFD_r{}_s{} {} {} nonviolators'.format(self.r,
                       self.s,self.reg,self.var))
    plt.savefig(os.path.join(output_dir,'r{}_s{}_{}_{}_nvtimestep.png'.format(
                self.r,self.s,self.reg,self.var)),
                bbox_inches='tight')
    #plt.show()
    plt.close('all')

  def pf_tabular(self,):
    '''load selected vectors, selected times, single output variable
       into a pandas DataFrame, where index (rows) is vector, and
       columns are times (analogous to Dakota response_fns).
       Sources are text files each containing outputs of single vector
       CALL pf_tabular() FIRST
    '''
    self.pf_df = pd.DataFrame(data=[],
                      index=self.vectors,
                      columns=self.times,
                      dtype=np.float)
    for v in self.vectors:
      filename = os.path.join(self.pf_dir,'pf_PFD_r{0}_s{1}_v{2:0>3}-int.dat'.format(
                 self.r,self.s,v)) #filename is only difference from orig. class
      with open (filename,'r') as f:
        header = f.readline().split(',')
        for i in xrange(len(header)):
          header[i] = header[i].strip('"\n')
      temp_df = pd.read_table(filename,delim_whitespace=True,
              index_col=0,skiprows=1,names=header[1:]).T #transpose
      try:
        self.pf_df.loc[v,:] = temp_df.loc[self.string,self.times]
      except ValueError:
        columns = []
        for t in self.times:
          clocation = temp_df.columns.get_loc(t)
          try:
            columns.append(int(clocation))
          except TypeError:
            columns.append(clocation.start)
        ilocation = temp_df.index.get_loc(self.string)
        self.pf_df.loc[v,:] = temp_df.iloc[ilocation,columns]
    #print(df)
    #return df

 ##############################################################################
if __name__ == '__main__':
  '''get command line arguments and run stuff
  '''
  string3 = ','.join([k for k in flux_dict.keys()])
  string4 = ','.join([k for k in loc_dict.keys()])

  parser = argparse.ArgumentParser(
    description='Make horsetail plots of bragflo and pflotran results.')
  parser.add_argument('-f','--bragflo_h5_file',
                      help='hdf5 file containing summary of bragflo output' +
                           ' [default = ./pfd_analysis_bragflo_summary.h5]',
                      default='pfd_analysis_bragflo_summary.h5',
                      required=False,)
  parser.add_argument('-b','--bragflo_dir',
                      help='directory containing bragflo *_fluxes.txt files' +
                      'REQUIRED',
                      required = True,)
  parser.add_argument('-p','--pflotran_dir',
                      help='directory containing pflotran *obs*.ave files' +
                           ' [default = ./s1]',
                      default = 's1',
                      required=False,)
  parser.add_argument('-s','--scenario',
                      help='int (1 thru 6) specifying scenario' +
                           ' [default = 1]',
                      default = 1,
                      required = False,)
  parser.add_argument('-r','--replicate',
                      help='int (1 thru 3) specifying replicate' +
                           ' [default = 1]',
                      default = 1,
                      required = False,)
  parser.add_argument('-v','--vectors', nargs='+',
                      help='int, list of ints [1-100], or keyword "all"' +
                           ' specifying vectors' +
                           ' [default = "all"]',
                      default='all',
                      required=False,)
  parser.add_argument('-e','--region',
                      help='To plot fluxes , string must exactly equal one of: ' +
                            string3 +
                           '\n[default = CUMULATIVE_BRINE_MASS_FLUX_OUT]',
                      default='CUMULATIVE_BRINE_MASS_FLUX_OUT',
                      required = False,)
  parser.add_argument('-a','--variable',
                      help='To plot fluxes , string must exactly equal one of: ' +
                            string4 +
                           '\n[default = borehole_culebra]',
                      default='borehole_culebra',
                      required = False,)
  parser.add_argument('-d','--output_dir',
                      help='relative path to directory for output' +
                           ' [default = .]',
                      default='.',
                      required = False,)
  parser.add_argument('-l','--layout', nargs='+',
                      help='list "horsetails","violators",or "nonviolators"' +
                           ' [default = horsetails]',
                      default='horsetails',
                      required = False,)
  parser.add_argument('-c','--colors',
                      help='choose "rainbow" or "blue_and_red"' +
                           ' [default = blue_and_red]',
                      default='blue_and_red',
                      required = False,)
  parser.add_argument('-x','--text_for_plot',
                      help='optional string to add to plots' +
                           ' [default = None]',
                      default = None,
                      required = False,)
  parser.add_argument('--xlim', nargs=2,
                      help='optional tuple x limits of plot' +
                           ' [default = None None]',
                      default = (None,None),
                      required = False,)
  parser.add_argument('--ylim', nargs=2,
                      help='optional tuple y limits of plot' +
                           ' [default = (None,None)]',
                      default = (None,None),
                      required = False,)

  args = parser.parse_args()

  #check that output dir exists. will overwrite contents with abandon
  #corresponding shell script asks for permission to overwrite and 
  #creates dir if it doesn't exist.
  output_dir = os.path.join(os.getcwd(),args.output_dir)
  if not os.path.isdir(output_dir):
    print('ERROR: {} does not exist. Create it and try again.'.format(output_dir))
    print('Exiting.')
    sys.exit()

  try:
     xlim = (float(args.xlim[0]),float(args.xlim[1]))
  except TypeError:
     xlim = (args.xlim[0],args.xlim[1]) #okay for None type default
  try:
     ylim = (float(args.ylim[0]),float(args.ylim[1]))
  except TypeError:
     ylim = (args.ylim[0],args.ylim[1]) #okay for None type default
  if args.variable in (loc_dict.keys()):
    horsetail = FluxHorsetailPlot(
                              bf_file=args.bragflo_h5_file,
                              pf_dir=args.pflotran_dir,
                              scenario=int(args.scenario),
                              replicate=int(args.replicate),
                              vectors=args.vectors,
                              region=args.region,
                              variable=args.variable,
                              #times=times,
                              colors=args.colors,
                              plot_string=args.text_for_plot,
                              xlim = xlim,
                              ylim = ylim,
                              )
    if 'violators' in args.layout:
      horsetail.replot_violators(args.bragflo_dir)
    elif 'nonviolators' in args.layout:
      horsetail.replot_nonviolators(args.bragflo_dir)
    elif 'compare_only' in args.layout:
      horsetail.plot_and_compare(args.bragflo_dir,plot_flag=False)
    elif 'horsetails_only' in args.layout:
      horsetail.plot_and_compare(args.bragflo_dir,compare_flag=False)
    elif 'violators_timestep' in args.layout:
      horsetail.plot_timestep_violators(args.bragflo_dir)
    elif 'nonviolators_timestep' in args.layout:
      horsetail.plot_timestep_nonviolators(args.bragflo_dir)
    else:
      horsetail.plot_and_compare(args.bragflo_dir)
    
