import sys
import os
try:
    pflotran_dir = os.environ['PFLOTRAN_DIR']
except KeyError:
    print('PFLOTRAN_DIR must point to PFLOTRAN installation directory and be defined in system environment variables.')
    sys.exit(1)
sys.path.append(pflotran_dir + '/src/python')

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import math
import pflotran as pft

mpl.rcParams['font.size']=12
#mpl.rcParams['font.weight']='bold'
mpl.rcParams['lines.linewidth']=3
mpl.rcParams['xtick.labelsize']=18
mpl.rcParams['ytick.labelsize']=18

def make_subplots(nrow, ncol, nplots, filenames, columns, name, titles):
  for iplot in range(nplots):
    plt.subplot(nrow,ncol,iplot+1)
    plt.title(titles[iplot])
    plt.xlabel('Time (years)', fontsize=18, fontweight='bold')
    plt.ylabel('{}'.format(titles[iplot]), fontsize=18, fontweight='bold')

    plt.xlim(0.,1.e4)
    #plt.ylim(24., 31.)
    plt.grid(True)

    for icol in range(iplot*len(columns)/nplots,iplot*len(columns)/nplots+len(columns)/nplots):
      ifile = icol
      data = pft.Dataset(filenames[ifile],1,columns[icol])
    #label with column header
    #  string = data.get_name('yname').split()[3].split('_')[1]
    #or label with file name
    #   string = filenames[ifile]
    #or label with name assigned above
      string = name[icol]
      plt.plot(data.get_array('x'),data.get_array('y'),color=colors[icol],linestyle=linestyles[icol],label=string)

path = []
path.append('.')

titles = []
files = []
columns = []
name = []
linestyles = []
colors = []
basestyles = ['-' for x in range(8)]
basecolors = ['red','darkorange','gold','yellowgreen','green','turquoise','blue','purple']

#volume ave pressure
titles.append('Vol Ave Liquid P (Pa)')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(2)
name.append('rWAS_AREA')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(7)
name.append('rSROR')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(12)
name.append('rNROR')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(17)
name.append('rOPS_AREA')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(22)
name.append('rEXP_AREA')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(27)
name.append('rSPCS')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(32)
name.append('rMPCS')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(37)
name.append('rNPCS')

linestyles.extend(basestyles)
colors.extend(basecolors)

#volume ave liquid saturation
titles.append('Vol Ave Liquid S')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(3)
name.append('rWAS_AREA')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(8)
name.append('rSROR')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(13)
name.append('rNROR')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(18)
name.append('rOPS_AREA')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(23)
name.append('rEXP_AREA')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(28)
name.append('rSPCS')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(33)
name.append('rMPCS')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(38)
name.append('rNPCS')

linestyles.extend(basestyles)
colors.extend(basecolors)

#volume ave gas saturation
titles.append('Vol Ave Gas S')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(4)
name.append('rWAS_AREA')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(9)
name.append('rSROR')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(14)
name.append('rNROR')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(19)
name.append('rOPS_AREA')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(24)
name.append('rEXP_AREA')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(29)
name.append('rSPCS')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(34)
name.append('rMPCS')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(39)
name.append('rNPCS')

linestyles.extend(basestyles)
colors.extend(basecolors)

#volume ave effective porosity
titles.append('Vol Ave Eff Por')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(5)
name.append('rWAS_AREA')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(10)
name.append('rSROR')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(15)
name.append('rNROR')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(20)
name.append('rOPS_AREA')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(25)
name.append('rEXP_AREA')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(30)
name.append('rSPCS')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(35)
name.append('rMPCS')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(40)
name.append('rNPCS')

linestyles.extend(basestyles)
colors.extend(basecolors)

#total brine volume
titles.append('Total Brine Volume (m^3)')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(6)
name.append('rWAS_AREA')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(11)
name.append('rSROR')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(16)
name.append('rNROR')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(21)
name.append('rOPS_AREA')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(26)
name.append('rEXP_AREA')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(31)
name.append('rSPCS')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(36)
name.append('rMPCS')
files.append('./s1/s1_v1_pflotran-obs-0.lumped')
columns.append(41)
name.append('rNPCS')

linestyles.extend(basestyles)
colors.extend(basecolors)

filenames = pft.get_full_paths(path,files)

f = plt.figure(figsize=(24,16)) #adjust size to fit plots (width, height)
#f.suptitle("Cement, line loads: Temperature (degrees C)",fontsize=16)

make_subplots(2,3,5,filenames,columns,name,titles)

#Choose a location for the legend. #This is associated with the last plot
#'best'         : 0, (only implemented for axis legends)
#'upper right'  : 1,
#'upper left'   : 2,
#'lower left'   : 3,
#'lower right'  : 4,
#'right'        : 5,
#'center left'  : 6,
#'center right' : 7,
#'lower center' : 8,
#'upper center' : 9,
#'center'       : 10,
#plt.legend(loc=(1.20,0.175),title='FRACTURE REALIZATION')
plt.legend(loc=0)
# xx-small, x-small, small, medium, large, x-large, xx-large, 12, 14
plt.setp(plt.gca().get_legend().get_texts(),fontsize=14,fontweight='normal')
plt.setp(plt.gca().get_legend().get_texts(),linespacing=0.)
plt.setp(plt.gca().get_legend().get_frame().set_fill(False))
plt.setp(plt.gca().get_legend().draw_frame(False))
#plt.gca().yaxis.get_major_formatter().set_powerlimits((-1,1))

#adjust blank space and show/save the plot
f.subplots_adjust(hspace=0.2,wspace=0.2, bottom=.12,top=.9, left=.14,right=.9)
plt.savefig('lumped_params.png',bbox_inches='tight')
plt.show()
