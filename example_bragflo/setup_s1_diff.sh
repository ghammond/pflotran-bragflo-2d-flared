#! /bin/bash 

for i in {001,002,003,004,005,006,007,008,009,010}; do
  #                    bragflo filename        pflotran filename         plot=yes/no
  python difference.py bf2_PFD_r1_s1_v$i.bf.h5 ../s1/pf_PFD_r1_s1_v$i.h5 plot=no #yes
  python ../write_xmf.py bf2_PFD_r1_s1_v$i'_difference' ../grid/domain-elev-equal
done
