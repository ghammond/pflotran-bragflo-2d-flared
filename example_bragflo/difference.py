'''
Open bragflo results and pflotran results.
Calculate difference.
Save difference to new .h5 file, cell-indexed
Emily 8.17.17
'''

import numpy as np
from h5py import *
import sys
import matplotlib.pyplot as plt

def bragflo_pflotran_diff():

  bragflo_filename = sys.argv[1]
  pflotran_filename = sys.argv[2]
  plot_choice = sys.argv[3][5:]

  if plot_choice == 'yes' or plot_choice == 'Yes':
    plot_flag = True
  elif plot_choice == 'no' or plot_choice == 'No':
    plot_flag = False
  outfilename = bragflo_filename.split('.')[0] + '_difference.h5'
  label = pflotran_filename[-10:]
  label = label[:-3]

  #BRAGFLO names
  lpname = 'PRESBRIN'
  gpname = 'PRESGAS'
  cpname = 'PCGW'
  lsname = 'SATBRINE'
  gsname = 'SATGAS'
  ldname = 'DENBRINE'
  gdname = 'DENGAS'
  pornam = 'POROS'

  #PFLOTRAN names
  lppflo = 'Liquid Pressure [Pa]'
  gppflo = 'Gas Pressure [Pa]'
  cppflo = 'Capillary Pressure [Pa]'
  lspflo = 'Liquid Saturation'
  gspflo = 'Gas Saturation'
  ldpflo = 'Liquid Density [kg_m^3]'
  gdpflo = 'Gas Density [kg_m^3]'
  porpfl = 'Effective Porosity'
  matpfl = 'Material ID'

  #open files
  h5bf = File(bragflo_filename,'r')
  h5pf = File(pflotran_filename,'r')
  h5out = File(outfilename,'w')

  print('Calculating differences . . . ')
  times = h5bf.keys()
  max_liq_p = np.zeros(len(times)); max_gas_p = np.zeros(len(times))
  max_cap_p = np.zeros(len(times))
  max_liq_s = np.zeros(len(times)); max_gas_s = np.zeros(len(times))
  max_liq_d = np.zeros(len(times)); max_gas_d = np.zeros(len(times))
  avg_liq_p = np.zeros(len(times)); avg_gas_p = np.zeros(len(times))
  avg_cap_p = np.zeros(len(times))
  avg_liq_s = np.zeros(len(times)); avg_gas_s = np.zeros(len(times))
  avg_liq_d = np.zeros(len(times)); avg_gas_d = np.zeros(len(times))
  time_array = np.zeros(len(times));	

  count = 0
  for tkey in times:
    if tkey.startswith('Time'):
      bf_liq_p = np.array(h5bf['{}/{}'.format(tkey,lpname)][:][:],'=f8')
      bf_gas_p = np.array(h5bf['{}/{}'.format(tkey,gpname)][:][:],'=f8')
      bf_cap_p = np.array(h5bf['{}/{}'.format(tkey,cpname)][:][:],'=f8')
      bf_liq_s = np.array(h5bf['{}/{}'.format(tkey,lsname)][:][:],'=f8')
      bf_gas_s = np.array(h5bf['{}/{}'.format(tkey,gsname)][:][:],'=f8')
      bf_liq_d = np.array(h5bf['{}/{}'.format(tkey,ldname)][:][:],'=f8')
      bf_gas_d = np.array(h5bf['{}/{}'.format(tkey,gdname)][:][:],'=f8')
      bf_poros = np.array(h5bf['{}/{}'.format(tkey,pornam)][:][:],'=f8')
      ndown = len(bf_liq_p[:])
      nacross = len(bf_liq_p[0][:])
      #make cell-indexed arrays
      ci_liq_p = np.zeros((ndown*nacross),'=f8')
      ci_gas_p = np.zeros((ndown*nacross),'=f8')
      ci_cap_p = np.zeros((ndown*nacross),'=f8')
      ci_liq_s = np.zeros((ndown*nacross),'=f8')
      ci_gas_s = np.zeros((ndown*nacross),'=f8')
      ci_liq_d = np.zeros((ndown*nacross),'=f8')
      ci_gas_d = np.zeros((ndown*nacross),'=f8')
      ci_poros = np.zeros((ndown*nacross),'=f8')
      for j in xrange(nacross):
        for i in xrange(ndown):
          index = i + j*ndown
          ci_liq_p[index] = bf_liq_p[i][j]
          ci_gas_p[index] = bf_gas_p[i][j]
          ci_cap_p[index] = bf_cap_p[i][j]
          ci_liq_s[index] = bf_liq_s[i][j]
          ci_gas_s[index] = bf_gas_s[i][j]
          ci_liq_d[index] = bf_liq_d[i][j]
          ci_gas_d[index] = bf_gas_d[i][j]
          ci_poros[index] = bf_poros[i][j]
      #Find equivalent pflotran times
      time = float(tkey.split()[1])
      ptimes = h5pf.keys()
      for pkey in ptimes:
        if float(pkey.split()[2]) == time:
          print(pkey)
          p_liq_p = np.array(h5pf['{}/{}'.format(pkey,lppflo)],'=f8')
          p_gas_p = np.array(h5pf['{}/{}'.format(pkey,gppflo)],'=f8')
          p_cap_p = np.array(h5pf['{}/{}'.format(pkey,cppflo)],'=f8')
          p_liq_s = np.array(h5pf['{}/{}'.format(pkey,lspflo)],'=f8')
          p_gas_s = np.array(h5pf['{}/{}'.format(pkey,gspflo)],'=f8')
          p_liq_d = np.array(h5pf['{}/{}'.format(pkey,ldpflo)],'=f8')
          p_gas_d = np.array(h5pf['{}/{}'.format(pkey,gdpflo)],'=f8')
          p_poros = np.array(h5pf['{}/{}'.format(pkey,porpfl)],'=f8')
          p_mater = np.array(h5pf['{}/{}'.format(pkey,matpfl)],'=i4')
          #calculate differences - use absolute value
          d_liq_p = abs(ci_liq_p - p_liq_p)
          d_gas_p = abs(ci_gas_p - p_gas_p)
          d_cap_p = abs(ci_cap_p - p_cap_p)  # these are all zero?
          d_liq_s = abs(ci_liq_s - p_liq_s)
          d_gas_s = abs(ci_gas_s - p_gas_s)
          d_liq_d = abs(ci_liq_d - p_liq_d)
          d_gas_d = abs(ci_gas_d - p_gas_d)
          d_poros = abs(ci_poros - p_poros)
          #calculate relative (fractional) differences
          drel_liq_p = abs(d_liq_p/ci_liq_p)
          max_liq_p[count] = max(drel_liq_p)
          avg_liq_p[count] = np.mean(drel_liq_p)
          drel_gas_p = abs(d_gas_p/ci_gas_p)
          max_gas_p[count] = max(drel_gas_p)
          avg_gas_p[count] = np.mean(drel_gas_p)
          drel_cap_p = abs(d_cap_p/ci_cap_p)
          max_cap_p[count] = max(drel_cap_p)
          avg_cap_p[count] = np.mean(drel_cap_p)
          drel_liq_s = abs(d_liq_s/ci_liq_s)
          max_liq_s[count] = abs(max(d_liq_s))
          avg_liq_s[count] = abs(np.mean(d_liq_s))
          drel_gas_s = abs(d_gas_s/ci_gas_s)
          max_gas_s[count] = abs(max(d_gas_s))
          avg_gas_s[count] = abs(np.mean(d_gas_s))
          drel_liq_d = abs(d_liq_d/ci_liq_d) 
          max_liq_d[count] = max(drel_liq_d)
          avg_liq_d[count] = np.mean(drel_liq_d)
          drel_gas_d = abs(d_gas_d/ci_gas_d)
          max_gas_d[count] = max(drel_gas_d)
          avg_gas_d[count] = np.mean(drel_gas_d)
          drel_poros = abs(d_poros/ci_poros)
          #write to new h5 file
          year = tkey.split()[1]
          #differences
          h5out.create_dataset('{} Time {} y/{}_Difference'.format(
                               count,year,lpname),data=d_liq_p)
          h5out.create_dataset('{} Time {} y/{}_Difference'.format(
                               count,year,gpname),data=d_gas_p)
          h5out.create_dataset('{} Time {} y/{}_Difference'.format(
                               count,year,cpname),data=d_cap_p)
          h5out.create_dataset('{} Time {} y/{}_Difference'.format(
                               count,year,lsname),data=d_liq_s)
          h5out.create_dataset('{} Time {} y/{}_Difference'.format(
                               count,year,gsname),data=d_gas_s)
          h5out.create_dataset('{} Time {} y/{}_Difference'.format(
                               count,year,ldname),data=d_liq_d)
          h5out.create_dataset('{} Time {} y/{}_Difference'.format(
                               count,year,gdname),data=d_gas_d)
          h5out.create_dataset('{} Time {} y/{}_Difference'.format(
                               count,year,pornam),data=d_poros)
          #percent differences
          h5out.create_dataset('{} Time {} y/{}_Relative_Diff'.format(
                               count,year,lpname),data=drel_liq_p)
          h5out.create_dataset('{} Time {} y/{}_Relative_Diff'.format(
                               count,year,gpname),data=drel_gas_p)
          h5out.create_dataset('{} Time {} y/{}_Relative_Diff'.format(
                               count,year,cpname),data=drel_cap_p)
          h5out.create_dataset('{} Time {} y/{}_Relative_Diff'.format(
                               count,year,lsname),data=drel_liq_s)
          h5out.create_dataset('{} Time {} y/{}_Relative_Diff'.format(
                               count,year,gsname),data=drel_gas_s)
          h5out.create_dataset('{} Time {} y/{}_Relative_Diff'.format(
                               count,year,ldname),data=drel_liq_d)
          h5out.create_dataset('{} Time {} y/{}_Relative_Diff'.format(
                               count,year,gdname),data=drel_gas_d)
          h5out.create_dataset('{} Time {} y/{}_Relative_Diff'.format(
                               count,year,pornam),data=drel_poros)
          #materials
          h5out.create_dataset('{} Time {} y/{}'.format(
                               count,year,matpfl),data=p_mater)
      
          time_array[count] = time
          count += 1

  #close files  
  h5bf.close()
  h5pf.close()
  h5out.close()


  if (plot_flag):
    #plot maximum % differences
    print 'Busy making plots . . .' 

    error_max = 1.e3
    ticks_y = [0.25,0.50,0.75,1.00,
              2.5,5.0,7.5,10.0,
              25.0,50.0,75.0,100.0,
              250.0,500.0,750.0,1000.0]
    ticks_x = [1.00,
              2.5,5.0,7.5,10.0,
              25.0,50.0,75.0,100.0,
              250.0,500.0,750.0,1000.0,
              2500.0,5000.0,7500.0,10000.0]
    
    plt.plot(time_array,max_liq_p,'ro',time_array,avg_liq_p,'bo',alpha=0.7);
    plt.yscale('symlog',linthreshy=1.0);
    plt.xscale('symlog')
    plt.xlabel('Time [yr]');
    plt.ylabel('Difference [%]', color='k');
    plt.xlim([0,max(time_array)]);
    plt.ylim([0,error_max]);
    plt.legend(('Maximum','Average'),loc='best');
    plt.title('Difference in Liquid Pressure: ' + label); 
    plt.yticks(ticks_y); 
    plt.xticks(ticks_x);
    plt.grid(which='major',alpha=1.0);
  #  plt.show()
    plt.savefig('max_diff_liqp_'+label+'.png',dpi=300)
    plt.close()

    plt.plot(time_array,max_gas_p,'ro',time_array,avg_gas_p,'bo',alpha=0.7);
    plt.yscale('symlog',linthreshy=1.0);
    plt.xscale('symlog')
    plt.xlabel('Time [yr]');
    plt.ylabel('Difference [%]', color='k');
    plt.xlim([0,max(time_array)]);
    plt.ylim([0,error_max]);
    plt.legend(('Maximum','Average'),loc='best');
    plt.title('Difference in Gas Pressure: ' + label);  
    plt.yticks(ticks_y); 
    plt.xticks(ticks_x);
    plt.grid(which='major',alpha=1.0);
  #  plt.show()
    plt.savefig('max_diff_gasp_'+label+'.png',dpi=300)
    plt.close()

    plt.semilogx(time_array,max_liq_s,'ro',time_array,avg_liq_s,'bo',alpha=0.7);
    plt.xlabel('Time [yr]');
    plt.ylabel('Absolute Difference', color='k');
    plt.xlim([0,max(time_array)]);
    plt.ylim([0,1.0]);
    plt.legend(('Maximum','Average'),loc='best');
    plt.title('Difference in Liquid Saturation: ' + label);
    plt.grid();
  #  plt.show()
    plt.savefig('max_diff_liqs_'+label+'.png',dpi=300)
    plt.close()

    plt.semilogx(time_array,max_gas_s,'ro',time_array,avg_gas_s,'bo',alpha=0.7);
    plt.xlabel('Time [yr]');
    plt.ylabel('Absolute Difference', color='k');
    plt.xlim([0,max(time_array)]);
    plt.ylim([0,1.0]);
    plt.legend(('Maximum','Average'),loc='best');
    plt.title('Difference in Gas Saturation: ' + label);
    plt.grid();
  #  plt.show()
    plt.savefig('max_diff_gass_'+label+'.png',dpi=300)
    plt.close()

  #  plt.plot(time_array,max_cap_p,'ro',time_array,avg_cap_p,'bo',alpha=0.7);
  #  plt.yscale('symlog',linthreshy=1.0);
  #  plt.xscale('symlog')
  #  plt.xlabel('Time [yr]');
  #  plt.ylabel('Difference [%]', color='k');
  #  plt.xlim([0,max(time_array)]);
  #  plt.ylim([0,error_max]);
  #  plt.legend(('Maximum','Average'),loc='best');
  #  plt.title('Difference in Capillary Pressure: ' + label);  
  #  plt.yticks(ticks_y); 
  #  plt.xticks(ticks_x);
  #  plt.grid(which='major',alpha=1.0);
  ##  plt.show()
  #  plt.savefig('max_diff_capp_'+label+'.png',dpi=300)
  #  plt.close()

    plt.plot(time_array,max_liq_d,'ro',time_array,avg_liq_d,'bo',alpha=0.7);
    plt.yscale('symlog',linthreshy=1.0);
    plt.xscale('symlog')
    plt.xlabel('Time [yr]');
    plt.ylabel('Difference [%]', color='k');
    plt.xlim([0,max(time_array)]);
    plt.ylim([0,error_max]);
    plt.legend(('Maximum','Average'),loc='best');
    plt.title('Max Difference in Liquid Density: ' + label); 
    plt.yticks(ticks_y); 
    plt.xticks(ticks_x);
    plt.grid(which='major',alpha=1.0);
  #  plt.show()
    plt.savefig('max_diff_liqd_'+label+'.png',dpi=300)
    plt.close()

    plt.plot(time_array,max_gas_d,'ro',time_array,avg_gas_d,'bo',alpha=0.7);
    plt.yscale('symlog',linthreshy=1.0);
    plt.xscale('symlog')
    plt.xlabel('Time [yr]');
    plt.ylabel('Difference [%]', color='k');
    plt.xlim([0,max(time_array)]);
    plt.ylim([0,error_max]);
    plt.legend(('Maximum','Average'),loc='best');
    plt.title('Difference in Gas Density: ' + label);   
    plt.yticks(ticks_y); 
    plt.xticks(ticks_x);
    plt.grid(which='major',alpha=1.0);
  #  plt.show()
    plt.savefig('max_diff_gasd_'+label+'.png',dpi=300)
    plt.close()

  print('done')

if __name__ == '__main__':
  bragflo_pflotran_diff()



