'''history.py
   read bragflo .bf.h5 history variables to a pandas df? that I can use for plotting
   alongside pflotran obs point data - which is very easy to read to a dataframe
   Emily Stein (ergiamb@sandia.gov)
'''
import sys
import os
import argparse
import numpy as np
from h5py import File
#import h5py
import pandas as pd
import matplotlib.pyplot as plt

def read_bf_history(input_dir,replicate,scenario,vector):

  var_dict = {'H001':'PRESBRIN','H007':'RELPERMB','H008':'RELPERMG','H017':'SATBRINE'}
  reg_dict = {'0001':'40_10','0002':'40_11','0003':'40_12', #i,j indices ops
                  '0004':'41_10','0005':'41_11','0006':'41_12', #i,j indices ops
                  '0007':'42_10','0008':'42_11','0009':'42_12', #i,j indices ops
                  '0010':'43_10','0011':'43_11','0012':'43_12', #i,j indices shaft
                  '0013':'44_10','0014':'44_11','0015':'44_12', #i,j indices exp
                  '0016':'45_10','0017':'45_11','0018':'45_12', #i,j indices exp
                 }
          
  var_list = ['PRESBRIN','RELPERMB','RELPERMG','SATBRINE']
          
  # Define various dimensions
  num_reg= len(reg_dict)
  num_var= len(var_dict)
  sec_per_yr = 3.1536e7
  # add one because in the history_variables_table, 
  # the TIMESEC data was added as the first column
  obs_list = ['TIMESEC']
  for i in range(num_var):
    for j in range(num_reg):
      obs_list.append('{}_{}'.format(reg_dict['{:0>4}'.format(j+1)],var_list[i]))
  #print obs_list

  hf = File(os.path.join(input_dir,'bf2_PFD_r{}_s{}_v{:0>3}.bf.h5'.format(replicate,scenario,vector)))
  dataset = '/output_tables/history_variables_table'
  df = pd.DataFrame(data=hf[dataset][:,1:],index=hf[dataset][:,0]/sec_per_yr,columns=obs_list[1:])
  hf.close()
  #print(df)
  return df 

def pf_load_var(input_dir,replicate,scenario,vector):
    filename = os.path.join(input_dir,'pf_PFD_r{0}_s{1}_v{2:0>3}-obs-0.tec'.format(
                replicate,scenario,vector))
    with open (filename,'r') as f:
      header = f.readline().split(',')
      for i in xrange(len(header)): #get rid of coordinates
        header[i] = header[i].strip('"\n')
        header[i] = ' '.join(header[i].split()[:-3])
    df = pd.read_table(filename,delim_whitespace=True,
              index_col=0,skiprows=1,names=header[1:])
    #print(df)
    return df

var_dict = {#bragflo:pflotran
            'PRESBRIN':'Liquid Pressure [Pa]',
            'RELPERMB':'Liquid Mobility [1/Pa-s]',
            'RELPERMG':'Gas Mobility [1/Pa-s]',
            'SATBRINE':'Liquid Saturation',
           }
reg_dict = {#bragflo:pflotran -could have been clever with for loops
            '40_10':'rOPS_AREA (652)',
            '40_11':'rOPS_AREA (720)',
            '40_12':'rOPS_AREA (788)',
            '41_10':'rOPS_AREA (653)',
            '41_11':'rOPS_AREA (721)',
            '41_12':'rOPS_AREA (789)',
            '42_10':'rOPS_AREA (654)',
            '42_11':'rOPS_AREA (722)',
            '42_12':'rOPS_AREA (790)',
            '43_10':'rSHAFT_unofficial (655)',
            '43_11':'rSHAFT_unofficial (723)',
            '43_12':'rSHAFT_unofficial (791)',
            '44_10':'rEXP_AREA (656)',
            '44_11':'rEXP_AREA (724)',
            '44_12':'rEXP_AREA (792)',
            '45_10':'rEXP_AREA (657)',
            '45_11':'rEXP_AREA (725)',
            '45_12':'rEXP_AREA (793)',
           }
liq_viscosity = 2.1e-3 #Pa-s
gas_viscosity = 8.93389e-6 #Pa-s

###############################################################################
if __name__ == '__main__':
  colors = ['navy','blue','dodgerblue','lightblue','mediumvioletred','magenta','red','darkorange',]
  bf_list = []
  pf_list = []
  for sim in ['02','03','04','05']:
    bf_dir = '/home1/ergiamb/pfd_analysis/s4r2v099_hist{}'.format(sim)
    pf_dir = 'rerun_r2s4v99_hist{}'.format(sim)
    bf_list.append(read_bf_history(bf_dir,2,4,99))
    pf_list.append(pf_load_var(pf_dir,2,4,99))
  nrows = 3
  ncols = 6
  #put lowest j on the bottom, which is highest row id in fig
  for var in var_dict:
    fig,axes = plt.subplots(nrows=3,ncols=6,figsize=(48,24)) #width,height
    fig.suptitle('r2_s4_v099 OPS SHAFT EXP {}'.format(var))
    row = nrows - 1
    for j in range(10,13):
      col = 0
      for i in range(40,46):
        reg = '{}_{}'.format(i,j)
        tol = 2
        for bf_df in bf_list:
          bf_df['{}_{}'.format(reg,var)].plot(ax=axes[row,col],
              marker='x',color=colors[tol-2],legend=True,label='bf_{:0>2}'.format(tol))
          tol += 1
        tol = 2
        for pf_df in pf_list:
          if var == 'RELPERMB':
            df_temp = pf_df['{} {}'.format(var_dict[var],reg_dict[reg])]*liq_viscosity
            df_temp.plot(ax=axes[row,col],marker='+',color=colors[tol+2],legend=True,label='pf_{:0>2}'.format(tol))
            axes[row,col].set_ylabel('Liquid Relative Permeability')
          elif var == 'RELPERMG':
            df_temp = pf_df['{} {}'.format(var_dict[var],reg_dict[reg])]*gas_viscosity
            df_temp.plot(ax=axes[row,col],marker='+',color=colors[tol+2],legend=True,label='pf_{:0>2}'.format(tol))
            axes[row,col].set_ylabel('Gas Relative Permeability')
          else:
            pf_df['{} {}'.format(var_dict[var],reg_dict[reg])].plot(ax=axes[row,col],
                  marker='+',color=colors[tol+2],legend=True,label='pf_{:0>2}'.format(tol))
            axes[row,col].set_ylabel('Liquid Saturation') #gets overwritten if pressure
          tol += 1
        axes[row,col].grid()
        axes[row,col].set_xlim(0.,400.) #yr
        axes[row,col].set_xlabel('Time [y]')
        if var == 'PRESBRIN':
          axes[row,col].set_ylim(0.,6.e5)
          axes[row,col].set_ylabel('Pressure [Pa]')
        else:
          axes[row,col].set_ylim(-0.05,1.05)
        col += 1
      row -= 1
    plt.savefig('r2s4v099_hist_{}.png'.format(var),boundingbox='tight')
#    plt.show()
    plt.close('all')
        
