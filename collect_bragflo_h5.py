#!/usr/bin/python
import sys
import os
import argparse
import numpy as np
import h5py
import scipy
import scipy.integrate

"""
This creates an h5 file with the following structure:
/Scenario scenario_number/TIME_YEARS
/Scenario scenario_number/TIME_SECONDS
/Scenario scenario_number/region_name/variable_name[run_number,time]
/Scenario scenario_number/CUMULATIVE_BRINE_MASS_FLUX/flux_point_name[run_number,time]

"""

def collect_bragflo_h5(input_dir,output_dir,scenario,vectors):

  SCENARIO = scenario
  num_runs = vectors

  fname_analysis_summary = 'pfd_analysis_bragflo_summary_scenario' + '{scen:0>1}'.format(scen=SCENARIO) +'.h5';
  directory_inputs = input_dir
  directory_output = output_dir
  input_prefix = 'bf2_PFD';
  input_suffix = '.bf.h5';

  ###############################################################################
  ## Define Variables and Regions of Interest
  ###############################################################################

  # Define region references 
  # NOTE this uses python/c zero-based indexing (the fortran-to-c ordering was done in the h5 converter)
  # also, the waste area includes the borehole
  reg_WAS_AREA = np.index_exp[22:29,  9:12, 0]; #python does not include last
  reg_SPCS     = np.index_exp[29:31,  9:12, 0];
  reg_SROR     = np.index_exp[31:33,  9:12, 0];
  reg_MPCS     = np.index_exp[33:35,  9:12, 0];
  reg_NROR     = np.index_exp[35:37,  9:12, 0];
  reg_NPCS     = np.index_exp[37:39,  9:12, 0];
  reg_OPS      = np.index_exp[39:42,  9:12, 0];
  reg_SHAFT    = np.index_exp[   42,  9:12, 0];
  reg_EXP      = np.index_exp[43:45,  9:12, 0];

  list_region_names = [ 'WAS_AREA', 'SPCS', 'SROR',
                        'MPCS', 'NROR', 'NPCS',
                        'OPS', 'SHAFT', 'EXP' ];
  list_variable_names = [ 'PRESBRIN', 'POROS', 'SATBRINE','BRINEVOL',
                          'FECONC', 'CELLCONC', 'MGOC' ];
  list_region_refs = [ reg_WAS_AREA, reg_SPCS, reg_SROR,
                       reg_MPCS, reg_NROR, reg_NPCS,
                       reg_OPS, reg_SHAFT, reg_EXP ];

  # Define various dimensions
  num_printtimes = 68; # not 65, - should read in
  num_regions = len(list_region_refs)
  num_variables = len(list_variable_names)
  nx = 68;
  ny = 33;
  nz =  1;

  # Define points at which to calculate cumulative flux vs time
  # The flux data is pulled from the history_variables_table, 
  # which is outuput at every timestep.
  # Since each run will have a difference series of timesteps, 
  # in this summary h5 file, we will report 
  # the cumulative flux at each binary print time, which is the same for each run.

  # add one because in the history_variables_table, 
  # the TIMESEC data was added as the first column
  flux_dict = {#column index:name
               231+1:'lwb_south_mb139',
               232+1:'lwb_south_anhydrite',
               233+1:'lwb_south_mb138',
               234+1:'lwb_south_culebra',
               235+1:'lwb_north_mb139',
               236+1:'lwb_north_anhydrite',
               237+1:'lwb_north_mb138',
               238+1:'lwb_north_culebra',
               260+1:'borehole_mb138',
               261+1:'borehole_salado',
               262+1:'borehole_culebra',
               263+1:'shaft_mb138',
               264+1:'shaft_salado',
               265+1:'shaft_culebra',
              }
  num_flux_points = len(flux_dict)
  max_timesteps = 20001;
  reference_brine_density = 1220.00; # kg/m^3 1.2200E+03

  ###############################################################################
  ## Allocate memory arrays
  ###############################################################################

  # Reading access pattern: table[i_run, i_time, i_var, i_reg]
  # Writing access pattern: table[i_reg, i_var, i_run, i_time]

  # Allocate 2d python list of array objects mgr[region][variable]=np.array([run,time]) 
  # table_vars_regions = [ [ np.zeros((num_runs,num_printtimes), np.float64) for j in range(num_variables) ] for i in range(num_regions) ];

  # Allocate one monolithic, 4d numpy array, in the read-access order
  # 1800*68*6*9 * (8/(1024^2)) = 50.427 MiB
  table_big_data = np.zeros((num_runs,num_printtimes,num_variables,num_regions), np.float64);
  table_big_data.fill(np.nan) #avoid calcs on failed runs

  # Allocate working arrays to hold grid data
  arr_working_grid_data = np.zeros((nx,ny,nz), np.float64);
  arr_working_grid_data.fill(np.nan) #doesn't probably matter whether zero or nan
  arr_working_satbrine = np.zeros((nx,ny,nz), np.float64);
  arr_working_satbrine.fill(np.nan) #doesn't probably matter whether zero or nan
  arr_working_poros = np.zeros((nx,ny,nz), np.float64);
  arr_working_poros.fill(np.nan) #doesn't probably matter whether zero or nan

  # Allocate array to hold grid cell volumes
  arr_gridvolume = np.zeros((nx,ny,nz), np.float64);
  arr_gridvolume.fill(np.nan) #doesn't probably matter whether zero or nan

  # Allocate one monolithic, 3d numpy array, in the read-access order, for the flux points
  # 1800*68*6*9 * (8/(1024^2)) = 50.427 MiB
  table_big_data_fluxes = np.zeros((num_runs,num_printtimes,num_flux_points), np.float64);
  table_big_data_fluxes.fill(np.nan) #avoid calcs on failed runs
  table_big_data_inst_fluxes = np.zeros((num_runs,num_printtimes,num_flux_points), np.float64);
  table_big_data_inst_fluxes.fill(np.nan) #avoid calcs on failed runs

  # We should preallocate arrays for the per-timestep flux data
  times_s = np.zeros((max_timesteps,), np.float64);
  flux_data = np.zeros((max_timesteps,num_flux_points), np.float64);
  cumulative_flux = np.zeros((max_timesteps,num_flux_points), np.float64);
  cumulative_flux_printtimes = np.zeros((num_printtimes,num_flux_points), np.float64);
  kbin = np.zeros((max_timesteps,), np.float64);
  indices_print_times = np.zeros((num_printtimes,), np.int64);
  #I think printtimes, kbin, and indices want zeros maybe for true/false
  times_s.fill(np.nan)
  flux_data.fill(np.nan)
  cumulative_flux.fill(np.nan)

  ###############################################################################
  ## Read phase: Loop over each input hdf5 file
  ###############################################################################

  # Loop over runs (1-300)
  have_times = False
  for i_run in range(num_runs) :
  
    # Calculate file name
    # # To order by vector, replicate, then scenario, use:
    # note, i_run is zero-based
    VECTOR = (i_run%100) + 1;
    REPLICATE = ((i_run//100)%3) + 1;
    #SCENARIO = (i_run//300) + 1;
  
    fname_bragflo_h5 = input_prefix + '_r{rep:0>1}_s{scen:0>1}_v{vec:0>3}'.format(rep=REPLICATE,scen=SCENARIO,vec=VECTOR)  + input_suffix;
    print( fname_bragflo_h5 + '\n' );
  
    # Open input hdf5
    #fname_bragflo_h5 = 'bf2_PFD_r1_s2_v001.bf.h5';
    try: 
      hf_bragflo_h5 = h5py.File(os.path.join(directory_inputs,fname_bragflo_h5), mode='r');
    except:
      print( 'Cannot open ' + fname_bragflo_h5 + ' ; skipping... \n');
      continue;
  
    # Get snapshot times
    if not have_times: 
      snapshot_times_s = hf_bragflo_h5['/output_times/time_s'][...]; # the [...] tells python to return the actual data, nota reference to the h5 dataset
      snapshot_times_y = hf_bragflo_h5['/output_times/time_y'][...]; # the [...] tells python to return the actual data, nota reference to the h5 dataset
      have_times = True
  
    snapshot_time_refs = hf_bragflo_h5['/output_times/snapshot_refs'][...];
    #num_snapshot_times = np.size(snapshot_times_y);
    
    # Get the cell volumes
    hf_bragflo_h5['/grid/gridvol'].read_direct(arr_gridvolume);
  
    # Loop over times
    for i_time in range(len(snapshot_time_refs)): #range(num_printtimes) :
      for i_var in range(num_variables) : # Loop over variables
        if list_variable_names[i_var] == 'BRINEVOL':
          hf_bragflo_h5[snapshot_time_refs[i_time]]['SATBRINE'].read_direct(arr_working_satbrine);
          hf_bragflo_h5[snapshot_time_refs[i_time]]['POROS'].read_direct(arr_working_poros);
          for i_reg in range(num_regions): #loop over regions to calc total brine volume in each
            table_big_data[i_run,i_time,i_var,i_reg] = np.sum( arr_working_satbrine[list_region_refs[i_reg]] *
                                                               arr_working_poros[list_region_refs[i_reg]] *
                                                               arr_gridvolume[list_region_refs[i_reg]])
        else:
          hf_bragflo_h5[snapshot_time_refs[i_time]][list_variable_names[i_var]].read_direct(arr_working_grid_data);
          for i_reg in range(num_regions) : #loop over regions to calc volume-averaged quantities
            table_big_data[i_run,i_time,i_var,i_reg] = np.average( arr_working_grid_data[list_region_refs[i_reg]], axis=None, weights=arr_gridvolume[list_region_refs[i_reg]] );
  
    # Get fluxes
    times_s = hf_bragflo_h5['/output_tables/history_variables_table'][:,0][...];
    flux_data = hf_bragflo_h5['/output_tables/history_variables_table'][:,sorted(flux_dict.keys())][...];
  
    # Multiply by reference density to get flux units in kg/s
    flux_data = reference_brine_density * flux_data;
    
    # Integrate fluxes over time - cumtrapz returns the integrated data over time (instead of just at the final time)
    #cumulative_flux = scipy.integrate.cumtrapz(flux_data, x=times_s, axis=0, initial=0.0);
    # But we need righthand rectangle integration, not trapezoidal integration
    cumulative_flux = np.zeros_like(flux_data)
    dt = times_s[1:] - times_s[:-1]
    for i_point in xrange(num_flux_points):
      cumulative_flux[1:,i_point] = flux_data[1:,i_point] * dt
      for i_flux in xrange(len(dt)):
        cumulative_flux[i_flux+1,i_point] += cumulative_flux[i_flux,i_point]
    
    # resample at print times
    kbin = hf_bragflo_h5['/output_tables/simulation_statistics_table'][:,11][...];
    # indices_print_times = np.where(kbin==1.0);
    indices_print_times = np.nonzero(kbin);
    num_actual_printtimes = np.count_nonzero(kbin)
    #if (np.count_nonzero(kbin) != num_printtimes) :
    if (num_actual_printtimes > num_printtimes) :
      print( 'size(kbin eq 1) is different from num_printtimes' );
      print( 'size(indices_print_times) = ', repr(indices_print_times.size) );
      print( 'num_printtimes = ', repr(num_printtimes) );
      print( 'bailing out' );
      exit(-1);
    #elif (np.count_nonzero(kbin)) < num_printtimes):
      #should be okay to let this go
      
    # Write to memory data table
    table_big_data_fluxes[i_run,:num_actual_printtimes,:] = cumulative_flux[indices_print_times,:]
    table_big_data_inst_fluxes[i_run,:num_actual_printtimes,:] = flux_data[indices_print_times,:]
  
    # Close the hdf5 file
    hf_bragflo_h5.close();

  ###############################################################################
  ## Write phase: 
  ###############################################################################
  # Open output hdf5
  hf_analysis_summary =  h5py.File(directory_output+'/'+fname_analysis_summary, mode='w');

  # Reorder the big data to the write access pattern:
  # Reading access pattern: table[i_run, i_time, i_var, i_reg]
  # Writing access pattern: table[i_reg, i_var, i_run, i_time]

  table_big_data_write_order = table_big_data.transpose( (3,2,0,1) );

  # Create top group
  groupname_scenario = b'/Scenario ' + '{scen:0>1}'.format(scen=SCENARIO);
  grp_scenario = hf_analysis_summary.create_group(groupname_scenario);

  # Write time data
  grp_scenario.create_dataset(b'TIMES_SECONDS', data=snapshot_times_s);
  grp_scenario.create_dataset(b'TIMES_YEARS', data=snapshot_times_y);

  # Loop over regions
  for i_reg in range(num_regions) :
    # Create sub-group for the region
    groupname_region = list_region_names[i_reg];
    grp_region = grp_scenario.create_group(groupname_region);
  
    # Loop over variables
    for i_var in range(num_variables) :
      # Create dataset for each variable: variable[i_run, i_time]
      grp_region.create_dataset(list_variable_names[i_var], 
                                data=table_big_data_write_order[i_reg,i_var,:,:]);

  # Write fluxes

  # Reorder the big data to the write access pattern:
  # Reading access pattern: table[i_run, i_time, i_flux_point]
  # Writing access pattern: table[i_flux_point, i_run, i_time]
  table_big_data_fluxes_write_order = table_big_data_fluxes.transpose( (2,0,1) );
  table_big_data_inst_fluxes_write_order = table_big_data_inst_fluxes.transpose( (2,0,1) );

  # Create group to contain the flux points
  grp_cumu_fluxes = grp_scenario.create_group('CUMULATIVE_BRINE_MASS_FLUX');
  grp_inst_fluxes = grp_scenario.create_group('INSTANT_BRINE_MASS_FLUX');

  # Loop over flux points
  index = 0
  for key in sorted(flux_dict.keys()) :
    grp_cumu_fluxes.create_dataset(flux_dict[key],
                              data=table_big_data_fluxes_write_order[index,:,:]);
    grp_inst_fluxes.create_dataset(flux_dict[key],
                              data=table_big_data_inst_fluxes_write_order[index,:,:]);
    index += 1

  # Close the hdf5 file
  hf_analysis_summary.close();

###############################################################################
if __name__ == '__main__':
  '''get command line arguments and run
  '''
  parser = argparse.ArgumentParser(
    description='Post-process and collect bragflo output into summary h5 file.')
  parser.add_argument('-i','--input_dir',
                      help='path to directory containing bragflo output' +
                           ' [default = ./example_bragflo]',
                      default='./example_bragflo',
                      required=False,)
  parser.add_argument('-d','--output_dir',
                      help='path to directory where summary file will be saved' +
                           ' [default = ./example_bragflo]',
                      default='./example_bragflo',
                      required=False,)
  parser.add_argument('-s','--scenario',
                      help='int (1 thru 6) specifying scenario' +
                           ' [default = 1]',
                      default=1,
                      required=False,)
  parser.add_argument('-v','--vectors',
                      help='int (1 thru 300) specifying vectors' +
                           ' will process all vectors up to and including the number given' +
                           ' [default = 10]',
                      default=10,
                      required=False,)
  args = parser.parse_args()

  #check that dirs exist
  input_dir = os.path.join(os.getcwd(),args.input_dir)
  output_dir = os.path.join(os.getcwd(),args.output_dir)
  if not os.path.isdir(input_dir):
    print('ERROR: {} does not exist. Exiting.'.format(input_dir))
    sys.exit()
  if not os.path.isdir(output_dir):
    print('ERROR: {} does not exist. Create it and try again.'.format(output_dir))
    print('Exiting.')
    sys.exit()

  #collect
  collect_bragflo_h5(input_dir,output_dir,args.scenario,int(args.vectors))
          
