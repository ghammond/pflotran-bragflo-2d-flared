#! /bin/bash 

# Before running this script, you must:
# post process bf flux data using bf_outward_flux.py 

# choose bragflo results to compare against
this_dir=$(pwd)
if [[ $this_dir == *"tt04"* ]]; then
  bf_sum_dir=../bragflo_summary_tt_04 #not used for fluxes
  bf_dir=/home1/ergiamb/pfd_analysis/bragflo_output_tt_04
else
  bf_sum_dir=../bragflo_summary_orig_data #not used for fluxes
  bf_dir=/home1/ergiamb/pfd_analysis/bragflo_output_fluxes
fi
echo ''
echo 'pwd = '$this_dir
echo 'bf fluxes in '$bf_dir
echo ''
# confirm output directory
output_dir=horsetail_flux_dir
if test -d $output_dir; then
  read -p "$output_dir exists. Overwrite duplicate contents? [yes/no]: " stdin
  case $stdin in
    no | n) echo ""
      echo "If you want to keep $output_dir, move it and rerun script."
      echo "Exiting." 
      echo ""
      exit 1
      ;;
    yes | y) echo ""
      echo "Will overwrite duplicate contents. Starting..."
      echo ""
      ;;
    *) echo ""
      echo "Enter yes or no. Exiting."
      echo ""
      exit 1
      ;;
  esac
else
  mkdir $output_dir
fi
# call python script
for s in {1,2,3,4,5,6,}; do 
  for r in {1,2,3,}; do
    for reg in {INSTANT_BRINE_MASS_FLUX,}; do
      for var in {lwb_north_mb138,lwb_north_anhydrite,lwb_north_mb139,lwb_south_mb138,lwb_south_anhydrite,lwb_south_mb139,borehole_culebra,shaft_culebra,}; do
        python horsetail_fluxes.py -f $bf_sum_dir/pfd_analysis_bragflo_summary_scenario$s.h5 \
                            -b $bf_dir \
                            -p s$s \
                            -v all \
                            -s $s \
                            -r $r \
                            -e $reg \
                            -a $var \
                            -d $output_dir \
                            -l compare_only \
                            -c rainbow
      done
    done
  done
done
