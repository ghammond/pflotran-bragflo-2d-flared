#!/usr/bin/python
import sys
import os
import shutil
import argparse
import numpy as np
print('Using numpy version {}'.format(np.__version__))
import scipy, scipy.special
import h5py
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import pandas as pd

from matplotlib.backends.backend_pdf import PdfPages

plt.rcParams['figure.dpi'] = 300;
plt.rcParams['xtick.labelsize'] = 9;
plt.rcParams['ytick.labelsize'] = 9;
plt.rcParams['axes.labelsize'] = 10;
plt.rcParams['axes.titlesize'] = 10;
plt.rcParams['legend.fontsize'] = 10;
  
###############################################################################
########################################################################

def ave_v_time(grp_scenario,r,s,v,region_list,fig_dir,pdf_figures):
  '''Plot volume_averaged outputs v. time for requested 
     replicate, scenario, vector. Also plot difference.
     r = replicate (int)
     s = scenario (int)
     v = vector (int)
     region_list = [list of strings]
  '''

  # load time once
  time_yr_bragflo = grp_scenario['TIMES_YEARS'][...];
  # get region descriptions, pflotran regions and variables
  name_dict = get_long_names() 
  pf_reg_dict = get_pf_regions()
  pf_var_dict = get_pf_variables()
  denom_dict = get_min_denom()
  # calculate run number
  i_run = (v-1) + 100*(r-1);

  for reg in region_list:
    print('in ave_v_time regions loop: {}'.format(reg))
    # Load BRAGFLO data
    ###########################################################
    # organized as [time, run], where run is the combined vector,replicate
    # number (e.g. 0-299).
    # [...] tells h5py to return the underlying data, not reference to h5 object
    pressure_bragflo = grp_scenario[reg]['PRESBRIN'][i_run,:][...];
    saturation_bragflo = grp_scenario[reg]['SATBRINE'][i_run,:][...];
    porosity_bragflo = grp_scenario[reg]['POROS'][i_run,:][...];
      
    # Load PFLOTRAN data
    ###########################################################
    #this will fail if file isn't there or incomplete
    pf_df = make_pflotran_df(make_pflotran_filename(r,s,v))
    pressure_pflotran = pf_df['{} {}'.format(pf_var_dict['PRESBRIN'],pf_reg_dict[reg])]
    saturation_pflotran = pf_df['{} {}'.format(pf_var_dict['SATBRINE'],pf_reg_dict[reg])]
    porosity_pflotran = pf_df['{} {}'.format(pf_var_dict['POROS'],pf_reg_dict[reg])]
    time_yr_pflotran = pf_df.index
    
    # Plot variable and error vs. time
    ###########################################################
    # Try to estimate sane y limits for saturation
    sat_print_max = np.max(saturation_bragflo); 
    ylim_sat_max = None;  
    if (sat_print_max > 0.90): ylim_sat_max = 1.0;
      
    # Plot pressures and saturations
    plot_2x2_variable_error_vs_time( 
      time_var1_exp=time_yr_pflotran, var1_exp=pressure_pflotran, 
      time_var1_ref=time_yr_bragflo, var1_ref=pressure_bragflo, 
      time_var2_exp=time_yr_pflotran, var2_exp=saturation_pflotran, 
      time_var2_ref=time_yr_bragflo, var2_ref=saturation_bragflo, 
      var1_min_significant_value=denom_dict['PRESBRIN'], 
      var2_min_significant_value=denom_dict['SATBRINE'],
      label_exp='PFLOTRAN', label_ref='BRAGFLO',
      #change naming convention
      fig_save_name = os.path.join(fig_dir,
                      'PFD_r{}_s{}_v{:0>3}_{}_pres_sat'.format(r,s,v,reg)),
      pdf_output_handle = pdf_figures,
      fig_title = '{}: Brine Pressure and Saturation, r{}_s{}_v{:0>3}'.format(reg,r,s,v),
      fig_caption = 'Average brine pressure and saturation in {}, r{}_s{}_v{:0>3}'.format(name_dict[reg],r,s,v),
      subplot_titles  = [ ['Brine Pressure', 'Difference in Brine Pressure'],
                          ['Brine Saturation', 'Difference in Brine Saturation'] ],
      axes_xlabels = [ ['Time [yr]', 'Time [yr]' ], ['Time [yr]', 'Time [yr]' ] ],
      axes_ylabels = [ ['Brine Pressure [Pa]', 'Percent Difference [%]'],
                       ['Brine Saturation [-]', 'Percent Difference [%]'] ],
      axes_xlimits = [ [(0.0, 1.0e4), (0.0, 1.0e4)],
                       [(0.0, 1.0e4), (0.0, 1.0e4)] ],
      axes_ylimits = [ [(None, None), (None, None)],
                       [(0.0, ylim_sat_max), (None, None)] ],
      axes_xtick_numformat = [ ['int', 'int'],
                               ['int', 'int'] ],
      axes_ytick_numformat = [ ['exp', 'flt'],
                               ['flt_or_exp', 'flt_or_exp'] ]
      )
  return

def rmsd(grp_scenario,scenario,regions,times,fig_dir,pdf_figures):
  '''plot root mean square difference for all replicates/vectors in
     given scenario (bar plot). Also return a table in text file.
     Use requested comparison times and requested regions.
     Assumes that runs are consecutive vectors starting from 0 (up to 299)
     in bf summary h5 file and r1,v1 through r3,v100 in pf .ave files
  '''

  #synonym
  s = scenario
  # load bf all time once
  time_yr_bragflo = grp_scenario['TIMES_YEARS'][...];
  # get region descriptions, pflotran regions and variables
  name_dict = get_long_names() 
  pf_reg_dict = get_pf_regions()
  pf_var_dict = get_pf_variables()
  denom_dict = get_min_denom()
  # find number of runs in bf file
  nruns = grp_scenario['WAS_AREA']['PRESBRIN'].shape[0] #number of rows
  if nruns > 300:
    print('ERROR: Number of runs ({}) > 300. Exiting.')
    sys.exit()

  # Define the time-array indices of the times we want to compare at:
  ###########################################################
  ntimes = len(times)
  times = np.array(times,dtype=np.float)
#  ix = np.isin(time_yr_bragflo,times) #array of booleans #need numpy1.13 or higher
#  idx_bf_times = np.where(ix) #indices where ix evaluates to True
  idx_bf_times = []
  for i in xrange(len(time_yr_bragflo)):
    if any(round(t) == round(time_yr_bragflo[i]) for t in times):
      idx_bf_times.append(i)
  if len(idx_bf_times) != ntimes:
    print('ERROR: len(idx_bf_times) {} != ntimes {}. Exiting.'.format(len(idx_bf_times),ntimes))
    sys.exit()
  idx_bf_times = np.array(idx_bf_times)
    
  for reg in regions:
    pres_rmsd = [] #will become array of rmsd for each run in this region
    sat_rmsd = [] #will become array of rmsd for each run in this region
    for run in xrange(nruns):
      #calc corresponding pf replicate and vector
      if run < 100:
        r = 1
        v = run + 1
      elif run < 200:
        r = 2
        v = run - 100 + 1
      elif run < 300:
        r = 3
        v = run - 200 + 1
      print('In rmsd(), region {}, run {}: replicate r = {}; vector v = {}'.format(reg,run,r,v))

      # Load BRAGFLO data
      ###########################################################
      pressure_bragflo = grp_scenario[reg]['PRESBRIN'][run,idx_bf_times][...];
      saturation_bragflo = grp_scenario[reg]['SATBRINE'][run,idx_bf_times][...];
      porosity_bragflo = grp_scenario[reg]['POROS'][run,idx_bf_times][...];
      
      # Load PFLOTRAN data
      ###########################################################
      #this will fail if file isn't there or incomplete
      pf_df = make_pflotran_df(make_pflotran_filename(r,s,v))
      pressure_pflotran = pf_df.loc[times,'{} {}'.format(pf_var_dict['PRESBRIN'],pf_reg_dict[reg])]
      saturation_pflotran = pf_df.loc[times,'{} {}'.format(pf_var_dict['SATBRINE'],pf_reg_dict[reg])]
      porosity_pflotran = pf_df.loc[times,'{} {}'.format(pf_var_dict['POROS'],pf_reg_dict[reg])]
    
    # Calculate normalized error:
    # RMS Difference = L_2 / sqrt(n_runs)
    # note to self - biased_error() reverses sign compared to difference.py,but doesn't matter when abs()
      err_pressure = np.linalg.norm( np.abs(biased_error(
                     pressure_bragflo, pressure_pflotran, denom_dict['PRESBRIN'])),
                     ord=2, axis=0, keepdims=False ) / np.sqrt(ntimes);
      #err_saturation = np.linalg.norm( np.abs(biased_error(
      #               saturation_bragflo, saturation_pflotran, denom_dict['SATBRINE'])),
      #               ord=2, axis=0, keepdims=False ) / np.sqrt(ntimes);
      err_saturation = np.linalg.norm( np.abs(saturation_bragflo-saturation_pflotran),
                     ord=2, axis=0, keepdims=False ) / np.sqrt(ntimes);
      pres_rmsd.append(err_pressure)
      sat_rmsd.append(err_saturation)
    #end loop through runs
    pres_rmsd = np.array(pres_rmsd)
    sat_rmsd = np.array(sat_rmsd)
    
    # Plot error vs run
    ###########################################################
  
    run_numbers = np.arange(1,nruns+1);
    #print(run_numbers)
    # Pressure
    plot_1x1_error_vs_run( 
    run_numbers, pres_rmsd,
    fig_save_name = os.path.join(fig_dir,'PFD_s{}_{}_PRESBRIN_rmsd'.format(s,reg)), 
    pdf_output_handle = pdf_figures,
    fig_title = 'RMSD of Pressure in {}, Scenario {}'.format(name_dict[reg],s),
    fig_caption = 'Root-Mean-Squared-Deviation of Average Pressure in, Scenario {}'.format(name_dict[reg],s),
    subplot_titles  = None,
    axes_xlabels = 'Run Number',
    axes_ylabels = 'Percent Difference [%]',
    axes_xlimits = (0, nruns+1),
    axes_ylimits = (None, None),
    axes_xtick_numformat = 'int',
    axes_ytick_numformat = 'flt',
    line = np.array([[0,nruns+1],[1.,1.]]), #put line at 1%
    );
      
    # Saturation
    plot_1x1_error_vs_run( 
    run_numbers, sat_rmsd,
    fig_save_name = os.path.join(fig_dir,'PFD_s{}_{}_SATBRINE_rmsd'.format(s,reg)), 
    pdf_output_handle = pdf_figures,
    fig_title = 'RMSD of SATBRIN in {}, Scenario {}'.format(name_dict[reg],s),
    fig_caption = 'Root-Mean-Squared-Deviation of Average Saturation in, Scenario {}'.format(name_dict[reg],s),
    subplot_titles  = None,
    axes_xlabels = 'Run Number',
    axes_ylabels = 'Absolute Difference [-]',
    axes_xlimits = (0, nruns+1),
    axes_ylimits = (0, 1), #(None,None), if use % diff
    axes_xtick_numformat = 'int',
    axes_ytick_numformat = 'flt'
    );
      
  return
    
def maxabs(grp_scenario,scenario,regions,times,fig_dir,pdf_figures):
  '''plot max absolute value of difference for all replicates/vectors in
     given scenario (bar plot). Also return a table in text file.
     Use requested comparison times and requested regions.
     Assumes that runs are consecutive vectors starting from 0 (up to 299)
     in bf summary h5 file and r1,v1 through r3,v100 in pf .ave files
  '''

  #synonym
  s = scenario
  # load bf all time once
  time_yr_bragflo = grp_scenario['TIMES_YEARS'][...];
  # get region descriptions, pflotran regions and variables
  name_dict = get_long_names() 
  pf_reg_dict = get_pf_regions()
  pf_var_dict = get_pf_variables()
  denom_dict = get_min_denom()
  # find number of runs in bf file
  nruns = grp_scenario['WAS_AREA']['PRESBRIN'].shape[0] #number of rows
  if nruns > 300:
    print('ERROR: Number of runs ({}) > 300. Exiting.')
    sys.exit()

  # Define the time-array indices of the times we want to compare at:
  ###########################################################
  ntimes = len(times)
  times = np.array(times,dtype=np.float)
#  ix = np.isin(time_yr_bragflo,times) #array of booleans #need numpy1.13 or higher
#  idx_bf_times = np.where(ix) #indices where ix evaluates to True
  idx_bf_times = []
  for i in xrange(len(time_yr_bragflo)):
    if any(round(t) == round(time_yr_bragflo[i]) for t in times):
      idx_bf_times.append(i)
  if len(idx_bf_times) != ntimes:
    print('ERROR: len(idx_bf_times) {} != ntimes {}. Exiting.'.format(len(idx_bf_times),ntimes))
    sys.exit()
  idx_bf_times = np.array(idx_bf_times)
    
  for reg in regions:
    pres_maxd = [] #will become array of rmsd for each run in this region
    sat_maxd = [] #will become array of rmsd for each run in this region
    time_pres_maxd = [] #array of time of pres_maxd for each run in this region
    time_sat_maxd = [] #array of time of sat_maxd for each run in this region
    for run in xrange(nruns):
      #calc corresponding pf replicate and vector
      r = run%100 + 1
      v = run - (run%100)*100 + 1
      if run < 100:
        r = 1
        v = run + 1
      elif run < 200:
        r = 2
        v = run - 100 + 1
      elif run < 300:
        r = 3
        v = run - 200 + 1
      print('In maxabs(), region {}, run {}: replicate r = {}; vector v = {}'.format(reg,run,r,v))

      # Load BRAGFLO data
      ###########################################################
      pressure_bragflo = grp_scenario[reg]['PRESBRIN'][run,idx_bf_times][...];
      saturation_bragflo = grp_scenario[reg]['SATBRINE'][run,idx_bf_times][...];
      porosity_bragflo = grp_scenario[reg]['POROS'][run,idx_bf_times][...];
      
      # Load PFLOTRAN data
      ###########################################################
      #this will fail if file isn't there or incomplete
      pf_df = make_pflotran_df(make_pflotran_filename(r,s,v))
      pressure_pflotran = pf_df.loc[times,'{} {}'.format(pf_var_dict['PRESBRIN'],pf_reg_dict[reg])]
      saturation_pflotran = pf_df.loc[times,'{} {}'.format(pf_var_dict['SATBRINE'],pf_reg_dict[reg])]
      porosity_pflotran = pf_df.loc[times,'{} {}'.format(pf_var_dict['POROS'],pf_reg_dict[reg])]
    
    # Find maximum absolute value of difference, and time of max diff
    # Percent diff for pressure
    # Absolute diff for saturation
      err_pressure = np.abs(biased_error(
                     pressure_bragflo, pressure_pflotran, denom_dict['PRESBRIN']))
      err_saturation = np.abs(saturation_bragflo-saturation_pflotran)

      max_err_p = np.max(err_pressure)
      max_err_s = np.max(err_saturation)
      pres_maxd.append(max_err_p)
      sat_maxd.append(max_err_s)

      i_p = np.argmax(max_err_p)
      i_s = np.argmax(max_err_s)
      time_pres_maxd.append(times[i_p])
      time_sat_maxd.append(times[i_s])

    #end loop through runs
    pres_maxd = np.array(pres_maxd)
    sat_maxd = np.array(sat_maxd)
    time_pres_maxd = np.array(time_pres_maxd)
    time_sat_maxd = np.array(time_sat_maxd)

    #print tables of max differences and times of occurrence to text file
    iarray = np.arange(1,len(pres_maxd)+1,dtype=np.int)
    darray = np.stack((iarray,time_pres_maxd,pres_maxd),axis=-1)
    np.savetxt(os.path.join(fig_dir,'PFD_s{}_{}_PRESBRIN_maxd.txt'.format(s,reg)),
                            darray,
                            fmt='%10d %12f %12f',
                            header='%10s %12s %12s' %('run_id','time_[y]','max%diff'))
    darray = np.stack((iarray,time_sat_maxd,sat_maxd),axis=-1)
    np.savetxt(os.path.join(fig_dir,'PFD_s{}_{}_SATBRIN_maxd.txt'.format(s,reg)),
                            darray,
                            fmt='%10d %12f %12f',
                            header='%10s %12s %12s' %('run_id','time_[y]','max_diff_[-]'))
    
    # Plot error vs run
    ###########################################################
  
    run_numbers = np.arange(1,nruns+1);
    #print(run_numbers)
    # Pressure
    plot_1x1_error_vs_run( 
    run_numbers, pres_maxd,
    fig_save_name = os.path.join(fig_dir,'PFD_s{}_{}_PRESBRIN_maxd'.format(s,reg)), 
    pdf_output_handle = pdf_figures,
    fig_title = 'Max diff of Pressure in {}, Scenario {}'.format(name_dict[reg],s),
    fig_caption = 'Maximum Difference of Average Pressure in, Scenario {}'.format(name_dict[reg],s),
    subplot_titles  = None,
    axes_xlabels = 'Run Number',
    axes_ylabels = 'Percent Difference [%]',
    axes_xlimits = (0, nruns+1),
    axes_ylimits = (None, None),
    axes_xtick_numformat = 'int',
    axes_ytick_numformat = 'flt',
    line = np.array([[0,nruns+1],[1.,1.]]), #put line at 1%
    );
      
    # Saturation
    plot_1x1_error_vs_run( 
    run_numbers, sat_maxd,
    fig_save_name = os.path.join(fig_dir,'PFD_s{}_{}_SATBRINE_maxd'.format(s,reg)), 
    pdf_output_handle = pdf_figures,
    fig_title = 'Max diff of SATBRIN in {}, Scenario {}'.format(name_dict[reg],s),
    fig_caption = 'Maximum Difference of Average Saturation in, Scenario {}'.format(name_dict[reg],s),
    subplot_titles  = None,
    axes_xlabels = 'Run Number',
    axes_ylabels = 'Absolute Difference [-]',
    axes_xlimits = (0, nruns+1),
    axes_ylimits = (0.,1.), #(None, None),
    axes_xtick_numformat = 'int',
    axes_ytick_numformat = 'flt'
    );
      
  return
    
###############################################################################
# Overload some stuff to make scientific notation look reasonable on plots
# It allows decimals in scientific notation, instead of just integers
# https://stackoverflow.com/questions/42142144/displaying-first-decimal-digit-in-scientific-notation-in-matplotlib
from matplotlib.ticker import ScalarFormatter
class ScalarFormatterForceFormat(ScalarFormatter):
    def _set_format(self,vmin,vmax):  # Override function that finds format to use.
        self.format = "%5.2f"  # Give format here
###############################################################################

###############################################################################
def biased_error(data_reference, data_experimental, minimum_significant_value) :
  """ Calculates the percent error, with the denominator biased to some minimum significant value """
  return 100.0* (np.squeeze(data_experimental)-np.squeeze(data_reference)) / np.maximum(np.abs(np.squeeze(data_reference)), np.abs(minimum_significant_value));
###############################################################################

###############################################################################
def biased_error_interp_time(time_reference, data_reference, time_experiemtnal, data_experimental, minimum_significant_value) :
  """ Interpolates the experimental values at the reference time points, then 
      Calculates the percent error, with the denominator biased to some minimum significant value """
  data_experimental_interpolated = np.interp(time_reference, time_experiemtnal, data_experimental);
  return 100.0* (data_experimental_interpolated-data_reference) / np.maximum(np.abs(data_reference), np.abs(minimum_significant_value));
###############################################################################

###############################################################################
def plot_1x2_variable_error_vs_time( 
    time_var1_exp, var1_exp, 
    time_var1_ref, var1_ref, 
    # time_var1_err, var1_err,
    # time_var2_err, var2_err,
    var1_min_significant_value, 
    label_exp='PFLOTRAN', label_ref='BRAGFLO', 
    fig_save_name = 'fig_1x2',
    fig_title = 'Figure Title', 
    fig_caption = None,
    pdf_output_handle = None,
    subplot_titles  = [ ['Brine Pressure', 'Difference in Brine Pressure'], ],
    axes_xlabels = [ ['Time [yr]', 'Time [yr]' ], ],
    axes_ylabels = [ ['Brine Pressure [Pa]', 'Percent Difference [%]'], ],
    axes_xlimits = [ [(0.0, None), (0.0, None)], ],
    axes_ylimits = [ [(None, None), (None, None)], ],
    axes_xtick_numformat = [ ['int', 'int'], ],
    axes_ytick_numformat = [ ['exp', 'flt'], ]
    ) :
  """ Creates a 1x2 subplot """
  
  # Create figure and axes
  #  matplotlib.pyplot.subplots(nrows=1, ncols=1, sharex=False, sharey=False, squeeze=True, subplot_kw=None, gridspec_kw=None, **fig_kw)
  fig101, axes101 = plt.subplots(nrows=1, ncols=2, sharex=False, sharey=False, squeeze=False, figsize=(6.5, 2.1), dpi=300);
  
  # Set overall figure title
  # Suppress for the 1x2 case
  fig101.suptitle(fig_title, fontsize=11);
  
  # Top-left subplot
  ###########################################################
  row=0; col=0;
  
  # Plot variable values
  axes101[row,col].plot(time_var1_ref, var1_ref, 'b-',  alpha=0.7, label=label_ref);
  axes101[row,col].plot(time_var1_exp, var1_exp, 'r--', alpha=0.7, label=label_exp);
  
   # Top-right subplot
  ###########################################################
  row=0; col=1;
  
  # Plot error
  # axes101[row,col].plot(time_var1_err, var1_err, 'r--' );
  #axes101[row,col].plot(time_var1, biased_error(var1_ref, var1_exp, var1_min_significant_value), 'r--' );
  axes101[row,col].plot(time_var1_ref, biased_error_interp_time(time_var1_ref, var1_ref, time_var1_exp, var1_exp, var1_min_significant_value), 'r--' );
  
  
   # Apply formatting options
  ###########################################################
  for col in [0,1] :
    for row in [0,0] :
      
      # Format x-tick labels
      if (axes_xtick_numformat[row][col] == 'int') : 
        axes101[row,col].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
      elif (axes_xtick_numformat[row][col] == 'flt') : 
        axes101[row,col].xaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
      elif (axes_xtick_numformat[row][col] == 'exp') : 
        # do some magic to format the exponent in scientific notation
        xfrmtr=ScalarFormatterForceFormat(); xfrmtr.set_powerlimits((-1,1));  xfrmtr.set_scientific(True);
        axes101[row,col].xaxis.set_major_formatter(xfrmtr);
      
      # Format y-tick labels
      if (axes_ytick_numformat[row][col] == 'flt_or_exp') : 
        # Check ylim to see if the y scale should be displayed in floating-point (with 2 decimals) or scientific notation
        # Ensure that ylimits on the error plots aren't too small or duplicates
        ymin, ymax = axes101[row,col].get_ylim();
        if (abs(ymax) <= 0.02 and abs(ymin) <= 0.02 ):
          axes_ytick_numformat[row][col] = 'exp';
        else :
          axes_ytick_numformat[row][col] = 'flt';
      
      # Format y-tick labels
      if (axes_ytick_numformat[row][col] == 'int') : 
        axes101[row,col].yaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
      elif (axes_ytick_numformat[row][col] == 'flt') : 
        axes101[row,col].yaxis.set_major_formatter(tck.FormatStrFormatter('%5.2f'));
      elif (axes_ytick_numformat[row][col] == 'exp') : 
        # do some magic to format the exponent in scientific notation
        yfrmtr=ScalarFormatterForceFormat(); yfrmtr.set_powerlimits((-1,1));  yfrmtr.set_scientific(True);
        axes101[row,col].yaxis.set_major_formatter(yfrmtr);
      
      # Set axes limits
      axes101[row,col].set_xlim( axes_xlimits[row][col] );
      axes101[row,col].set_ylim( axes_ylimits[row][col] );
      
      # # Ensure that ylimits on the error plots aren't too small or duplicates
      # ymin, ymax = axes101[row,col].get_ylim();
      # axes101[row,col].set_ylim( np.sign(ymin)*np.maximum(np.abs(ymin),0.01), np.sign(ymax)*np.maximum(np.abs(ymax),0.01));
      # ymin, ymax = axes101[row,col].get_ylim();
      # if(ymin==-0.01 and ymax==0.01): axes101[row,col].locator_params(axis='y', nbins=3);
      
      # Set labels and legend
      axes101[row,col].set_xlabel( axes_xlabels[row][col], color='k' );
      axes101[row,col].set_ylabel( axes_ylabels[row][col], color='k');
      axes101[row,col].legend(loc='best');
      
      # Set title
      axes101[row,col].set_title( subplot_titles[row][col] );
  
  # Final stuff
  ###########################################################
  # Adjust layout
  fig101.tight_layout();
  fig101.subplots_adjust(top=0.82);
  
  # # Display plot on screen - this locks and waits for the user to close the window
  # plt.show();
  
  # Append figure to a pdf
  if (pdf_output_handle) :
    pdf_output_handle.savefig(fig101);
  
  # Save figure as .png file
  if (fig_save_name) :
    # fig101.savefig(fig_output_dir+'/'+deckname+'_'+fig_subname+'.png', dpi=300);
    fig101.savefig(fig_save_name + '.png',) # dpi='figure');
  
  # # toss stuff
  plt.close(fig101);
  # del(fig101, axes101);
  
###############################################################################


###############################################################################
def plot_2x2_variable_error_vs_time( 
    time_var1_exp, var1_exp, 
    time_var1_ref, var1_ref, 
    time_var2_exp, var2_exp, 
    time_var2_ref, var2_ref, 
    # time_var1_err, var1_err,
    # time_var2_err, var2_err,
    var1_min_significant_value, var2_min_significant_value,
    label_exp='PFLOTRAN', label_ref='BRAGFLO', 
    fig_save_name = 'fig_2x2',
    fig_title = 'Figure Title', 
    fig_caption = None,
    pdf_output_handle = None,
    subplot_titles  = [ ['Brine Pressure', 'Difference in Brine Pressure'], ['Brine Saturation', 'Difference in Brine Saturation'] ],
    axes_xlabels = [ ['Time [yr]', 'Time [yr]' ], ['Time [yr]', 'Time [yr]' ] ],
    axes_ylabels = [ ['Brine Pressure [Pa]', 'Percent Difference [%]'], ['Brine Saturation [-]', 'Percent Difference [%]'] ],
    axes_xlimits = [ [(0.0, None), (0.0, None)], [(0.0, None), (0.0, None)] ],
    axes_ylimits = [ [(None, None), (None, None)], [(0.0, None), (None, None)] ],
    axes_xtick_numformat = [ ['int', 'int'], ['int', 'int'] ],
    axes_ytick_numformat = [ ['exp', 'flt'], ['flt', 'flt'] ]
    ) :
  """ Creates a 2x2 subplot """
  
  # Create figure and axes
  #  matplotlib.pyplot.subplots(nrows=1, ncols=1, sharex=False, sharey=False, squeeze=True, subplot_kw=None, gridspec_kw=None, **fig_kw)
  fig101, axes101 = plt.subplots(nrows=2, ncols=2, sharex=False, sharey=False, squeeze=False, figsize=(6.5, 4.0), dpi=300);
  
  # Set overall figure title
  fig101.suptitle(fig_title, fontsize=11);
  
  # Top-left subplot
  ###########################################################
  row=0; col=0;
  
  # Plot variable values
  axes101[row,col].plot(time_var1_ref, var1_ref, 'b-',  alpha=0.7, label=label_ref);
  axes101[row,col].plot(time_var1_exp, var1_exp, 'r--', alpha=0.7, label=label_exp);
  
   # Top-right subplot
  ###########################################################
  row=0; col=1;
  
  # Plot error
  # axes101[row,col].plot(time_var1_err, var1_err, 'r--' );
  #axes101[row,col].plot(time_var1, biased_error(var1_ref, var1_exp, var1_min_significant_value), 'r--' );
  axes101[row,col].plot(time_var1_ref, biased_error_interp_time(time_var1_ref, var1_ref, time_var1_exp, var1_exp, var1_min_significant_value), 'r--' );
  
   # Bottom-left subplot
  ###########################################################
  row=1; col=0;
  
  # Plot variable values
  axes101[row,col].plot(time_var2_ref, var2_ref, 'b-',  alpha=0.7, label=label_ref);
  axes101[row,col].plot(time_var2_exp, var2_exp, 'r--', alpha=0.7, label=label_exp);
  
   # Bottom-right subplot
  ###########################################################
  row=1; col=1;
  
  # Plot error
  # axes101[row,col].plot(time_var2_err, var2_err, 'r--' );
  #axes101[row,col].plot(time_var2, biased_error(var2_ref, var2_exp, var2_min_significant_value), 'r--' );
  axes101[row,col].plot(time_var2_ref, biased_error_interp_time(time_var2_ref, var2_ref, time_var2_exp, var2_exp, var2_min_significant_value), 'r--' );
  
  
   # Apply formatting options
  ###########################################################
  for col in [0,1] :
    for row in [0,1] :
      
      # Format x-tick labels
      if (axes_xtick_numformat[row][col] == 'int') : 
        axes101[row,col].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
      elif (axes_xtick_numformat[row][col] == 'flt') : 
        axes101[row,col].xaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
      elif (axes_xtick_numformat[row][col] == 'exp') : 
        # do some magic to format the exponent in scientific notation
        xfrmtr=ScalarFormatterForceFormat(); xfrmtr.set_powerlimits((-1,1));  xfrmtr.set_scientific(True);
        axes101[row,col].xaxis.set_major_formatter(xfrmtr);
      
      # Format y-tick labels
      if (axes_ytick_numformat[row][col] == 'flt_or_exp') : 
        # Check ylim to see if the y scale should be displayed in floating-point (with 2 decimals) or scientific notation
        # Ensure that ylimits on the error plots aren't too small or duplicates
        ymin, ymax = axes101[row,col].get_ylim();
        if (abs(ymax) <= 0.02 and abs(ymin) <= 0.02 ):
          axes_ytick_numformat[row][col] = 'exp';
        else :
          axes_ytick_numformat[row][col] = 'flt';
      
      # Format y-tick labels
      if (axes_ytick_numformat[row][col] == 'int') : 
        axes101[row,col].yaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
      elif (axes_ytick_numformat[row][col] == 'flt') : 
        axes101[row,col].yaxis.set_major_formatter(tck.FormatStrFormatter('%5.2f'));
      elif (axes_ytick_numformat[row][col] == 'exp') : 
        # do some magic to format the exponent in scientific notation
        yfrmtr=ScalarFormatterForceFormat(); yfrmtr.set_powerlimits((-1,1));  yfrmtr.set_scientific(True);
        axes101[row,col].yaxis.set_major_formatter(yfrmtr);
      
      # Set axes limits
      axes101[row,col].set_xlim( axes_xlimits[row][col] );
      axes101[row,col].set_ylim( axes_ylimits[row][col] );
      
      # # Ensure that ylimits on the error plots aren't too small or duplicates
      # ymin, ymax = axes101[row,col].get_ylim();
      # axes101[row,col].set_ylim( np.sign(ymin)*np.maximum(np.abs(ymin),0.01), np.sign(ymax)*np.maximum(np.abs(ymax),0.01));
      # ymin, ymax = axes101[row,col].get_ylim();
      # if(ymin==-0.01 and ymax==0.01): axes101[row,col].locator_params(axis='y', nbins=3);
      
      # Set labels and legend
      axes101[row,col].set_xlabel( axes_xlabels[row][col], color='k' );
      axes101[row,col].set_ylabel( axes_ylabels[row][col], color='k');
      axes101[row,col].legend(loc='best');
      
      # Set title
      axes101[row,col].set_title( subplot_titles[row][col] );
  
  # Final stuff
  ###########################################################
  # Adjust layout
  fig101.tight_layout();
  fig101.subplots_adjust(top=0.90);
  
  # # Display plot on screen - this locks and waits for the user to close the window
  # plt.show();
  
  # Append figure to a pdf
  if (pdf_output_handle) :
    pdf_output_handle.savefig(fig101);
  
  # Save figure as .png file
  if (fig_save_name) :
    # fig101.savefig(fig_output_dir+'/'+deckname+'_'+fig_subname+'.png', dpi=300);
    fig101.savefig(fig_save_name + '.png',) # dpi='figure');
  
  # return fig101, axes101;
  
  # # toss stuff
  plt.close(fig101);
  # del(fig101, axes101);
  
###############################################################################


###############################################################################
def plot_1x1_error_vs_run( 
    run_numbers, error_values, 
    label1='L_2', label2='L_inf', 
    fig_save_name = 'fig_2x2',
    fig_title = 'Figure Title', 
    fig_caption = None,
    pdf_output_handle = None,
    subplot_titles  = None,
    axes_xlabels = 'Run Number',
    axes_ylabels = 'Percent Difference [%]',
    axes_xlimits = (None, None),
    axes_ylimits = (None, None),
    axes_xtick_numformat = 'int',
    axes_ytick_numformat = 'flt',
    line = np.array([[-99,-99],[-99,-99]]),
    ) :
  """ Creates a bar plot """
  
  # Create figure and axes
  #  matplotlib.pyplot.subplots(nrows=1, ncols=1, sharex=False, sharey=False, squeeze=True, subplot_kw=None, gridspec_kw=None, **fig_kw)
  fig101, axes101 = plt.subplots(squeeze=True, figsize=(6.5, 4.0), dpi=300);
  
  # Set overall figure title
  fig101.suptitle(fig_title, fontsize=11);
  
  # Plot error
  ###########################################################
  # bar(x, height, *, align='center', **kwargs)
  bars = axes101.bar(run_numbers, error_values, align='center',color='r');
  
  
   # Apply formatting options
  ###########################################################
  # Format x-tick labels
  #if (axes_xtick_numformat == 'int') : 
  axes101.xaxis.set_major_formatter(tck.FormatStrFormatter('%03i'));
  
  
  # Format y-tick labels
  if (axes_ytick_numformat == 'flt_or_exp') : 
    # Check ylim to see if the y scale should be displayed in floating-point (with 2 decimals) or scientific notation
    # Ensure that ylimits on the error plots aren't too small or duplicates
    ymin, ymax = axes101.get_ylim();
    if (abs(ymax) <= 0.02 and abs(ymin) <= 0.02 ):
      axes_ytick_numformat = 'exp';
    else :
      axes_ytick_numformat = 'flt';
  
  # Format y-tick labels
  if (axes_ytick_numformat == 'int') : 
    axes101.yaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
  elif (axes_ytick_numformat == 'flt') : 
    axes101.yaxis.set_major_formatter(tck.FormatStrFormatter('%5.2f'));
  elif (axes_ytick_numformat == 'exp') : 
    # do some magic to format the exponent in scientific notation
    yfrmtr=ScalarFormatterForceFormat(); yfrmtr.set_powerlimits((-1,1));  yfrmtr.set_scientific(True);
    axes101.yaxis.set_major_formatter(yfrmtr);
  
  # Set axes limits
  axes101.set_xlim( axes_xlimits );
  axes101.set_ylim( axes_ylimits );
  
  # # Ensure that ylimits on the error plots aren't too small or duplicates
  # ymin, ymax = axes101.get_ylim();
  # axes101.set_ylim( np.sign(ymin)*np.maximum(np.abs(ymin),0.01), np.sign(ymax)*np.maximum(np.abs(ymax),0.01));
  # ymin, ymax = axes101.get_ylim();
  # if(ymin==-0.01 and ymax==0.01): axes101.locator_params(axis='y', nbins=3);
  
  # Draw line if one was given.
  # this isn't working!!!
  x,y = line
  mpl.lines.Line2D(x,y,lw=5.,color='gray',linestyle='--')

  # Set labels and legend
  axes101.set_xlabel( axes_xlabels, color='k' );
  axes101.set_ylabel( axes_ylabels, color='k');
  axes101.legend(loc='best');
  
  # Set title
  #axes101.set_title( subplot_titles );
  
  # Final stuff
  ###########################################################
  # Adjust layout
  fig101.tight_layout();
  fig101.subplots_adjust(top=0.90);
  
  # # Display plot on screen - this locks and waits for the user to close the window
  # plt.show();
  
  # Append figure to a pdf
  if (pdf_output_handle) :
    pdf_output_handle.savefig(fig101);
  
  # Save figure as .png file
  if (fig_save_name) :
    # fig101.savefig(fig_output_dir+'/'+deckname+'_'+fig_subname+'.png', dpi=300);
    fig101.savefig(fig_save_name + '.png',) # dpi='figure');
  
  # return fig101, axes101;
  
  # # toss stuff
  plt.close(fig101);
  # del(fig101, axes101);
  
###############################################################################


###############################################################################
def make_pflotran_filename(r,s,v):
  '''Put together pflotran filename from replicate, scenario, vector numbers
     Specific to directory structure of pflotran-bragflo-2d-flared/
  '''
  filename = './s{1}/pf_PFD_r{0}_s{1}_v{2:0>3}-obs-0.ave'.format(r,s,v)
  return filename
  
def make_pflotran_df(filename):
  '''Read pflotran <output>.ave into a pandas DataFrame
  '''
  with open (filename,'r') as f:
    header = f.readline().split(',')
    for i in xrange(len(header)):
      header[i] = header[i].strip('"\n')

  #pf_df = pd.read_table(filename,delim_whitespace=True,index_col=0,skiprows=1,)
  pf_df = pd.read_table(filename,delim_whitespace=True,
              index_col=0,skiprows=1,names=header[1:])
  return pf_df

def get_pf_regions():
  '''Return dictionary with bragflo regions as keys and pflotran regions as
     values.
  '''
  pf_reg_dict = {'WAS_AREA':'rWAS_AREA',
              'SPCS':'rSPCS',
              'SROR':'rSROR',
              'MPCS':'rMPCS',
              'NROR':'rNROR',
              'NPCS':'rNPCS',
              'OPS':'rOPS_AREA',
              'SHAFT':'rSHAFT_unofficial',
              'EXP':'rEXP_AREA'
             }
  return pf_reg_dict

def get_pf_variables():
  '''Return dictionary with bragflo variables as keys and pflotran variables as
     values.
  '''
  pf_var_dict = {'PRESBRIN':'VA Liquid Pressure [Pa]',
              'SATBRINE':'VA Liquid Saturation',
              'PRESGAS':'VA Gas Pressure [Pa]',
              'SATGAS':'VA Gas Saturation',
              'POROS':'VA Effective Porosity',
              #brine volume will need to be added if we decide to plot it
             }
  return pf_var_dict

def get_long_names():
  '''Return dictionary with bragflo regions as keys and descriptions as
     values.
  '''
  name_dict = {'WAS_AREA':'Waste Area',
               'SPCS':'South Panel Closure',
               'SROR':'South Rest of Repository',
               'MPCS':'Middle Panel Closure',
               'NROR':'North Rest of Repository',
               'NPCS':'North Panel Closure',
               'OPS':'Operations Area',
               'SHAFT':'Shaft',
               'EXP':'Experimental Area',
              }
  return name_dict

def get_min_denom():
  '''Return dictionsar of minimum values to use in the denominator of
     percent difference calculation. Not sure these are good values.
  '''
  denom_dict = {
                'PRESBRIN' : 1.0E+5 * 1.0E-2,
                'SATBRINE' : 1.0E-2 * 1.0E-2,
                'POROS' : 1.0E-2 * 1.0E-2,
                'DENBRINE' : 1.0E+3 * 1.0E-2,
                'DENGAS' : 1.0E-1 * 1.0E-2,
                'RXN' : 1.0E-12 * 1.0E-2,
                'CONC' : 1.0E-12 * 1.0E-2,
                'MASSFLUX' : 1.0E-2,
               }
  return denom_dict

###############################################################################
## Main code
def main(hf_bragflo_sum,replicates,scenarios,vectors,regions,compare_times):
  
  # Define directory to hold the generated .png image files
  fig_dir = os.path.join(os.getcwd(),'pfd_comparison_figures');
  if os.path.isdir(fig_dir) == True:
    print('Folder {} exists.'.format(fig_dir))
    keep = raw_input('Do you want to overwrite it? [yes/no]')
    if keep == 'yes' or keep == 'y':
      print('Deleting {}'.format(fig_dir))
      shutil.rmtree(fig_dir)
    elif keep == 'no' or keep == 'n':
      sys.exit('Keeping {}. Exiting.'.format(fig_dir))
    else:
      sys.exit('Unknown response. Exiting.')
    print('Creating {}'.format(fig_dir))
  os.mkdir(fig_dir)

  # Open pdf file to write images to
  # if I use this will need to pass it into plotting fns
  fname_fig_pdf = 'pfd_comparison_figures.pdf';
  pdf_figures = PdfPages(os.path.join(fig_dir,fname_fig_pdf));
  
  # Make line plots of ave'd outputs vs. time
  if (replicates == [] or vectors == []):
    print('No replicates/vectors specified for plotting. Not making line plots')
  else:
    for s in scenarios :
      grp_scenario = hf_bragflo_sum['/Scenario {:d}'.format(s)];
      for r in replicates:
        for v in vectors:
          ave_v_time(grp_scenario,r,s,v,regions,fig_dir,pdf_figures)

  # Make bar plots of root mean square difference 
  for s in scenarios:
    grp_scenario = hf_bragflo_sum['/Scenario {:d}'.format(s)];
    maxabs(grp_scenario,s,regions,compare_times,fig_dir,pdf_figures)
    #rmsd(grp_scenario,s,regions,compare_times,fig_dir,pdf_figures)

  hf_bragflo_sum.close();
  pdf_figures.close();
  
###############################################################################
# Run main
if __name__ == '__main__':

  parser = argparse.ArgumentParser(
    description='Compare PFLOTRAN and BRAGFLO volume-weighted averages.')
  parser.add_argument('-f','--bragflo_h5_file',
                      help='hdf5 file containing summary of bragflo output' +
                           ' [default = ./pfd_analysis_bragflo_summary.h5]',
                      default='pfd_analysis_bragflo_summary.h5',
                      required=False)
  parser.add_argument('-s','--scenarios', nargs='+',
                      help='int, list of ints [1-6], or keyword "all"' +
                           ' specifying scenario or scenarios to plot' +
                           ' [default = all]',
                      default=['all'],
                      required=False)
  parser.add_argument('-r','--replicates', nargs='+',
                      help='int, list of ints [1-3], or keyword "all"' +
                           ' specifying replicates for plots v. time' +
                           ' [default is no line plots]',
                      default=[],
                      required=False)
  parser.add_argument('-v','--vectors', nargs='+',
                      help='int, list of ints [1-300], or keyword "all"' +
                           ' specifying vectors for plots v. time' +
                           ' [default is no line plots]',
                      default=[],
                      required=False)
  args = parser.parse_args()
                    
  # check that input makes sense
  if args.replicates == ['all']:
    replicates = [x+1 for x in xrange(3)]
  else: replicates = [int(x) for x in args.replicates]
  for r in replicates:
    if (r<0 or r>3):
      print('ERROR: r{} should not exist. Exiting'.format(r))
      sys.exit()

  if args.scenarios == ['all']:
    scenarios = [x+1 for x in xrange(6)]
  else: scenarios = [int(x) for x in args.scenarios]
  for s in scenarios:
    if (s<0 or s>6):
      print('ERROR: s{} should not exist. Exiting'.format(s))
      sys.exit()

  if args.vectors == ['all']:
    vectors = [x+1 for x in xrange(100)]
  else: vectors = [int(x) for x in args.vectors]
  for v in vectors:
    if (v<0 or v>300):
      print('ERROR: v{} should not exist. Exiting'.format(v))
      sys.exit()

  # Open BRAGFLO h5  summary file
  try: 
    bf_h5 = h5py.File(args.bragflo_h5_file, mode='r')
  except:
    print( 'Cannot open {}'.format(args.bragflo_h5_file))
    sys.exit()
    
  regions = ['WAS_AREA','SPCS','SROR','MPCS',
             'NROR','NPCS','OPS','SHAFT','EXP']
  compare_times = [1.,99.,349.,549.,750.,999.,1199.,
                   1400.,1999.,3000.,4000.,5000.,10000.]
  main(bf_h5,replicates,scenarios,vectors,regions,compare_times);

###############################################################################
