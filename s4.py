'''
This file defines input that is necessary to a single scenario:
s4
including:
1. brine, a flag equal to either 'salado' or 'castile'
2. variables used in SIMULATION block
3. stratalist, a list of tuples for STRATA blocks
4. variables related to time step control (TIME)
5. variables related to solver behavior (TIMESTEPPER, NEWTON_SOLVER, LINEAR_SOLVER)
6. variables related to output (OUTPUT)

It is read and executed as a python script by prepflotran.py
Variable names in this file must match those in prepflotran.py

Emily 11.3.17
'''

#brine flag is used to determine whether BRUCITEC or BRUCITES 
#is written to WIPP_SOURCE_SINK
brine = 'salado'

#SIMULATION stuff
#Options that are required are hardcoded in block_templates.py
#Remove '!' to activate an option. Add leading '!' to deactivate.
s_flow_options = {
                  'ALLOW_NEGATIVE_GAS_PRESSURE':'',
                  'BRAGFLO_RESIDUAL_UNITS':'',
                  '2D_FLARED_DIRICHLET_BCS':'\n  EXTERNAL_FILE ../dirichlet_bcs.txt\n/',
                  'SCALE_JACOBIAN':'                        ! LSCALE',
                  'CONVERGENCE_TEST':'BOTH                  ! ICONVTEST 1',
                  'LIQUID_RESIDUAL_INFINITY_TOL':'1.d-2     ! FTOL_SAT',
                  'GAS_RESIDUAL_INFINITY_TOL':'1.d-2        ! FTOL_PRES',
                  'MAX_ALLOW_REL_LIQ_PRES_CHANG_NI':'1.d-2  ! EPS_PRES',
                  'MAX_ALLOW_REL_GAS_SAT_CHANGE_NI':'1.d-3  ! EPS_SAT',
                  'GAS_SAT_THRESH_FORCE_EXTRA_NI':'1.d-3    ! SATLIMIT',
                  'GAS_SAT_THRESH_FORCE_TS_CUT':'0.2d0      ! DSATLIM',
                  'MIN_LIQ_PRES_FORCE_TS_CUT':'-1.d8        ! DPRELIM',
                  'MAX_ALLOW_GAS_SAT_CHANGE_TS':'1.d0       ! DSAT_MAX',
                  'MAX_ALLOW_LIQ_PRES_CHANGE_TS':'1.d7      ! DPRES_MAX',
                  'GAS_SAT_CHANGE_TS_GOVERNOR':'0.3d0       ! SATNORM',
                  'LIQ_PRES_CHANGE_TS_GOVERNOR':'5.d5       ! PRESNORM',
                  'GAS_SAT_GOV_SWITCH_ABS_TO_REL':'1.d0     ! TSWITCH',
                  'MINIMUM_TIMESTEP_SIZE':'8.64d-4          ! DELTMIN',
                 }
if brine.lower() != 'salado' and brine.lower() != 'castile': 
  print('Warning: WIPP_SOURCE_SINK will be written, but not used.')
  s_wippsourcesink = False
  brine = 'castile' #valid value necessary for WippSourceSink()
else:              #even if you are not turning it on
  s_wippsourcesink = True
s_transport = False
s_chkpt = False
s_chkpt_format = 'HDF5'
s_chkpt_times = [0.,350.,550.,10000.]
s_chkpt_unit = 'y'
s_restart = True
s_restart_0 = True

#STRATA stuff
#three types of strata:
#1. materials from .h5 file
#   ('filename')
#2. initial or unchanging material by coupling with region
#   ('region','material')
#3. material change
#   ('region','material',start time,end time,'unit')
#units need to be consistent with PFLOTRAN usage
#I will look that up and add instructions

stratalist = [('../grid/materials.h5'), #t = -5y
              ('rWAS_AREA','WAS_AREA'), #t = 0y
              ('rPCS','PCS_T1',0.,100.,'y'),
              ('rREPOSIT','REPOSIT'),
              ('rDRZ','DRZ_1'),
              ('rDRZ_OE','DRZ_OE_1'),
              ('rDRZ_PCS','DRZ_PC_1',0.,200.,'y'),
              ('rEXP_AREA','EXP_AREA'),
              ('rOPS_AREA','OPS_AREA'),
              ('rCONC_MON','CONC_MON'),
              ('rSHFTL','SHFTL_T1',0.,200.,'y'),
              ('rSHFTU','SHFTU'),
              ('rCULEBRA','CULEBRA'),
              ('rFORTYNIN','FORTYNIN'),
              ('rMAGENTA','MAGENTA'),
              ('rDEWYLAKE','DEWYLAKE'),
              ('rTAMARISK','TAMARISK'),
              ('rUNNAMED','UNNAMED'),
              ('rSANTAROS','SANTAROS'),
              ('rPCS','PCS_T2',100.,200.,'y'), #t = 100 y
              ('rPCS','PCS_T3',200.,10000.,'y'), #t = 200 y
              ('rDRZ_PCS','DRZ_PCS',200.,10000.,'y'),
              ('rSHFTL','SHFTL_T2',200.,10000.,'y'),
              ('rBH_OPEN','BH_OPEN',350.,550.,'y'), #t = 350 y
              ('rCONC_PLG','CONC_PLG',350.,550.,'y'),
              ('rBH_OPEN','BH_SAND',550.,10000.,'y'), #t = 550 y
              ('rCONC_PLG','BH_SAND',550.,10000.,'y'),
             ]
              
#TIME stuff
t_final = 10000.  # years
t_final_unit = 'y'
t_initial_step = 8.64e+02
t_initial_unit = 's' #'d'
#{at time : limit to this time step}
t_max_step = {0.000000000e+00:1.728e+09,
              3.153600000e+09:8.64e+02, #100 y
              3.153600864e+09:1.728e+09,
              6.307200000e+09:8.64e+02, #200 y
              6.307200864e+09:1.728e+09,
              1.1037600000e+10:8.64e+02, #350 y
              1.1037600864e+10:1.728e+09,
              1.7344800000e+10:8.64e+02, #550 y
              1.7344800864e+10:1.728e+09,
             }
t_max_unit = 's'

#TIMESTEPPER stuff
ts_flow_pm = 'FLOW'
ts_flow_maxcuts = 40
ts_flow_maxsteps = 20000
#hardwired TS_ACCCELERATION and DT_FACTOR into block template

#NEWTON_SOLVER FLOW
ns_flow_pm = 'FLOW'
ns_flow_maxit = 8

#LINEAR_SOLVER FLOW
ls_flow_pm = 'FLOW'
ls_flow_solver = 'DIRECT'

#OBSERVATION points
obslist = ['rWAS_AREA',
           'rSROR',
           'rNROR',
           'rOPS_AREA',
           'rEXP_AREA',
           'rSPCS',
           'rMPCS',
           'rNPCS',
           'rSHAFT_unofficial',
          ]

#OUTPUT stuff
o_variables = ['!MAXIMUM_PRESSURE',
               'LIQUID_PRESSURE',
               'LIQUID_SATURATION',
               'LIQUID_DENSITY',
               'GAS_PRESSURE',
               'GAS_SATURATION',
               'GAS_DENSITY',
               '!LIQUID_MOLE_FRACTIONS',
               '!GAS_MOLE_FRACTIONS',
               'CAPILLARY_PRESSURE',
               '!VAPOR_PRESSURE',
               '!SATURATION_PRESSURE',
               '!AIR_PRESSURE',
               '!TEMPERATURE',
               'EFFECTIVE_POROSITY',
               '!POROSITY',
               'PERMEABILITY',
               'MATERIAL_ID',
               '!MATERIAL_ID_KLUDGE_FOR_VISIT',
               'VOLUME']
o_format = ['HDF5']
o_times = [
   0.00000000000000e+00,
   3.15360000000000e+07,
   6.30720000000000e+07,
   9.46080000000000e+07,
   1.26144000000000e+08,
   1.57680000000000e+08,
   1.89216000000000e+08,
   2.20752000000000e+08,
   2.52288000000000e+08,
   2.83824000000000e+08,
   3.15360000000000e+08,
   6.30720000000000e+08,
   9.46080000000000e+08,
   1.26144000000000e+09,
   1.57680000000000e+09,
   1.89216000000000e+09,
   2.20752000000000e+09,
   2.52288000000000e+09,
   2.83824000000000e+09,
   3.12206400000000e+09,
   3.15360000000000e+09,
   3.18513600000000e+09,
   4.73040000000000e+09,
   6.27566400000000e+09,
   6.33873600000000e+09,
   7.88400000000000e+09,
   9.46080000000000e+09,
   1.10060640000000e+10,
   1.10376000000000e+10,
   1.10691360000000e+10,
   1.26144000000000e+10,
   1.41912000000000e+10,
   1.57680000000000e+10,
   1.73132640000000e+10,
   1.73763360000000e+10,
   1.89216000000000e+10,
   2.04984000000000e+10,
   2.20752000000000e+10,
   2.36520000000000e+10,
   2.52288000000000e+10,
   2.68056000000000e+10,
   2.83824000000000e+10,
   2.99592000000000e+10,
   3.15044640000000e+10,
   3.15360000000000e+10,
   3.15675360000000e+10,
   3.46896000000000e+10,
   3.78116640000000e+10,
   3.78747360000000e+10,
   4.09968000000000e+10,
   4.41504000000000e+10,
   4.73040000000000e+10,
   4.88492640000000e+10,
   4.89123360000000e+10,
   5.04576000000000e+10,
   5.36112000000000e+10,
   5.67648000000000e+10,
   5.99184000000000e+10,
   6.30404640000000e+10,
   6.31035360000000e+10,
   9.46080000000000e+10,
   1.26144000000000e+11,
   1.57680000000000e+11,
   1.89216000000000e+11,
   2.20752000000000e+11,
   2.52288000000000e+11,
   2.83824000000000e+11,
   3.15360000000000e+11,
          ]
o_times_unit = 's'  # y for years, d for days, etc
o_periodic_times = {3.15360000000000e+07:[0.,3.15360000000000e+07]}
o_periodic_unit = 's'

############ FLOW_CONDITION ##################
#Works only with MODE WIPP_FLOW or MODE BRAGFLO
#(name, liquid pressure, gas saturation)
#values can be numeric or refer to pflotran dataset name listed above
liqp_dataset = 'initial_liq_pressure'
gass_dataset = 'initial_gas_saturation'
flowlist = [('initial',liqp_dataset,gass_dataset),
            ('culebra',CULEBRA.pressure,0.),
            ('magenta',MAGENTA.pressure,0.),
            ('top',BRINESAL.ref_pres,1.-SANTAROS.sat_ibrn),
           ]

############ INITIAL_CONDITION ###############
#Irrelevant if initial conditions are read from restart file.
# (flow condition, (transport condition), region)
iclist = [('initial','all'),
          ('culebra','rCULEBRA'),
          ('magenta','rMAGENTA')]

############ BOUNDARY CONDITION ##############
#(flow condition, (transport condition), region)
bclist = [('top','top_edges'),]


