'''
paraview python script (pvpython) to plot presbrin differences at all saved times
later this might be generalized to plot any output variable difference you choose
Emily
01.16.18
'''
import os
import sys 
import shutil

if len(sys.argv) != 8:
  print('ERROR: Incorrect command line arguments.\n')
  print('Usage: pvpython paraview_plot_presbrin_diff.py {replicate} {scenario} {vector} {output variable} {n_pflotran_xmf} {n_diff_xmf} [times]')
  print('Where {output variable} is BRAGFLO output variable name:')
  print('PRESBRIN, SATBRINE, PRESGAS, SATGAS, ...')
  print('And [times] is a list (in brackets []) of floats')
  print('with comma-separated values - no spaces!!!!')
  sys.exit()
r = sys.argv[1] #replicate
s = sys.argv[2] #scenario
v = sys.argv[3] #vector
bf_o = sys.argv[4] #bragflo output variable
n_pflo = int(sys.argv[5])
n_diff = int(sys.argv[6])
print_times = sys.argv[7].strip('[]').split(',')
for i in xrange(len(print_times)):
  print_times[i] = float(print_times[i])

#set variable names:
if bf_o == 'PRESBRIN':
  pf_o = 'Liquid Pressure [Pa]'
  diff = 'PRESBRIN_Difference'
  perc = 'PRESBRIN_Relative_Diff'
elif bf_o == 'SATBRINE':
  pf_o = 'Liquid Saturation'
  diff = 'SATBRINE_Difference'
  perc = 'SATBRINE_Relative_Diff'
elif bf_o == 'PRESGAS':
  pf_o = 'Gas Pressure [Pa]'
  diff = 'PRESGAS_Difference'
  perc = 'PRESGAS_Relative_Diff'
elif bf_o == 'SATGAS':
  pf_o = 'Gas Saturation'
  diff = 'SATGAS_Difference'
  perc = 'SATGAS_Relative_Diff'
elif bf_o == 'PCGW':
  pf_o = 'Capillary Pressure [Pa]'
  diff = 'PCGW_Difference'
  perc = 'PCGW_Relative_Diff'
elif bf_o == 'POROS':
  pf_o = 'Effective Porosity'
  diff = 'POROS_Difference'
  perc = 'POROS_Relative_Diff'
elif bf_o == 'DENBRINE':
  pf_o = 'Liquid Density [kg_m^3]'
  diff = 'DENBRINE_Difference'
  perc = 'DENBRINE_Relative_Diff'
elif bf_o == 'DENGAS':
  pf_o = 'Gas Density [kg_m^3]'
  diff = 'DENGAS_Difference'
  perc = 'DENGAS_Relative_Diff'
else:
  print('ERROR: Unrecognized output variable. Choose from:')
  print('PRESBRIN, SATBRINE, PRESGAS, SATGAS, PCGW, POROS,')
  print('DENBRIN, DENGAS')
  sys.exit()

#shell scripts creates paraview_plot_dir - make sure it exists
print('Plotting PFD_r{0}_s{1}_v{2} {3}'.format(r,s,v,bf_o))
paraview_plot_dir = os.path.join(os.getcwd(),'paraview_plot_dir')
if not os.path.isdir(paraview_plot_dir):
  print('ERROR: {} does not exist. Create it and try again.'.format(
        paraview_plot_dir))
  print('Exiting.')
  sys.exit()

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'XDMF Reader'
differenceReader = XDMFReader(FileNames=[os.path.realpath(os.getcwd()) +
                   '/example_bragflo/bf2_PFD_r{0}_s{1}_v{2}_difference-{3}.xmf'.format(
                    r,s,v,t) for t in xrange(n_diff)])
differenceReader.CellArrayStatus = ['Cell_Ids', 'DENBRINE_Difference', 'DENBRINE_Relative_Diff', 'DENGAS_Difference', 'DENGAS_Relative_Diff', 'Material ID', 'PCGW_Difference', 'PCGW_Relative_Diff', 'POROS_Difference', 'POROS_Relative_Diff', 'PRESBRIN_Difference', 'PRESBRIN_Relative_Diff', 'PRESGAS_Difference', 'PRESGAS_Relative_Diff', 'SATBRINE_Difference', 'SATBRINE_Relative_Diff', 'SATGAS_Difference', 'SATGAS_Relative_Diff', 'XC', 'YC', 'ZC']

# get animation scene
#animationScene1 = GetAnimationScene()

# update animation scene based on data timesteps
#animationScene1.UpdateAnimationUsingDataTimeSteps()

# Properties modified on differenceReader
differenceReader.GridStatus = ['Mesh']

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
renderView1.ViewSize = [1024, 1024]

# get color transfer function/color map for 'XC'
xCLUT = GetColorTransferFunction('XC')

# get opacity transfer function/opacity map for 'XC'
xCPWF = GetOpacityTransferFunction('XC')

# show data in view
differenceReaderDisplay = Show(differenceReader, renderView1)
# trace defaults for the display properties.
differenceReaderDisplay.Representation = 'Surface With Edges'
differenceReaderDisplay.ColorArrayName = ['CELLS', 'XC']
differenceReaderDisplay.LookupTable = xCLUT
differenceReaderDisplay.OSPRayScaleArray = 'XC'
differenceReaderDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
differenceReaderDisplay.SelectOrientationVectors = 'None'
differenceReaderDisplay.ScaleFactor = 6.800000000000001
differenceReaderDisplay.SelectScaleArray = 'XC'
differenceReaderDisplay.GlyphType = 'Arrow'
differenceReaderDisplay.GlyphTableIndexArray = 'XC'
differenceReaderDisplay.DataAxesGrid = 'GridAxesRepresentation'
differenceReaderDisplay.PolarAxes = 'PolarAxesRepresentation'
differenceReaderDisplay.ScalarOpacityFunction = xCPWF
differenceReaderDisplay.ScalarOpacityUnitDistance = 5.773810161355497
differenceReaderDisplay.GaussianRadius = 3.4000000000000004
differenceReaderDisplay.SetScaleArray = [None, '']
differenceReaderDisplay.ScaleTransferFunction = 'PiecewiseFunction'
differenceReaderDisplay.OpacityArray = [None, '']
differenceReaderDisplay.OpacityTransferFunction = 'PiecewiseFunction'

# reset view to fit data
renderView1.ResetCamera()

# show color bar/color legend
differenceReaderDisplay.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
#renderView1.Update()

# reset view to fit data
#renderView1.ResetCamera()

# get layout
layout1 = GetLayout()

# split cell
layout1.SplitHorizontal(0, 0.5)

# set active view
SetActiveView(None)

# Create a new 'Render View'
renderView2 = CreateView('RenderView')
renderView2.ViewSize = [1024, 1024]
renderView2.AxesGrid = 'GridAxes3DActor'
renderView2.StereoType = 0
renderView2.Background = [0.32, 0.34, 0.43]

# place view in the layout
layout1.AssignView(2, renderView2)

# set active source
SetActiveSource(differenceReader)

# show data in view
differenceReaderDisplay_1 = Show(differenceReader, renderView2)
# trace defaults for the display properties.
differenceReaderDisplay_1.Representation = 'Surface With Edges'
differenceReaderDisplay_1.ColorArrayName = ['CELLS', 'XC']
differenceReaderDisplay_1.LookupTable = xCLUT
differenceReaderDisplay_1.OSPRayScaleArray = 'XC'
differenceReaderDisplay_1.OSPRayScaleFunction = 'PiecewiseFunction'
differenceReaderDisplay_1.SelectOrientationVectors = 'None'
differenceReaderDisplay_1.ScaleFactor = 6.800000000000001
differenceReaderDisplay_1.SelectScaleArray = 'XC'
differenceReaderDisplay_1.GlyphType = 'Arrow'
differenceReaderDisplay_1.GlyphTableIndexArray = 'XC'
differenceReaderDisplay_1.DataAxesGrid = 'GridAxesRepresentation'
differenceReaderDisplay_1.PolarAxes = 'PolarAxesRepresentation'
differenceReaderDisplay_1.ScalarOpacityFunction = xCPWF
differenceReaderDisplay_1.ScalarOpacityUnitDistance = 5.773810161355497
differenceReaderDisplay_1.GaussianRadius = 3.4000000000000004
differenceReaderDisplay_1.SetScaleArray = [None, '']
differenceReaderDisplay_1.ScaleTransferFunction = 'PiecewiseFunction'
differenceReaderDisplay_1.OpacityArray = [None, '']
differenceReaderDisplay_1.OpacityTransferFunction = 'PiecewiseFunction'

# show color bar/color legend
differenceReaderDisplay_1.SetScalarBarVisibility(renderView2, True)

# reset view to fit data
renderView2.ResetCamera()

# reset view to fit data
#renderView2.ResetCamera()

# set active view
SetActiveView(renderView1)

# split cell
layout1.SplitVertical(1, 0.5)

# set active view
SetActiveView(None)

# Create a new 'Render View'
renderView3 = CreateView('RenderView')
renderView3.ViewSize = [1024, 1024]
renderView3.AxesGrid = 'GridAxes3DActor'
renderView3.StereoType = 0
renderView3.Background = [0.32, 0.34, 0.43]

# place view in the layout
layout1.AssignView(4, renderView3)

# create a new 'XDMF Reader'
pflotranReader = XDMFReader(FileNames=[os.path.realpath(os.getcwd()) +
                 '/s{1}/pf_PFD_r{0}_s{1}_v{2}-{3}.xmf'.format(
                 r,s,v,t) for t in xrange(n_pflo)])
pflotranReader.CellArrayStatus = ['Capillary Pressure [Pa]', 'Cell_Ids', 'Effective Porosity', 'Gas Density [kg_m^3]', 'Gas Pressure [Pa]', 'Gas Saturation', 'Liquid Density [kg_m^3]', 'Liquid Pressure [Pa]', 'Liquid Saturation', 'Material ID', 'Permeability X [m^2]', 'Volume [m^3]', 'XC', 'YC', 'ZC']

# Properties modified on pflotranReader
pflotranReader.GridStatus = ['Mesh']

# show data in view
pflotranReaderDisplay = Show(pflotranReader, renderView3)
# trace defaults for the display properties.
pflotranReaderDisplay.Representation = 'Surface With Edges'
pflotranReaderDisplay.ColorArrayName = ['CELLS', 'XC']
pflotranReaderDisplay.LookupTable = xCLUT
pflotranReaderDisplay.OSPRayScaleArray = 'XC'
pflotranReaderDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
pflotranReaderDisplay.SelectOrientationVectors = 'None'
pflotranReaderDisplay.ScaleFactor = 6.800000000000001
pflotranReaderDisplay.SelectScaleArray = 'XC'
pflotranReaderDisplay.GlyphType = 'Arrow'
pflotranReaderDisplay.GlyphTableIndexArray = 'XC'
pflotranReaderDisplay.DataAxesGrid = 'GridAxesRepresentation'
pflotranReaderDisplay.PolarAxes = 'PolarAxesRepresentation'
pflotranReaderDisplay.ScalarOpacityFunction = xCPWF
pflotranReaderDisplay.ScalarOpacityUnitDistance = 5.773810161355497
pflotranReaderDisplay.GaussianRadius = 3.4000000000000004
pflotranReaderDisplay.SetScaleArray = [None, '']
pflotranReaderDisplay.ScaleTransferFunction = 'PiecewiseFunction'
pflotranReaderDisplay.OpacityArray = [None, '']
pflotranReaderDisplay.OpacityTransferFunction = 'PiecewiseFunction'

# reset view to fit data
renderView3.ResetCamera()

# show color bar/color legend
pflotranReaderDisplay.SetScalarBarVisibility(renderView3, True)

# update the view to ensure updated data information
#renderView3.Update()

# reset view to fit data
#renderView3.ResetCamera()

# set active view
SetActiveView(renderView2)

# split cell
layout1.SplitVertical(2, 0.5)

# set active view
SetActiveView(None)

# Create a new 'Render View'
renderView4 = CreateView('RenderView')
renderView4.ViewSize = [1024, 1024]
renderView4.AxesGrid = 'GridAxes3DActor'
renderView4.StereoType = 0
renderView4.Background = [0.32, 0.34, 0.43]

# place view in the layout
layout1.AssignView(6, renderView4)

# create a new 'VisItPFLOTRANReader'
bragfloReader = VisItPFLOTRANReader(FileName=os.path.realpath(os.getcwd()) +
                       '/example_bragflo/bf2_PFD_r{0}_s{1}_v{2}.bf.h5'.format(
                       r,s,v))

bragfloReader.Meshes = ['mesh']
bragfloReader.CellArrays = []

# Properties modified on bragfloReader
bragfloReader.CellArrays = ['BIORATH', 'BIORATI', 'BRINRATE', 'CBRBR', 'CELLCONC', 'CELLRATE', 'CGASBR', 'CORRATH', 'CORRATI', 'CWELLBR', 'CWELLGAS', 'DENBRINE', 'DENGAS', 'FECONC', 'FEOH2C', 'FEOH2R', 'FEOH2_SR', 'FERATE', 'FESC', 'FESR', 'FE_SR', 'FLOWBRX', 'FLOWBRY', 'FLOWBRZ', 'FLOWGASX', 'FLOWGASY', 'FLOWGASZ', 'H2RATE', 'HYMAGC', 'HYMAGR', 'HYMAG_CR', 'MASSBALB', 'MASSBALG', 'MGCO3C', 'MGCO3R', 'MGOC', 'MGOH2C', 'MGOH2R', 'MGOH2_CR', 'MGOR', 'MGO_CR', 'MGO_HR', 'PBUB', 'PCGW', 'PERMBRX', 'PERMBRY', 'PERMBRZ', 'PERMGASX', 'PERMGASY', 'PERMGASZ', 'POROS', 'PORSOLID', 'POTG', 'POTO', 'PRESBRIN', 'PRESGAS', 'RELPERMB', 'RELPERMG', 'SALTC', 'SATBRINE', 'SATGAS', 'VELDBRX', 'VELDBRY', 'VELDBRZ', 'VELDGASX', 'VELDGASY', 'VELDGASZ', 'WELLBRIN', 'WELLGAS']

# show data in view
bragfloReaderDisplay = Show(bragfloReader, renderView4)
# trace defaults for the display properties.
bragfloReaderDisplay.Representation = 'Surface With Edges'
bragfloReaderDisplay.ColorArrayName = [None, '']
bragfloReaderDisplay.OSPRayScaleArray = 'BIORATH'
bragfloReaderDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
bragfloReaderDisplay.SelectOrientationVectors = 'None'
bragfloReaderDisplay.ScaleFactor = 6.800000000000001
bragfloReaderDisplay.SelectScaleArray = 'None'
bragfloReaderDisplay.GlyphType = 'Arrow'
bragfloReaderDisplay.GlyphTableIndexArray = 'None'
bragfloReaderDisplay.DataAxesGrid = 'GridAxesRepresentation'
bragfloReaderDisplay.PolarAxes = 'PolarAxesRepresentation'
bragfloReaderDisplay.GaussianRadius = 3.4000000000000004
bragfloReaderDisplay.SetScaleArray = [None, '']
bragfloReaderDisplay.ScaleTransferFunction = 'PiecewiseFunction'
bragfloReaderDisplay.OpacityArray = [None, '']
bragfloReaderDisplay.OpacityTransferFunction = 'PiecewiseFunction'

# reset view to fit data
renderView4.ResetCamera()

# update the view to ensure updated data information
#renderView4.Update()

# change representation type
bragfloReaderDisplay.SetRepresentationType('Surface With Edges')

# set active view
#SetActiveView(renderView2)
# set active view
#SetActiveView(renderView1)
# set active view
#SetActiveView(renderView3)
# set active view
#SetActiveView(renderView1)
# set active view
#SetActiveView(renderView4)

# create a new 'Text'
text1 = Text()
# Properties modified on text1
text1.Text = 'bragflo'
# show data in view
text1Display = Show(text1, renderView4)
# update the view to ensure updated data information
renderView4.Update()
# Properties modified on text1Display
text1Display.WindowLocation = 'LowerLeftCorner'

# set active view
#SetActiveView(renderView3)

# create a new 'Text'
text2 = Text()
# Properties modified on text2
text2.Text = 'pflotran'
# show data in view
text2Display = Show(text2, renderView3)
# update the view to ensure updated data information
renderView3.Update()
# Properties modified on text2Display
text2Display.WindowLocation = 'LowerLeftCorner'

# set active view
#SetActiveView(renderView1)

# create a new 'Text'
text3 = Text()
# Properties modified on text3
text3.Text = 'PFD_r{0}_s{1}_v{2}  {3}'.format(r,s,v,bf_o)
# show data in view
text3Display = Show(text3, renderView1)
# update the view to ensure updated data information
renderView1.Update()

# set active view
#SetActiveView(renderView2)

# create a new 'Annotate Time Filter'
annotateTimeFilter1 = AnnotateTimeFilter(Input=differenceReader)
# Properties modified on annotateTimeFilter1
annotateTimeFilter1.Format = 'Time: %0.2f y'
# show data in view
annotateTimeFilter1Display = Show(annotateTimeFilter1, renderView1)
annotateTimeFilter2Display = Show(annotateTimeFilter1, renderView2)
# update the view to ensure updated data information
renderView1.Update()
renderView2.Update()
# Properties modified on annotateTimeFilter1Display
annotateTimeFilter1Display.WindowLocation = 'UpperRightCorner'
annotateTimeFilter2Display.WindowLocation = 'UpperRightCorner'

# create a new 'Annotate Time Filter'
annotateTimeFilter3 = AnnotateTimeFilter(Input=pflotranReader)
# Properties modified on annotateTimeFilter1
annotateTimeFilter3.Format = 'Time: %0.2f y'
# show data in view
annotateTimeFilter3Display = Show(annotateTimeFilter3, renderView3)
# update the view to ensure updated data information
renderView3.Update()
# Properties modified on annotateTimeFilter1Display
annotateTimeFilter3Display.WindowLocation = 'UpperRightCorner'

# create a new 'Annotate Time Filter'
annotateTimeFilter4 = AnnotateTimeFilter(Input=bragfloReader)
# Properties modified on annotateTimeFilter1
annotateTimeFilter4.Format = 'Time: %0.2f y'
# show data in view
annotateTimeFilter4Display = Show(annotateTimeFilter4, renderView4)
# update the view to ensure updated data information
renderView4.Update()
# Properties modified on annotateTimeFilter1Display
annotateTimeFilter4Display.WindowLocation = 'UpperRightCorner'

################# Loop through times ####################
## some of this could probably occur outside the loop ###
for t in print_times:
  ts_diff = differenceReader.TimestepValues
  ts_pflo = pflotranReader.TimestepValues
  ts_brag = bragfloReader.TimestepValues
  renderView1.ViewTime = -999.
  renderView2.ViewTime = -999.
  renderView3.ViewTime = -999.
  renderView4.ViewTime = -999.
  for ts in ts_diff:
    if ts == t:
      renderView1.ViewTime = ts
      renderView2.ViewTime = ts
  for ts in ts_pflo:
    if ts == t:
      renderView3.ViewTime = ts
  for ts in ts_brag:
    if ts == t:
      renderView4.ViewTime = ts
  if (renderView1 == -999. or renderView2 == -999. or
      renderView3 == -999. or renderView4 == -999.):
    print('ERROR: print time {} does not occur in all input files'.format(t))
    sys.exit()

  # set active view
  SetActiveView(renderView1)
  # set active source
  SetActiveSource(differenceReader)
  # set scalar coloring
  ColorBy(differenceReaderDisplay, ('CELLS', diff))
  # update sooner to catch new color scale
  renderView1.Update()
  # Hide the scalar bar for this color map if no visible data is colored by it.
  HideScalarBarIfNotNeeded(xCLUT, renderView1)
  # rescale color and/or opacity maps used to include current data range
  differenceReaderDisplay.RescaleTransferFunctionToDataRange(True, False)
  # show color bar/color legend
  differenceReaderDisplay.SetScalarBarVisibility(renderView1, True)
  # get color transfer function/color map for 'PRESBRIN_Difference'
  pRESBRIN_DifferenceLUT = GetColorTransferFunction(diff)
  # rescale color and/or opacity maps used to exactly fit the current data range
  differenceReaderDisplay.RescaleTransferFunctionToDataRange(False, True)
  # update the view to ensure updated data information
  renderView1.Update()

  # set active view
  SetActiveView(renderView2)
  # set scalar coloring
  ColorBy(differenceReaderDisplay_1, ('CELLS', perc))
  # update sooner to catch new color scale
  renderView2.Update()
  # Hide the scalar bar for this color map if no visible data is colored by it.
  HideScalarBarIfNotNeeded(xCLUT, renderView2)
  # rescale color and/or opacity maps used to include current data range
  differenceReaderDisplay_1.RescaleTransferFunctionToDataRange(True, False)
  # show color bar/color legend
  differenceReaderDisplay_1.SetScalarBarVisibility(renderView2, True)
  # get color transfer function/color map for 'PRESBRIN_Relative_Diff'
  pRESBRIN_Relative_DiffLUT = GetColorTransferFunction(perc)
  # rescale color and/or opacity maps used to exactly fit the current data range
  differenceReaderDisplay_1.RescaleTransferFunctionToDataRange(False, True)
  # update the view to ensure updated data information
  renderView2.Update()

  # set active view
  SetActiveView(renderView3)
  # set active source
  SetActiveSource(pflotranReader)
  # set scalar coloring
  ColorBy(pflotranReaderDisplay, ('CELLS', pf_o))
  # update sooner to catch new color scale
  renderView3.Update()
  # Hide the scalar bar for this color map if no visible data is colored by it.
  HideScalarBarIfNotNeeded(xCLUT, renderView3)
  # rescale color and/or opacity maps used to include current data range
  pflotranReaderDisplay.RescaleTransferFunctionToDataRange(True, False)
  # show color bar/color legend
  pflotranReaderDisplay.SetScalarBarVisibility(renderView3, True)
  # get color transfer function/color map for 'LiquidPressurePa'
  #liquidPressurePaLUT = GetColorTransferFunction('LiquidPressurePa')
  liquidPressurePaLUT = GetColorTransferFunction(pf_o)
  # rescale color and/or opacity maps used to exactly fit the current data range
  pflotranReaderDisplay.RescaleTransferFunctionToDataRange(False, True)
  # update the view to ensure updated data information
  renderView3.Update()

  # set active view
  SetActiveView(renderView4)
  # set active source
  SetActiveSource(bragfloReader)
  # set scalar coloring
  ColorBy(bragfloReaderDisplay, ('CELLS', bf_o))
  # update sooner to catch new color scale
  renderView4.Update()
  # rescale color and/or opacity maps used to include current data range
  bragfloReaderDisplay.RescaleTransferFunctionToDataRange(True, False)
  # show color bar/color legend
  bragfloReaderDisplay.SetScalarBarVisibility(renderView4, True)
  # get color transfer function/color map for 'PRESBRIN'
  pRESBRINLUT = GetColorTransferFunction(bf_o)
  # rescale color and/or opacity maps used to exactly fit the current data range
  bragfloReaderDisplay.RescaleTransferFunctionToDataRange(False, True)
  # update the view to ensure updated data information
  renderView4.Update()

  # set active view
  #SetActiveView(renderView3)
  # set active source
  #SetActiveSource(pflotranReader)
  # change representation type
  #pflotranReaderDisplay.SetRepresentationType('Surface With Edges')
  # set active view
  #SetActiveView(renderView1)
  # set active source
  #SetActiveSource(differenceReader)
  # change representation type
  #differenceReaderDisplay.SetRepresentationType('Surface With Edges')
  # set active view
  #SetActiveView(renderView2)
  # change representation type
  #differenceReaderDisplay_1.SetRepresentationType('Surface With Edges')

  # current camera placement for renderView2
  renderView2.CameraPosition = [34.0, -78.8875131871483, 16.5]
  renderView2.CameraFocalPoint = [34.0, 67.14309427049578, 16.5]
  renderView2.CameraViewUp = [0.0, 0.0, 1.0]
  renderView2.CameraParallelScale = 37.795502377928514

  # current camera placement for renderView4
  renderView4.CameraPosition = [34.0, 16.5, 80.31362483225858]
  renderView4.CameraFocalPoint = [34.0, 16.5, -65.71698262538548]
  renderView4.CameraParallelScale = 37.795502377928514

  # current camera placement for renderView3
  renderView3.CameraPosition = [34.0, -78.8875131871483, 16.5]
  renderView3.CameraFocalPoint = [34.0, 67.14309427049578, 16.5]
  renderView3.CameraViewUp = [0.0, 0.0, 1.0]
  renderView3.CameraParallelScale = 37.795502377928514
  
  # current camera placement for renderView1
  renderView1.CameraPosition = [34.0, -78.8875131871483, 16.5]
  renderView1.CameraFocalPoint = [34.0, 67.14309427049578, 16.5]
  renderView1.CameraViewUp = [0.0, 0.0, 1.0]
  renderView1.CameraParallelScale = 37.795502377928514

  # save screenshot
  SaveScreenshot(os.path.join(paraview_plot_dir,
      'PFD_r{0}_s{1}_v{2}_{3}_diff-{4}.png'.format(r,s,v,t,bf_o)),
      layout1, SaveAllViews=1,
      ImageResolution=[1804, 1080])
