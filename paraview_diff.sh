#! /bin/bash 

# Before running this script, you must:
# copy desired bragflo output (*.bf.h5) from:
# jt:/home1/rsarath/pfd_analysis/bragflo_output
# to:
# ./example_bragflo
# cd ./example_bragflo
# ./setup_s1_diff.sh (may require edits)
# ./setup_s2_diff.sh (may require edits)
# cd ..
# ./setup_xmf.sh (may require edits)

#if necessary:
#export PARAVIEW_DIR=/path/to/your/paraview/directory

if test -d paraview_plot_dir; then
  read -p "paraview_plot_dir exists. Overwrite duplicate contents? [yes/no]: " stdin
  case $stdin in
    no | n) echo ""
      echo "If you want to keep paraview_plot_dir, move it and rerun script."
      echo "Exiting." 
      echo ""
      exit 1
      ;;
    yes | y) echo ""
      echo "Starting. Will overwrite duplicate contents..."
      echo ""
      ;;
    *) echo ""
      echo "Enter yes or no. Exiting."
      echo ""
      exit 1
      ;;
  esac
else
  mkdir paraview_plot_dir
fi

for s in {2,}; do #3,4,5,6}; do
  for r in {1,}; do #2,3}; do
    for v in {001,}; do
      for var in {PRESBRIN,SATBRINE,}; do #POROS,PRESGAS,SATGAS}; do

      #argv[] = [replicate scenario vector output_variable n_pflo n_diff [times]]
      $PARAVIEW_DIR/bin/pvbatch paraview_plot_diff.py $r $s $v $var \
        68 68 [0.,99.,101.,349.,351.,500.,549.,551.,600.,650.,700.,750.,800.,850.,950.,999.,1001.,1100.,1199.,1201.,1300.,1400.,1500.,1549.,1551.,1600.,1700.,1800.,1900.,1999.,2001.,3000.,4000.,10000.]

      done
    done
  done
done


# IMPORTANT NOTE!
# Ensure that correct number of xmf files is given.
# Ensure that all requested output times exist in pflotran, 
# braflo, and difference files.
# Script could run and produce garbage, if times are incorrect.

