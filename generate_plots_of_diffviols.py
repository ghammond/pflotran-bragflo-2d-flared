'''generate_plots_of_diffviols.py

   From *exceeds_per_diff.txt files find simulations where max % diff > threshold
   write shell script that calls horsetail.py to replot horsetails
   of violators.

   Glenn Hammond <gehammo@sandia.gov>
   Emily Stein <ergiamb@sandia.gov>
'''

import sys
import os
import shutil
import argparse

output_dir_block = '''output_dir={}
if test -d $output_dir; then
  read -p "$output_dir exists. Overwrite duplicate files? [yes/no]: " stdin
  case $stdin in
    no | n) echo ""
      echo "If you want to keep $output_dir, move it and rerun script."
      echo "Exiting." 
      echo ""
      exit 1
      ;;
    yes | y) echo ""
      echo "Will overwrite duplicate files. Starting..."
      echo ""
      ;;
    *) echo ""
      echo "Enter yes or no. Exiting."
      echo ""
      exit 1
      ;;
  esac
else
  mkdir $output_dir
fi\n
'''

output1 = 'horsetail_replot_diffviols_dir'
inputdir = 'horsetail_plot_dir_tt_nan'

command = 'python horsetail.py '
bf_path = '../bragflo_summary_tt_nan/pfd_analysis_bragflo_summary_scenario{}.h5 '
#bf_path = './example_bragflo/pfd_analysis_bragflo_summary_scenario{}.h5 '

outfilename = 'replot_diffviols.sh'
fout = open(outfilename,'w') #for violators
fout.write('#! /bin/bash\n\n')
fout.write(output_dir_block.format(output1))

for s in range(1,7):
  for r in range(1,4):
    for reg in ['WAS_AREA','SROR','NROR','SPCS','MPCS','NPCS','OPS','SHAFT','EXP']:
      for var in ['PRESBRIN','SATBRINE','POROS']:
        filename = os.path.join(inputdir,'s{}_r{}_{}_{}_exceeds_perdiff.txt'.format(
                     s,r,reg,var))
        try:
          with open(filename,'r') as f:
            vectors = f.readlines() #because it is one number per line!
        except IOError:
          #print('{} does not exist. skipping it.'.format(filename))
          break
        #add to shell scripts for violators
        if len(vectors) > 0:
          fout.write('{} -f {} -p s{} '.format(command,bf_path.format(s),s))
          fout.write('-d $output_dir -l horsetails differences -c rainbow ')
          fout.write('-s {} -r {} -e {} -a {} -v '.format(s,r,reg,var))
          for v in vectors:
            fout.write('{} '.format(int(v)))
          fout.write('\n')
    
fout.close()
print('done')
