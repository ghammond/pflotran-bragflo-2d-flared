#!/bin/bash

# Working version to edit as needed for playing around. Emily 12.19.17
###############################################################################
# sbatch script to run desired number of input decks
# 
# run with the command: sbatch slurm_run_pflotran.sh
# check the queue with squeue
# 
###############################################################################

###############################################################################
# Set the sbatch/SLURM options - these are specified using hash SBATCH:
#   These could be set as command line options to sbatch or environmental variables instead.

#SBATCH --partition=PA
#SBATCH --job-name=time-5y
#SBATCH --mail-type=ALL
#SBATCH --mail-user=ergiamb@sandia.gov
#SBATCH --array=1-300                #job array
##SBATCH --nodes=1                   #number of nodes - can be calc'd from ntasks and cpus-per-task
#SBATCH --ntasks=1                  #tasks per node - why would this be 1?
#SBATCH --cpus-per-task=1           #cpus per task
#SBATCH --time=6:00:00              #wall clock time

## also --ntasks-per-core and --ntasks-per-node available
## and --output=<name with variable substition> for slurm script output
###############################################################################

###############################################################################
# Do some work
# we could move all of this to a separate script, then execute it:
# e.g. srun -n1 task_script.sh 

###############################################################################
# Calculate the scenario, replicate, and vector from the job's TASK_ID
###############################################################################
# This uses the shell's integer math
# This assumes that there are 100 vectors, 3 replicates, and 6 scenarios, 
# and that the task_id starts at 1.
# Please adjust as necessary - and remember order-of-operations

TASK_ID=$SLURM_ARRAY_TASK_ID
nv=100
nr=3
ns=1

# # To order by vector, scenario, then replicate, use:
# VECTOR=$(( (($TASK_ID-1)%$nv) + 1 ))
# SCENARIO=$(( ((($TASK_ID-1)/$nv)%$ns) + 1 ))
# REPLICATE=$(( (($TASK_ID-1)/($nv*$ns)) + 1 ))

# To order by vector, replicate, then scenario, use:
VECTOR=$(( (($TASK_ID-1)%$nv) + 1 ))
REPLICATE=$(( ((($TASK_ID-1)/$nv)%$nr) + 1 ))
#SCENARIO=$(( (($TASK_ID-1)/($nv*$nr)) + 1 ))
SCENARIO='time-5y'

# # To order by scenario, vector, then replicate, use:
# SCENARIO=$(( (($TASK_ID-1)%$ns) + 1 ))
# VECTOR=$(( ((($TASK_ID-1)/$ns)%$nv) + 1 ))
# REPLICATE=$(( (($TASK_ID-1)/($nv*$ns)) + 1 ))

# Use printf to zero-pad the vector number, and place the output (-v) back in VECTOR
printf -v VECTOR "%03g" $VECTOR

# Concatenate the replicate, scenario, and vector
#RSV_NAME=r${REPLICATE}_s${SCENARIO}_v${VECTOR}
RSV_NAME=r${REPLICATE}_${SCENARIO}_v${VECTOR}
###############################################################################

###############################################################################
# Run PFLOTRAN
###############################################################################

# pflotran executable
#EXE_PFLOTRAN=/utilities/pflotran/pflotran/build/glenn_wipp-solution-controls/linux-gcc-4_8-debug/pflotran
#PFLOTRAN_SRC=/utilities/pflotran/pflotran/build/glenn_wipp-solution-controls/linux-gcc-4_8-opt
#PFLOTRAN_SRC=/home/ergiamb/software/pflotran/src/pflotran
#EXE_PFLOTRAN=/utilities/pflotran/pflotran/build/master/linux-gcc-4_8-debug/pflotran
EXE_PFLOTRAN=/utilities/pflotran/pflotran/src/pflotran/pflotran-071318
#EXE_PFLOTRAN=/utilities/pflotran/pflotran/build/linux-gcc-4_8-opt/pflotran-042318

# input and output directories
#PF_DECK_DIR=/home/rsarath/pfd_analysis/pflotran_decks
#PF_OUT_DIR=./s${SCENARIO}
PF_OUT_DIR=./${SCENARIO}

# deck name
PF_DECK_BASENAME="pf_PFD"

# Concatenate the deck name
DECK_NAME=${PF_DECK_BASENAME}_${RSV_NAME}
RESTART_NAME=${PF_DECK_BASENAME}_restart0y_r${REPLICATE}_v${VECTOR}.py

# Copy the deck to the output directory - we will run it from there
#echo ${DECK_NAME}
#mkdir -p ${PF_OUT_DIR}
#cp ${PF_DECK_DIR}/${DECK_NAME}.inp ${PF_OUT_DIR}/${DECK_NAME}.inp

# Run pflotran
cd ${PF_OUT_DIR}
$EXE_PFLOTRAN -input_prefix ${DECK_NAME} >& ${DECK_NAME}.stdout 2>&1
cp ${DECK_NAME}-restart.h5 ../restart
cd ../restart
python ${RESTART_NAME}
cd ..

#$EXE_PFLOTRAN \
#  -input_prefix ${PF_OUT_DIR}/${DECK_NAME} \
#  >& ${PF_OUT_DIR}/${DECK_NAME}.stdout 2>&1
  
# Remove some output
#rm -f {PF_OUT_DIR}/${DECK_NAME}.out
###############################################################################
