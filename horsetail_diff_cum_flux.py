'''horsetail_diff_cum_flux.py

   Contains HorsetailDiffCumFluxPlot() class, which has load_df() method
   specific to *_diff_cum.txt files, which contain cumulative flux values
   at 10ky.

   Uses HorsetailAreaPlot() class from horsetail_diff_area.py if plotting
   differences calc'd with absolute area integration (aia).

   Requires column headings df that match assumptions in horsetail_diff_area.py

   Glenn Hammond <gehammo@sandia.gov>
   Emily Stein <ergiamb@sandia.gov>
'''

import sys
import os
import shutil
import argparse

import numpy as np
import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import pandas as pd
import horsetail_diff_area as ha

interface_areas = {#location:area
             'lwb_north_mb139':9170.02*0.85,
             'lwb_north_anhydrite':9170.02*0.27,
             'lwb_north_mb138':9170.02*0.18,
             'lwb_south_mb139':11020.87*0.85,
             'lwb_south_anhydrite':11020.87*0.27,
             'lwb_south_mb138':11020.87*0.18,
             'borehole_culebra':0.27575*0.27575,
             'shaft_culebra':9.5*10.,
                 }

class HorsetailDiffCumFluxPlot(ha.HorsetailAreaPlot):
  '''Plots scatter plot of difference in cumulative flux.
     Requires knowledge of column headings in input file
     see https://stackoverflow.com/questions/5147112/how-to-put-individual-tags-for-a-scatter-plot
  '''
  def  __init__(self,directory,scenario,replicate,
                region,variable,colorflag='rainbow',
                reldiff_threshold=0.03,effectively_zero_diff=1.,
                plot_string=None):
    super(HorsetailDiffCumFluxPlot,self).__init__(directory,scenario,replicate,
                region,variable,colorflag,
                reldiff_threshold,effectively_zero_diff,
                plot_string)
                   
  def load_df(self,):
    '''load *_diff_cum.txt into pandas DataFrame
       (has more columns than *_diff_area.txt files)
    '''
    string = 'r{}_s{}_{}_diff_cum.txt'.format(self.r,self.s,self.var)
    self.area_filename = os.path.join(self.directory,string)
    self.df = pd.read_table(self.area_filename,sep=' ',engine='python',
              header=None,index_col=0,skiprows=5,skip_footer=4,
              names=['vector','bragflo','pflotran','difference','relative_difference'])

###############################################################################
if __name__ == '__main__':
  '''get command line arguments and run stuff
  '''
  string1 = ','.join(['CUMULATIVE_BRINE_MASS_FLUX','CUMULATIVE_BRINE_MASS_FLUX_OUT',])
  string2 = ','.join(['too','lazy'])

  parser = argparse.ArgumentParser()
  parser.add_argument('-d','--horsetail_flux_plot_dir',
                      help='directory containing flux difference *.txt files' +
                           ' [default = ./horsetail_plot_dir]',
                      default = 'horsetail_flux_plot_dir',
                      required=False,)
  parser.add_argument('-s','--scenario',
                      help='int (1 thru 6) specifying scenario' +
                           ' [default = 1]',
                      default = 1,
                      required = False,)
  parser.add_argument('-r','--replicate',
                      help='int (1 thru 3) specifying replicate' +
                           ' [default = 1]',
                      default = 1,
                      required = False,)
  parser.add_argument('-e','--region',
                      help='To plot flux output, string must exactly equal one of: ' +
                            string1 +
                           '\n[default = CUMULATIVE_BRINE_MASS_FLUX]',
                      default='CUMULATIVE_BRINE_MASS_FLUX',
                      required = False,)
  parser.add_argument('-a','--variable',
                      help='To plot flux output, string must exactly equal one of: ' +
                            string2 +
                           '\n[default = borehole_culebra]',
                      default='borehole_culebra',
                      required = False,)
  parser.add_argument('-c','--colors',
                      help='choose "rainbow" or "blue_and_red"' +
                           ' [default = rainbow]',
                      default='rainbow',
                      required=False,)
  parser.add_argument('-t','--threshold',
                      help='rel diff threshold above which flag violators' +
                           ' [default = 0.03]',
                      default = 0.03,
                      required = False,)
  parser.add_argument('-z','--effectively_zero',
                      help='abs diff threshold below which do not flag violators' +
                           ' [default = 0.01]' +
                           ' which is good for saturation but not for fluxes',
                      default = 0.01,
                      required = False,)
  parser.add_argument('-x','--text_for_plot',
                      help='optional string to add to plots' +
                           ' [default = None]',
                      default = None,
                      required = False,)

  args = parser.parse_args()

# scatter plots using straight up cumulative flux
  if args.region == 'CUMULATIVE_FLUX_10KY':
    diff = HorsetailDiffCumFluxPlot(directory=args.horsetail_flux_plot_dir,
                      scenario=int(args.scenario),
                      replicate=int(args.replicate),
                      region=args.region,
                      variable=args.variable,
                      colorflag=args.colors,
                      reldiff_threshold=float(args.threshold),
                      effectively_zero_diff=float(args.effectively_zero),
                      plot_string = args.text_for_plot)
    diff.find_violators()
    #diff.reldiff_v_vector()
    diff.reldiff_v_absdiff()
    #diff.reldiff_v_absvalue()

# scatter plots using absolute area integrals of whatever flux you've chosen
  else:
    diff = ha.HorsetailAreaPlot(directory=args.horsetail_flux_plot_dir,
                      scenario=int(args.scenario),
                      replicate=int(args.replicate),
                      region=args.region,
                      variable=args.variable,
                      colorflag=args.colors,
                      reldiff_threshold=float(args.threshold),
                      effectively_zero_diff=float(args.effectively_zero),
                      plot_string = args.text_for_plot)
    diff.find_violators()
    #diff.reldiff_v_vector()
    diff.reldiff_v_absdiff()
    #diff.reldiff_v_absvalue()

